<?php
ob_start();
$question = 8;

foreach ($_REQUEST as $a=>$b){$$a=$b;}
//$next_question = $_REQUEST["next_question"];
//$q8 = $_REQUEST["q8"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q8 == "" && $next_question == 9){
    $err_msg = "Please select a least one choice";
    $next_question = 8;
}

//Print and record question results
if ($next_question == 9){
    $total = count($q8);
    for($i = 0; $i < $total; $i++){
        $response .= "[Question_8: ".$q8[$i]."], ";
        if ($i < ($total - 1))
            $response .= "&";
    }
    setcookie ("next", "9");
    setcookie ("ans[7]", $total);    
    setcookie ("response", $response);
    $msg .= "<h4>Answer to Question Eight:</h4>";
    if (count($q8) == 8) {
        $msg .= "Correct! All sorts of people publish on the Web.<br>";
    } else {
        $msg .= "Nope. Actually <b>all</b> the groups
        listed can and do publish information on the web. Remember that
        domain names can end in: .edu, .gov, .com, .org and country
        codes. Each of these will give you a hint about what type of group
        is providing the information.<br>";
    }
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 9</a></div></p>";
}
?>
<h2>Question Eight</h2>
<h3>
Who publishes information on the Web?<br>(Choose all that apply)</h3>
<form method="POST" action="q8.php">
<div align="CENTER">
  <table border="0" width="33%" align="CENTER">
    <tr> 
    <td>
    <?php
    if ($q8[0] == "students"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"students\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"students\">";
    endif;
    ?> 
    Students</td>
  </tr>
    <tr> 
    <td>
    <?php
    if ($q8[0] == "faculty" || $q8[1] == "faculty"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"faculty\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"faculty\">";
    endif;
    ?> 
    Faculty</td>
  </tr>
   <tr> 
    <td>
    <?php
    if ($q8[0] == "foreigners" || $q8[1] == "foreigners" || $q8[2] == "foreigners"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"foreigners\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"foreigners\">";
    endif;
    ?> 
    People in foreign countries
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q8[0] == "libraries" || $q8[1] == "libraries" || $q8[2] == "libraries" || $q8[3] == "libraries"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"libraries\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"libraries\">";
    endif;
    ?> 
   Libraries
  </tr>
      <tr> 
    <td>
    <?php
    if ($q8[0] == "government" || $q8[1] == "government" || $q8[2] == "government" || $q8[3] == "government" || $q8[4] == "government"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"government\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"government\">";
    endif;
    ?> 
    Government agencies
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q8[0] == "universities" || $q8[1] == "universities" || $q8[2] == "universities" || $q8[3] == "universities" || $q8[4] == "universities" || $q8[5] == "universities"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"universities\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"universities\">";
    endif;
    ?> 
    Universities</td>
  </tr>
		  <tr> 
    <td> 
    <?php
    if ($q8[0] == "companies" || $q8[1] == "companies" || $q8[2] == "companies" || $q8[3] == "companies" || $q8[4] == "companies" || $q8[5] == "companies" || $q8[6] == "companies"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"companies\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"companies\">";
    endif;
    ?> 
    Companies</td>
  </tr>
				  <tr> 
    <td> 
    <?php
    if ($q8[0] == "non-profit" || $q8[1] == "non-profit" || $q8[2] == "non-profit" || $q8[3] == "non-profit" || $q8[4] == "non-profit" || $q8[5] == "non-profit" || $q8[6] == "non-profit" || $q8[7] == "non-profit"):
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"non-profit\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q8[]\" value=\"non-profit\">";
    endif;
    ?> 
    Non-profit organizations</td>
  </tr>
				
</table>
<p> 
 <input type="hidden" name ="next_question" value="9">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
