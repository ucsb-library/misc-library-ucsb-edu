<?php
///////////// function get the path of the page that called the script
///////////// so that the script knows where to get the configuration
///////////// files (mail_thanks.php, mail_thanks_ext.php, sendto.php,
///////////// and mail_confirm.php)

/* description

Returns a variable with the path of the page calling the script.

*/

function getPath(){

	 $who_called=parse_url(getenv ('HTTP_REFERER'));
	 
	 $doc_root=getenv('DOCUMENT_ROOT');
	 
	 $really_works=pathinfo($who_called["path"]);
	 
	 $really_huh=$really_works["dirname"];
	 
	 $my_dir="$doc_root"."$really_huh/";
	 
	 return $my_dir;

}


///////////// function to make sure that the page calling the script
///////////// is located on our server

/* description

Returns true if there is a match and false if not

*/

function checkUrl(){

	$who_called=parse_url(getenv ('HTTP_REFERER'));

	$host=$who_called["host"];
   		if(preg_match("/$url_allowed_from$/","$host")){
	   		return true;
   		}else{
	   		return false;
  		 }
}


///////////// function to get the recipients of the mail form from the
///////////// script sendto.php file located in the directory that called the
///////////// (Not of the main script mail_it.php)

/* description

gets passed $HTTP_POST_VARS in order to make use of parseForm in cases
where the file doesn't exists and the to form element is still present
Eventually remove support for the to header

The pop and slice stuff is used to chop off the comments in the file.

Returns a comma separated list of recipents

*/

function getTo($my_dir,$HTTP_POST_VARS){
	//print"mydir: $my_dir<br>\n";
	//print"HTTP_POST_VARS: $HTTP_POST_VARS<br>\n";
	 if(file_exists("$my_dir"."sendto.php")){
		   $send_to_file=file("$my_dir"."sendto.php");
		   $send_to=array_slice($send_to_file, 4);
		   $garbage=array_pop($send_to);
		   $max=count($send_to);
		   $i=1;
			  foreach($send_to as $key=>$val){
				  $send_em .= chop($val);
					//print"$key = $val<br>\n";
					  if($i != $max){
						  $send_em.=", ";
						  }
						  $i++;
			  }
	 }else{
		 $parsed= parseForm($HTTP_POST_VARS);
		 $send_em= $parsed["to"];
	 }
		 return $send_em;
}

///////////// function to put special fields in a separate array and lump
///////////// the rest in the $body array. In addition it strips the
///////////// 'required-'field labels off of the form names which is used by
///////////// the form validation javascript

/* description

can only be called after parseForm() which provides the $parsed array
Takes the array $parsed and generates an array of two elements
one (display) are the table rows that are to be displayed in the form
two (hidden) are the hidden form fields used to pass the values on to
the next step.

Returns a multidimensional array

*/

function parseForm($HTTP_POST_VARS){

	$parsed=array();
	$num  = sizeof($HTTP_POST_VARS);
	//print "Size of: $num</br>\n";
 
	foreach($HTTP_POST_VARS as $key => $val) {
		// added to remove slashes and encode quotes submitted from form
		$val = str_replace ("\\", "", $val);
 	   	$val = str_replace ( "\'", '&#039;', $val );
       		$val = str_replace ( "\"", '&quot;', $val );
		
	 	$key=str_replace('required-','',$key);
		 
		switch ($key) {
			case 'e-mail':
				$parsed["from"]=$val;
				$parsed['body']["e-mail"]=$val;
				break;
				 
			case 'from':
				$parsed["from"]=$val;
				break;
				 
			case 'to':
				//$parsed[to]=$val;
				break;
				 
			case 'subject':
				$parsed["subject"]=$val;
				break;
				 
			case 'confirm':
				$parsed["confirm"]=$val;
				break;
				 
			case 'formname':
				$parsed["subject"]=$val;
				break;
				 
			case 'submit':
				$parsed["submit"]=$val;
				break;
				 
			case 'mydir':
				$parsed["mydir"]=$val;
				break;
				 
			case 'nonucsb':
				$parsed["nonucsb"]=$val;
				break;
				 
			 default:
				$parsed['body']["$key"].="$val";
		}
		
	}
return $parsed;

}


///////////// function to put form values in hidden form fields to pass to next
///////////// page won't currently parse forms fields with array's for values

/* description

can only be called after parseForm() which provides the $parsed array
Takes the array $parsed and generates an array of two elements
one (display) are the table rows that are to be displayed in the form
two (hidden) are the hidden form fields used to pass the values on to
the next step.

Returns a multidimensional array

*/

function getConfirmVals($parsed,$my_dir) {

	 $body_array=$parsed['body'];
	 
	   $disp = '';
	   foreach ($body_array as $fieldName=>$fieldValue) {
		  $disp .= "<TR><TD class=text>&nbsp;$fieldName&nbsp;<BR></TD><TD class=text>&nbsp;$fieldValue&nbsp;<BR></TD></TR>\n";
	   }
	   $hidden="<input type=\"hidden\" name=\"mydir\" value=\"$my_dir\"/>\n";
	   foreach ($parsed as $fieldName=>$fieldValue) {
		 if($fieldName!="body"){
			 $hidden .= "<input type=\"hidden\" name=\"$fieldName\" value=\"$fieldValue\"/>\n";
		  }
	   }
	   foreach ($body_array as $fieldName=>$fieldValue) {
		  $hidden .= "<input type=\"hidden\" name=\"$fieldName\" value=\"$fieldValue\"/>\n";
	   }
	   
	   $confirm_vals=array();
	   $confirm_vals['display']=$disp;
	   $confirm_vals['hidden']=$hidden;
	   return $confirm_vals;
}


function mailIt($from,$to,$subject,$body){

// added to put the quotes back in for the email

 	   $body = str_replace ( "&#039;", "\'", $body );
       $body = str_replace ( "&quot;", "\"", $body );
       

	if(!isset($from) || $from==""){
		$from="Webform";
        }

	 require_once('Mail.php');
	 

	 
	 $headers['From']    = "$from";
	 $headers['To']      = "$to";
	 $headers['Cc']      = "$from";
	 $headers['Date']      = date('D, j M Y H:i:s O'); 
	 $headers['Subject'] = stripslashes("$subject");

//	 $recipients=$to;
	 $recipients=$to.",".$from; 
//   $params['host'] = 'localhost';
	 $params['host'] = 'smtp.library.ucsb.edu';
	 
	 $mail_object =& Mail::factory('smtp', $params);
	 
	 $success=$mail_object->send($recipients, $headers, $body);
	 
	 if($success==1){
		  return true;
	 }else{
		  $vars = get_object_vars($success);
		  $error=$vars[message];
		  return $error;
	 }
}

?>
