<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 8;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q8 == "" && $next_question == 9){
    $err_msg = "Please select a choice";
    $next_question = 8;
}

//Print and record question results
if ($next_question == 9){
    $correct = 0;
    $msg = "<h4>Answer to Question Eight:</h4>\n";
    if ($q8 == "title") {
        $msg .= "<b>A:</b> No. A keyword search looks for terms anywhere in the record of a book or article.<br>";
    }
    if ($q8 == "subject") {
        $msg .= "<b>B:</b> No. A keyword search looks for terms anywhere in the record of a book or article.<br>";
    } 
    if ($q8 == "keyword") {
        $msg .= "<b>C:</b> You're right! A keyword search looks for terms anywhere in the record of a book or article.<br>";
								$correct = 1; 
    }
    if ($q8 == "author") {
        $msg .= "<b>D:</b> No. A keyword search looks for terms anywhere in the record of a book or article.<br>";
    }

    $response = $_COOKIE["response"];
    $response .= "[Question_8: ".$q8."], ";
    setcookie ("response", $response);
    setcookie ("next", "9");
    setcookie ("ans[7]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 9</a></div></p>";
}
?>



<h2>Question Eight</h2>
<h3>Which one of these search types looks for terms anywhere in the record of a book or article? (Choose one)</h3>
<form method="POST" action="q8.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q8 == "title"):
        print "<input type=\"radio\" name=\"q8\" value=\"title\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"title\">";
    endif;
    ?>
    A. Title search 
  </tr>

  <tr> 
    <td> 
   <?php
    if ($q8 == "subject"):
        print "<input type=\"radio\" name=\"q8\" value=\"subject\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"subject\">";
    endif;
    ?>
    B. Subject search 
  </tr>

  <tr> 
    <td> 
   <?php
    if ($q8 == "keyword"):
        print "<input type=\"radio\" name=\"q8\" value=\"keyword\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"keyword\">";
    endif;
    ?>
    C. Keyword search 
  </tr>

  <tr> 
    <td> 
    <?php
    if ($q8 == "author"):
        print "<input type=\"radio\" name=\"q8\" value=\"author\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"author\">";
    endif;
    ?>
    D. Author search 
  </tr>
  </table>

<p>

<input type="hidden" name ="next_question" value="9">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>

<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>