<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 4;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q4 == "" && $next_question == 5){
    $err_msg = "Please select a choice";
    $next_question = 4;
}
//Print and record question results
if ($next_question == 5){
    $correct = 0;
    $msg = "<h4>Answer to Question Four:</h4>\n";
    if ($q4 == "technology") {
        $msg .= "<b>A:</b> No, sorry. 'Technology transfer' is a side issue. Using the descriptor 'Intelligent vehicle--highway
								systems' is closer to the topic of smart cars and will better focus your search.<br>";
    }
    if ($q4 == "automobiles") {
        $msg .= "<b>B:</b> No, sorry. New models of automobiles is a broader topic. Using the descriptor 'Intelligent vehicle--highway
								systems' is closer to the topic of smart cars and will better focus your search.<br>";
				} 
    if ($q4 == "ivehicle") {
    $msg .= "<b>C:</b> Correct. 'Intelligent vehicle--highway systems' is close to the keyword terms you used.
				Searching by this subject descriptor should help you focus your search and find other articles on this subject.<br>";
    $correct = 1; 
				}
    	
    $response = $_COOKIE["response"];
    $response .= "[Question_4: ".$q4."], ";
    setcookie ("response", $response);
    setcookie ("next", "5");
    setcookie ("ans[3]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 5</a></div></p>";
}
?>

<h2>Question Four</h2>
<h3 align="left"><font color="#1F4366">You need to find recent articles on cars 
  that have automated features. You have searched for the topic in an index using 
  the keyword terms &quot;smart&quot; and &quot;cars&quot;. Below is a record 
  that you found.</font></h3>
<table width="71%" border="0" cellspacing="3" cellpadding="3" align="center">
<tr> 
<td valign="top" width="18%"><strong>TITLE:<br>
AUTHOR:<br>
SOURCE:<br>
DESCRIPTOR:</strong></td>
<td><strong>Cars of the Future<br>
Mandel, William <br>
<i>Electronic Design</i>, v. 48 no25 (Dec.4 2000) p.64<br>
Technology transfer<br>
Automobiles---New models<br>
Intelligent vehicle--highway systems</strong></td>
</tr>
</table>
  
<h3 align="left">
<font color="#1F4366">
You want to find more articles on this 
topic, so you decide to search using one of the descriptor subject terms. Which 
of the following searches will focus your search best? (Choose one)<br>
</font></h3>

<form method="POST" action="q4.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q4 == "technology"):
        print "<input type=\"radio\" name=\"q4\" value=\"technology\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"technology\">";
    endif;
    ?>
    A. Technology transfer 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q4 == "automobiles"):
        print "<input type=\"radio\" name=\"q4\" value=\"automobiles\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"automobiles\">";
    endif;
    ?>
    B. Automobiles--New models
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q4 == "ivehicle"):
        print "<input type=\"radio\" name=\"q4\" value=\"ivehicle\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"ivehicle\">";
    endif;
    ?>
    C. Intelligent vehicle--highway systems 
  </tr>
   </table>
<p>
<input type="hidden" name ="next_question" value="5">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
