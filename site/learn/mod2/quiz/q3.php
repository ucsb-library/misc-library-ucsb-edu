<?php
ob_start();
require_once "../../quiz_header.php";
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 3;
$next_question = $_REQUEST["next_question"];
$a = $_REQUEST["a"];
$b = $_REQUEST["b"];

//Check that question hasn't been answered

$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($a == "" && $next_question == 4){
    $err_msg = "Please select an answer for A";
    $next_question = 3;
} else if ($b == "" && $next_question == 4){
    $err_msg = "Please select an answer for B";
    $next_question = 3;
}
//Print and record question results
if ($next_question == 4){
    $correct = 0;
    $msg = "<h4>Answers to Question Three:</h4>";
    $msg .= "<b>A: </b>";
    if ($a == "and"){
        $msg .= "Yes. Connect different ideas with  AND.<br>";
								$correct ++;
    } else {
        $msg .="Nope. Phrases that include words like 'who' are usually not effective. 
								Remember to connect different ideas with AND.<br>";
    }
    $msg .= "<b>B: </b>";
    if ($b == "or") {
        $msg .= "Correct. The terms that are synonyms for the same idea are connected with OR. 
								You are saying you want articles with one term OR another.<br>";
        $correct ++;
    } else {   
        $msg .= "This is tricky. Using AND would mean the record would <b>have to include all four terms</b> in it. 
        Also, some of these terms are synonyms for the same concept.<br>
								They can be connected with OR (for example, work OR employment OR job).<br>";
    } 
    $response = $_COOKIE["response"];
    $response .= "[Question_3a: ".$a.", Question_3b:" .$b."], ";
    setcookie ("response", $response);
    setcookie ("next", "4");
    setcookie ("ans[2]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 4</a></div></p>";
}
?>
<h2>Question Three</h2>
<h3>
Imagine you are searching a Library database for articles about college students' jobs. 
Choose the better search strategy from each of the examples below. (Choose one answer for each question)</h3>

<form method="POST" action="q3.php">
<div align="CENTER">
 <table border="0" width="61%" cellspacing="8" cellpadding="10">
      <tr> 
        <td width="9%" align="LEFT" valign="TOP"> 
          <div align="LEFT"> A. </div>
        <td width="91%" align="LEFT"> 
          <?
  										if ($a == "natural"):
     							print "<input type=\"radio\" name=\"a\" value=\"natural\" CHECKED>";
  										else: 
     							print "<input type=\"radio\" name=\"a\" value=\"natural\">";
  										endif;
										?>
          students who work<font color="#000333">&nbsp;</font> <br>
         	<?
    								if ($a == "and"):
        				print "<input type=\"radio\" name=\"a\" value=\"and\" CHECKED>";
    								else: 
        				print "<input type=\"radio\" name=\"a\" value=\"and\">";
    								endif;
										?>
          students and work 
      </tr>
      <tr> 
        <td width="9%" valign="TOP"> 
          <div align="LEFT"> B. </div>
        <td width="91%"> 
         <?
    							if ($b == "and"):
        			print "<input type=\"radio\" name=\"b\" value=\"and\" CHECKED>";
    							else: 
        			print "<input type=\"radio\" name=\"b\" value=\"and\">";
    							endif;
    				?>
          <font color="#000333">work and employment and job</font> and students<br>
        <?
    								if ($b == "or"):
        				print "<input type=\"radio\" name=\"b\" value=\"or\" CHECKED>";
    								else: 
        				print "<input type=\"radio\" name=\"b\" value=\"or\">";
    								endif;
    				?>
          (work or employment or job) and students
      </tr>
    </table>

				<p>
<input type="hidden" name ="next_question" value="4">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "../../include/quiz_footer.php";
?>