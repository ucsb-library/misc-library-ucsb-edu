<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<html>
<head>
<title>Weaving the Web into Course Integrated Instruction</title>
</head>
<body bgcolor="#ffffff" text="#000000" link="#0000ff" alink="#b03060" vlink="#d02090">
<img src="web041.gif" alt="Untangling the Web">

<hr>

<h1 align=center>Weaving the Web into Course Integrated Instruction</h1><br>
<center><strong><a href="mailto:rwallach@charon.usc.edu">Ruth 
Wallach</a></strong><br> 
Acting Head<br>
<strong><a href="mailto:lmccann@calvin.usc.edu">Linda McCann</a></strong><br>
Reference Librarian<br>
Doheny Reference Center<br>
University of Southern California<br>
</center><p>
<hr>
<center>Copyright 1996, Ruth Wallach and Linda McCann. Used with 
permission.</center>
<hr>

<h2>Course Integrated Instruction in the Humanities: Two Specialized
Examples</h2>

In early fall 1995, the Reference Center was asked to conduct a research
session by a professor in the department of French and Italian, who was
teaching an upper division undergraduate course on Dante's Divine Comedy. 
She asked the librarians to talk about Dante related resources on the
Internet, and to show her students how to search the Dartmouth Dante
Project. The Dartmouth Dante Project is an electronic scholarly project
started in the early 1980s to provide scholars with line-by-line access to
the Divine Comedy in Italian and to approximately 60 commentaries written
in Italian, Latin and English, representing 670 years of Dante
scholarship. It is a very complex database, based on the BRS search
engine, and is not easily searchable. Our faculty member was at one point
heavily involved in the inception of the project, participating in the
original NEH grant proposal and editing two of the commentaries, but she
has not dealt with the database for many years, and did not know how to
search it. <p>

Preparing the library session for this course was a collaborative project
between myself and a colleague in the Reference Center who is here today,
Linda McCann. We were assisted by a third librarian, Eileen Flick, who
had considerable technical knowledge and understanding about the Internet. 
I created a simple homepage for the class, which listed the course
syllabus, the reading assignments, and selected Internet resources related
to Dante. <p>

My reasons for designing a homepage were as follows:

<ul>

<li>A homepage would unify various Internet resources in a meaningful way
and for a specific purpose

<li>It would be good practice, since in my opinion, librarians need to
participate in the creation of curricular and research resources on the
Internet. This homepage was a start. 

<li>The faculty member was a very important person at the university - it
would be good for the library to impress her with its technological and
subject related know-how. 

</ul>
<p>

Because this faculty member was interested specifically in the Dante
Project, Linda and I decided not to delve into the more traditional
components of library instruction, such as showing how to go about finding
primary and secondary print resources in Italian Renaissance literature,
since showing how to search the Dante Project would take up most of the
class time. <p>

In studying the Dante Project Linda discovered that it was completely
impossible to telnet to it at or around noon - the time of our class. The
weekend before our library instruction session, she hit on the idea of
logging into Dartmouth at a time when it was possible, downloading search
screens from the Project into our class homepage and incorporating them
into a very detailed online search guide. This way, if we could not
telnet to the Project on the appointed day, we could still show the
students how to search the database. Both of us spent some time leafing
through the Divine Comedy to find interesting canticles for demonstration. 
As soon as she was able to log on to the Dante Project, Linda performed
several searches which demonstrated the research potential of the
database, and captured the search screens and the result screens. She
then wrote a detailed search guide which explained how to use this complex
database. Eileen, the librarian in the department with considerable
technical know-how, HTMLed the search guide and added it and the captured
screens to the class homepage. After close to 40 hours of work, our class
homepage had 30 HTML files and 35 image files and Linda and I had a
back-up in case our telnet attempt to the Dartmouth Dante Project failed
during class. And it did. Nevertheless, our session was successful,
thanks to the "virtual" search guide. <p> I don't know if giving out our
search guide as a paper handout would have worked as well with a class of
undergraduates as showing it to them on the Web. Perhaps, if the guide
were not so long and complicated, this would have been a good idea. As
things stood, the guide worked very well in the hypertext environment
offered by the Web browser. Overall, preparing for this course consumed
close to 50 hours of 3 librarians' work. <p>

At around the same time I was preparing for a library instruction session
to be delivered to a small graduate seminar in French literature. The
overarching topic of this course was to examine the development of
Rousseau's idea of the social bond in French literature from the late 18th
to the end of the 19th century. The instructor asked that I show his
class how to search ARTFL, a large full text database of almost 2000
French authoritative texts spanning several centuries. I decided to
create a homepage for this course as well, similar to the one we did for
Dante, which would link to several interesting sites in literature and
criticism, and to a brief listing of important bibliographic resources in
French literary studies. Since I already had experience with "throwing
together" a course homepage, it didn't take too long to do this time. 
Then I embarked on the interesting process of learning how to search ARTFL
to which my institution subscribes. <p>

ARTFL is another complex database - it started out as a dictionary
treasure of the French language, has many authoritative texts, is
sensitive to the peculiarities of the French language (such as diacritic
marks), and has a very complex system of truncation to accommodate
variations in spelling over the centuries. The ARTFL Web site includes
online instructions, which by necessity are very lengthy, and are not easy
to follow. The course instructor told me that he determined that his
students were not electronically proficient, and would therefore need
basic instructions. In my opinion, it is close to impossible, and
probably not very productive to create basic instructions for a complex
full text database, and ARTFL was created precisely to enable complex
textual searches. It took me close to 10 hours to distill the online
instructions into a manageable and meaningful form. This time, because
there were few students in the class and the students themselves had
little experience searching full text databases, and because the database
was of utmost importance to their research, the search instructions were
distributed in class on paper. <p>

Unlike the more traditional library instruction courses which concentrated
on the use of known and well established bibliographic sources and guides,
these two classes revolved around the use of complex full text scholarly
resources in an unstable and ever-changing environment. Scholarly
electronic resources require subject knowledge on the part of the searcher
(be that a librarian or someone else) because they allow for greater
latitude and complexity in manipulating data. Thus, preparing for the two
sessions took a long time. Although some of the time was spent on
peripherals (i.e., preparing a simple course homepage), most of it was
consumed in exploring a particular resource and making
it...well...teachable. <p>

In thinking about designing library sessions with the use of the Web, the
instructor should keep the following questions in mind: 

<ul>
<li>Design of class
<ul>

<li>What is the audience? What is its subject knowledge? What are its
electronic skills? 

<li>Will the demonstration of an Internet resource be "balanced" by
discussion of a print resource? 

<li>Will there be props/aids developed for the class? Will there be paper
handouts, and of what kind? What will be their role vis-a-vis the
Internet? 

<li>Will the Internet be merely a tool for demonstrating a resource in
class, or is there a reason to create a homepage? Are there other reasons
to create a homepage? What are the future uses of this page? 

</ul>
<li>Time and Expertise of the Librarian(s)
<ul>

<li>Does the librarian have knowledge in the subject area (which may be
very specialized)? Concurrently, does the librarian understand the
research process practiced within the subject area? 

<li>What kind of incentive is there for the librarian to engage in
specialized instruction and in producing a curricular resource on the
Internet? 

</ul>
</ul>

The proliferation of full text scholarly resources on the Web and in other
electronic forms is changing the way we (and the teaching faculty)
conceive of library instruction. Teaching faculty (and occasionally
students) have the subject knowledge, but often no real understanding of
how full text databases are searchable. For librarians, knowing something
of the subject matter is very important, but being able to explain how to
get from point A to point B in a particular database is of utmost
importance. <p>

We would like to emphasize that instruction with the Internet has the
potential of becoming very specialized. Consider the fact that in the
past, in a class on how to do research on the Divine Comedy, the librarian
would have spoken about an array of search strategies the researcher would
use to find primary and secondary texts. The availability of a large
corpus of texts online changed the thrust of instruction towards a more
technological approach; yet the librarian still has to use his/her
knowledge to demonstrate a database and to create a guide to it. 
Librarians who contemplate doing library instruction with or on the
Internet must keep track of the time spent preparing and delivering
instruction on the use of electronic resources. As our experience with the
Dante and the French literature classes shows, preparing a session on the
use of even one electronic resource can be extremely time consuming. 
Presumably, next time we are asked to teach a class on Dante or on French
Literature, we would not have to put in as much work. That is, if we
assume that in the future these electronic resources would remain the
same. <p>

<h2>Course-Integrated Instruction Projects: Consequences of Success</h2>

As indicated in the first section of this paper, there are several
important issues which librarians who are developing course-integrated web
pages need to consider. First, because of the relative novelty of the
web, these projects frequently are requested by teaching faculty with
little advance notice. The instruction is to be integrated into course
schedules and syllabi developed by the instructor often well before any
consideration has been given to instruction in electronic resources. Of
course, one reason for doing this type of pilot project is to create a
model that can then be presented to other interested academic faculty, so
that a program for systematically developing such course-integrated web
projects on a wider scale can be put into place. Once one or two projects
have been designed and have proved their usefulness, what happens next?
<p>

This leads to a second problem that frequently confronts academic
librarians who are developing web tools for course-integrated instruction
in addition to all their other activities. The time and staff needed to
expand pilot projects into programs for the university at large is simply
not in place. With budget and other organizational issues facing academic
libraries, it is difficult to get this type of activity past the test
pilot phase. While collaborative team efforts between academic and
library faculty allow for creative approaches to integrating the web into
instruction, team efforts do not necessarily result in less use of
professional library staff time and expertise. On the contrary,
indications are that they require more professional staff if the efforts
are to be successfully expanded. If librarians are to create a role for
themselves in a world where the web directly links the instructors and
students to information - a role that is more than just that of building
and testing model web access tools for faculty to take over and develop
into full-blown programs - then we need to move quickly, because much of
the current activity in this area is found on academic department
homepages, rather than library homepages. <p>

This raises the question whether there is a special role for librarians,
given that many scholars are applying their scholarly approaches to
developing their own course-integrated web pages. When setting out to
build a specialized web tool for academic course-integrated use, one
quickly discovers that a major issue is that of coordinating two
non-intersecting sets of expertise. The first is the subject expertise of
the academic instructor, which is directed largely to texts and other
types of media, and the second is that of the librarian who brings, often
in conjunction with some subject expertise, a technical expertise based on
his or her training and experience in searching, organizing, providing
access to, and evaluating information. The two sets of expertise do not
automatically mesh. <p>

Expertise that librarians may have in creating web pages to provide access
to specialized scholarly resources can involve several different areas. 
One type of expertise is entirely technical; a web project may require
considerable programming and database design expertise that librarians may
possess or have access to through working with staff who possess these
skills. The expertise that is involved is not especially different from
that required for developing specialized electronic resources that are not
on the web or were migrated to the web from a previous environment, such
as library catalogs. <p>

Another type of expertise involves conceptual design of a web site that
adds value to the sort of tools librarians have always used to increase
access to information for research and instruction. In the case of the
Dartmouth Dante Project Database, which is a specialized tool designed by
scholars for purposes of research and instruction, the content was complex
(involving multiple languages and special search techniques), the database
interface was not intuitive, and the telnet address was experiencing
difficulties that go beyond the traffic problems we are used to
experiencing on the Internet. These are familiar problems to librarians
who have been heavily involved in library instruction. Based on expertise
in library instruction, we studied the subject content of the database and
designed sample searches to illustrate typical research problems, and then
wrote specialized brief and simplified instructions, in consultation with
the faculty in Princeton who designed the Dante database, that included
screen dumps as visual aids, and steps for telnet access. A librarian
with special technical expertise in creating web documents worked to
combine these components into an attractive and intuitive web page. <p>

The technical knowledge and experience with electronic information sources
and with issues of integrating these into instruction which librarians
bring to a web project can be crucial to its final success. As described
in the first part of the paper, the faculty member who approached us for
course instruction was originally interested in more traditional library
instruction. This faculty member became interested in the web project we
proposed as a way to gauge the potential usefulness of electronic
resources in future instructional programs. It was important that a
web tool was successfully designed and implemented in the first few weeks
of the course for students to use for research. A measure used by this
faculty member was satisfaction and increased interest by students in the
course. This faculty member was very pleased when students reported that
this advanced course on Dante for undergraduates in Comparative Literature
and Italian was a lot of fun. The faculty member attributed this to our
web page and plans to use this page again. <p>

The expertise that librarians bring to this type of pilot project, that of
interpreting technology for novice or occasional users, is often
considered to be outmoded in the web environment, where information is
readily available to be interpreted by the user. The web resources
described in this paper provide examples of a justification for
incorporating librarians' expertise into instruction, because the web site
provides twenty-four hour remote access and instruction to using difficult
research tools created by scholars. The usefulness of this type of
instructional research web tool has been successfully demonstrated in both
pilot projects described in the first part of this paper. Issues of
ownership, who will maintain the page to ensure access and preservation,
as well as questions of copyright, can be negotiated. What has yet to be
demonstrated is that the library is in a position to respond to very many
more requests for such course-integrated tools by other academic faculty.
<p>

We are still in the early phase of integrating the web into academic
course instruction, where both library and academic faculty are driven by
the technology. We do not have a good theoretical basis for thinking that
spending human and technical resources on developing web course-tools is a
good idea. In a time when most innovative instructional ideas are not
based on theory, we will need to develop better measures for determining
whether integrating the technology of the web improves instruction if we
want to develop single projects into programs; otherwise we run the risk
of being thrown off a runaway bandwagon. <p>

The initiative for collaborative efforts for pilot projects that involve
integrating the web into courses can come from either academic faculty or
librarians. Frequently when academic faculty have technical expertise or
access to technical expertise, for instance through centers for computing,
it will not occur to them to collaborate with librarians unless there is a
library program in place that encourages them to do so. However, at this
time it is probably safe to say that many academic faculty do not feel a
level of comfort with technology or enjoy a level of support from
departments and university administration that allows them to easily
initiate projects integrating the web into their courses. Faculty who are
interested in incorporating the web into courses can be very receptive
when librarians initiate such projects, especially when the emphasis is on
the content of the course, rather than on the technology, which many
faculty feel can provide a source of frustration or distraction from the
ideas or content. <p>

One role that librarians can play is to develop web access tools that
collocate electronic resources and provide instruction on how to evaluate
and use specialized electronic resources that have already been made
available through the web by academic faculty and researchers. These web
tools provide access to scholarly electronic resources that would
otherwise be relatively inaccessible; this is not because a user can not
find them, but because they are difficult to use due to scholarly content
and sometimes due to interface design that is not intuitive. A tool that
provides access for the novice or occasion user adds value to the
scholarly resource and justifies the development of the web project beyond
its use in a particular course. <p>

The multi-disciplinary background of many librarians also provides a new
perspective regarding the development of electronic resources for the web
that can complement the subject expertise of many academic faculty. 
Faculty may also teach courses in innovative ways that involve pulling
together many types of materials in courses that could benefit from using
the web to create a research tool. I am collaborating with a faculty
member who teaches a course at undergraduate and graduate levels which
involves literature, film, and other media, to develop a multimedia web
site for the course based on a bibliography and syllabus that will provide
hypertext links to additional materials, many of which we will put into
electronic form for the first time. This web project will provide a site
to incorporate original research that is submitted by graduate students as
part of their work for the course to collaboratively build a research
resource that can be used by future students as well as researchers. This
project would not have been independently proposed by this faculty member,
because of the level of comfort with technology and time constraints. <p>

The pilot projects described in this paper involved risk of failure
because they were new and untested and because they required many overtime
hours of the librarians involved. The innovative ideas as well as the
initiation of the collaborative efforts has come from the librarians. As
academic libraries are redefining goals and reallocating resources, these
pilot projects should be evaluated from the perspective of the value that
outreach to academic faculty and the positive new roles that library
faculty have taken on can represent in terms of the institutional role of
the library. The library will need to determine if these efforts are a
priority if they are to continue to be successful and the pilot projects
that librarians have been able to initiate are developed into programs
that involve library faculty past the initial conception. Academic
faculty will go elsewhere for collaborative programs if academic libraries
determine that this is not a priority for the future. 


<P> <A HREF="http://validator.w3.org/"><IMG SRC="http://www.library.ucsb.edu/gallery/valid_html3.2.gif" ALT="HTML 3.2 Checked!" border=0></A>

</body>
</html>
