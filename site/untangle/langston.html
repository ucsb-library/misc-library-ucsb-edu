<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<html> <head> <title>Scholarly Communication and Electronic Publication:
Implications for Research, Advancement, and Promotion</title> </head>
<body bgcolor="#ffffff" text="#000000" link="#0000ff" alink="#b03060"
vlink="#d02090"> <img src="web041.gif" alt="Untangling the Web">

<center> <table width="75%" border=1 cellpadding=3><tr><td> URLs in this  
document have been updated.  Links enclosed in <strong>{curly
brackets}</strong> have been changed.  If a replacement link was located,
the new URL was added and the link is active; if a new site could not be
identified, the broken link was removed.</td></tr></table></center><p>


<hr>

<h1 align=center>Scholarly Communication and Electronic Publication:
Implications for Research, Advancement, and Promotion</h1>

<center><strong><a href="mailto:langston@ucrac1.ucr.edu">Lizbeth
Langston</a></strong><br> University of California, Riverside</center>
<hr>
<center>Copyright 1996, Lizbeth Langston. Used with permission.</center>
<hr>

<h2>Abstract</h2>
<blockquote>

In this paper I look at the issue of how scholarly publishing in
electronic forums affects the academic tenure process. Currently,
scholarly publication as it relates to the merit and promotion process is
generally defined as publication in refereed journals or as publishing
scholarly books. This definition, implying a fixed language-based text
and an identifiable author (or attributable contributions from multiple
authors, depending upon the field) is beginning to be questioned in theory
and in practice. Implications for faculty members going through the
tenure process, and thus for academic libraries and for librarians, who
work to be integral players in the university community, will depend to
what extent contemporary theoretical positions are put into practice,
whether as radical change or as assimilated into current practices of the
advancement and promotion process.
</blockquote><hr><p>

In this paper I treat how scholarly publishing in electronic forums may
affect the academic tenure process. Several aspects to consider are:

<ol>

<li>the producers, publishers, and consumers of literature mounted on the
world wide web,

<li>the perceptions of the stability and quality of electronic
publications by those who hold academic power,

<li>whether or not electronic publications are refereed,

<li>different attitudes towards technology within and between academic
disciplines,

<li>if publications are available in both paper and electronic forms,
and<br> 5a) if the contents differ between forms, and

<li>the ease of access, (this includes access to appropriate computer
technology and also the ability to retrieve relevant or desired electronic
works via indexes and other finding aids.)

</ol>

Some assumptions underlying these issues relate to divergent views of the
Internet as it relates to scholarly production of knowledge. An optimistic
view claims that new technology (as epitomized by graphical,
easy-to-use browsers for the world wide web, coupled with sophisticated
software that can perform citation analysis and intelligently search
through vast amounts of information) will transform scholarly publication
for the better by allowing people to quickly seek out information, respond
to others, publish electronically at a low cost, and ultimately speed up
the typically long cycle of publishing a peer-reviewed article. [See, for
example, <a href="#harnad">Harnad (1995a,b)</a>; <a href="#lanham">Lanham
(1993)</a>; <a href="#odlyzko">Odlyzko (1995)</a>; <a
href="#schwier">Schwier (1994)</a>; <a href="#stodolsky">Stodolsky
(1995)</a>; and <a href="#taubes">Taubes (1996a,b)</a>.] <p>

In contrast, a less optimistic view points out that scholars do not have
time to perform the duties of a publishing house, whether electronic or
not (<a href="#press">Press 1995</a>, <a href="#rowland">Rowland
1995</a>).  Structures and scenarios might price or legislate information
beyond the reach of all but a few individuals or institutions, thus
decreasing access to information (<a href="#ask">"Ask Dr. Internet"
1995</a>), or if not so dire, demand subscription fees to access an
article database thus firewalling articles (<a href="#brent">Brent
1995</a>), or not insuring privacy through anonymous access to files (<a
href="#hickey">Hickey 1995</a>). Some have noted that the library as it's
currently constituted might be bypassed or killed, thus leading to
unemployment or drastic restructuring of the jobs for librarians (<a
href="#cisler">Cisler 1995</a>, <a href="#kling2">Kling and Lamb 1994</a>).
Others note physical harm from the
effects of computer technology, a lack of concern for the environment
resulting from the manufacturing technology of computer hardware and the
increased use of paper (<a href="#fuller2">Fuller 1995b</a>). The future
reality, of course, will compass most of those viewpoints, at least to a
certain extent, (as expressed by more moderate views, such as those of
Fuller generally [<a href="#fuller">1995a, b</a>] and <a
href="#grusin">Grusin 1994</a>). <p>

While the details of the scholarly publication and tenure process are
important in themselves, my interest today is, however, on the
differing theoretical positions that underlie varying stances on the
situation. Perhaps they are symptomatic of a larger debate between
different ideologies or theories. Remember, of course, that
most people
will profess a viewpoint that is not necessarily one extreme or the other,
will often blend arguments from differing stances, and may adapt or modify
viewpoints over time.<a href="#2">2</a> <p>

One approach advocates rational, systematic thought. Things can be
organized, classified, understood, and explained. There are procedures,
logic, rules, and boundaries, plus a sense of direction, and usually a
sense of correct and incorrect, if not actual definitions of right and
wrong. Alternatively, things are more complex. Only one way, (one right
and wrong) may not be enough, since the world is too complex to
understand. Rules based on truths are ultimately futile, since people and
their perceptions resist reduction. Individuals are not unitary actors,
but are decentered collections of roles, feelings, and motivations. The
self is fragmented, multiple, fluid, and nonlinear. What I am describing,
in other words, are conversations between aspects of modernism and
postmodernism (<a href="#lefurgy">LeFurgy 1996</a>; <a href="#sarup">Sarup
1993</a>; <a href="#selden">Selden and Widdowson 1993</a>).<a
href="#3">3</a> <p>

Theories that describe the identity of an electronic author as sets of
roles with diffuse responsibility for production and consumption of
information, though, are in conflict with the current academic reward
system, in which lasting, attributable, individual contributions to the
body of scholarly knowledge are valued by the members of the committees
responsible for recommending and the individuals that ultimately grant
tenure. Despite several studies that claim that electronic publishing is
medium-neutral, the scholarly reward system still rewards individual,
autonomous efforts. For some time to come, the collectivity of individuals
controlling the system probably will not redefine advancement criteria to
take into account certain features of electronic scholarly publishing,
such as easily changeable text revised through public critique and
collaboration. Rather, modes of electronic communication that mirror
established practices will be rewarded for the near future. <p>

A number of the arguments or points of view that are documented in print
and on the Internet (for example, the debate carried on between Stevan
Harnad and Steve Fuller in several forums,) attempt to invoke a particular
model or set of circumstances to assure that e-publication will remain
scholarly and that the current academic stakeholders will still be able to
communicate their ideas and receive the rewards and validation they expect
through publishing on the world wide web.<a href="#4">4</a> According to
this approach, while the Internet in general, and the world wide web
specifically, might be characterized as ever-changing, with as many focal
points as web sites, the area of scholarly publication on the Internet is,
at the moment, trying to institute procedures and to institutionalize
values that replicate the benefits of the scholarly publication
infrastructure, particularly quality control issues; that is, peer review,
while looking to various advantages that electronic publication can offer,
such as speed and delivery of images difficult or impossible in paper. <p>

<a href="#harnad">Stevan Harnad (1995a, b)</a> proposes a model in which he
suggests that scholars
place their preprints in freely-accessible archives in addition to
submitting them to peer-reviewed paper journals. He suggests archiving
preprints once they are accepted for publication. As publishers develop a
presence on the world wide web, print delivery mechanisms would become
less important, and refereed electronic-only journals would prevail. This
seems to be the trend, for example in certain areas of physics.<a
href="#5">5</a> <p>

Electronic journals offer opportunities for innovations or enhancements in
content (for example, <a href="#kling">Kling and Covi 1995</a>; <a
href="#taubes2">Taubes 1996b</a>). A recent issue of <cite>Science</cite>
notes interesting ways that particular journals add hyperlinks to video
and audio features. Other e-journals augment or link individual articles
to relevant discussion forums, related articles, or notification and
alerting services. (<a href="#taubes2">Taubes 1996b</a>) (For example, The
<cite>Journal of Current Clinical Trials</cite>, and <cite>Science
Online</cite> itself create hypertext links between related articles and
letters to the editor.) <p>

Last year, the Association for Computing Machinery (ACM) proposed a plan
that develops "its vision for the future of publication in the electronic
age and a program to achieve it." The ACM is made up of computer
scientists, and the group is moving aggressively into electronic
publishing (<a href="#denning">Denning and Rous 1995</a>). This document
reflects much thought by the ACM, one of the major professional
organizations in computer science, concerning the issue of electronic
publication. The ACM, not surprisingly, is now promoting electronic
publication in general. The Publications Board is moving ACM publications
into electronic-only form, citing breakdowns in the traditional system of
scholarly publication as one reason. The authors take the stance that
articles are collections of objects (individual paragraphs, graphs,
charts, and figures) not simply one textual unit. Hypertext links to other
items can be included, thus they discuss the copyright implications of the
links: they determine that links are citations, thus one need not have
special permission to embed a link in one's work. What is not mentioned
are any materialist arguments, such as any possible negative side or ill
effects of this technology on the physical environment, such as pollution
implications, ergonomic problems, the human and electrical energy needed
to create and maintain the database, or disparity in access to databases.
<p>

Another interesting example is documented by Rob Kling and Lisa Covi (<a
href="#kling">1995</a>). Their article has the provocative title
"Electronic Journals and Legitimate Media in the Systems of Scholarly
Communication." In their discussion of scholarly communication as a
sociotechnical system they examine a range of plausible forms of viable
scholarly publication. Since many current social networks are unsure of,
suspicious, or critical of electronic journals, one journal, the
<cite>Journal of Artificial Intelligence Research</cite> is the "stealth
e-journal of artificial intelligence research" (266), for it offers
authors rapid dissemination of their articles to readers, through article
posting on world wide web, gopher, and FTP servers coupled with
announcements on several Usenet groups. Yet the journal is formatted to
look like a paper journal, so that printouts of articles look like
photocopies from a print journal. An annual print version is offered for
sale, mainly aimed at libraries. Thus the intent of the publishers is to
exploit advantages of electronic media for notification and distribution,
while "simultaneously calming authors' fears of publishing in a
stigmatized electronic medium" (266). <p>

It seems to me that issues surrounding the nature, structure, and value of
the peer review process are the stumbling points to the widespread
acceptance of electronic-only journals. The structures for acceptance,
review and publication of paper-based journals that have an electronic
edition do not seem to be an issue, as e-versions are published in tandem
with paper versions. The system does not seem to be questioned, rather it
is validated.<a href="#6">6</a> While some commentators have questioned the
value or the procedures for peer review on the web (<a
href="#fuller2">Fuller 1995b</a>), others (such as <a
href="#harnad2">Harnad 1995b</a>) don't take issue with it as such or
assume the process itself will migrate relatively unchanged, though
procedural mechanisms will need to adapt to the electronic medium (<a
href="#odlyzko">Odlyzko 1995</a>). <p>

An alternative, though, to the standard process of peer review is a
process whereby articles will be posted along with comments of reviewers.
I've come across several versions of how this mechanism might work,
depending upon whether articles are invited or arrive unsolicited, whether
or not all submitted articles are included in the journal, how reviews are
solicited, if there are processes to fix a version of an article, to what
extent revisions are allowed, and whether or not articles remain easily
accessible online as opposed to being archived or removed (<a
href="#stodolsky">Stodolsky 1995</a>; <a href="#taubes">Taubes 1996a</a>;
<a href="#zenhausern">Zenhausern 1995</a>). <p>

The tension seems to be over the degree of control necessary in a system;
to create a balance among getting new ideas out and allowing a free flow
of ideas among a variety of scholars, not just an elite, while still
managing to preserve consensus and high quality of ideas and information
within a field. While the world wide web or the Internet in general may be
fluid, flexible, and uncontrolled, it appears that the issue of quality
control is important at some point in the electronic publishing process.
An unreviewed "preprint" is still viewed as preliminary research, since it
has not been validated.<a href="#7">7</a> <p>

Several recent articles have systematically examined aspects of electronic
communication as it relates to scholarly communication, publishing, and
the reward system (<a href="#butler">Butler 1994</a>; <a
href="#cronin">Cronin and Overfelt 1995</a>; <a href="#schauder">Schauder
1994</a>). Overall, the authors note that, in general, electronic
communication has facilitated scholarly interaction, by allowing people to
informally talk through electronic mail and listserv discussion groups
and through the sponsorship of online conferences. (<a href="#barry">Barry
1995</a>; <a href="#berge">Berge and Collins 1994</a>; <a
href="#ohaver">O'Haver 1995</a>; and <a href="#ruhleder">Ruhleder 1995</a>
also note these points.) Yet both through the literature and through
anecdotal evidence it appears that more formal means of communication are
still necessary in the reward system of universities. <p>

In the literature on electronic scholarly publishing authors are concerned
that electronic journals, even if refereed, may not be considered as
prestigious as traditional journals that have been around for awhile, in
part because they have been established fairly recently (<a
href="#butler">Butler 1995</a>).  Such journals may not be mainstream and,
in fact, may be experimental, avant-garde, or perhaps even controversial,
thus raising larger political issues within a department or university of
the appropriateness of particular research areas. As mainstream commercial
publishers are trying to cash in on electronic publishing as a way to
further target markets or save or shift costs, this issue may not be a
relevant assessment in certain fields. <p>

These studies note that quantitative research (surveys and interviews)
indicate that in the advancement and tenure process, electronic
publications will be treated similarly to works published in traditional,
paper-based journals.<a href="#8">8</a> Yet the general perception is that
electronic publishing is risky and that in fact, to receive tenure an
assistant professor must play the game according to the traditional rules
in the discipline, department, and at the institution. As more and more
peer-reviewed journals are moving to electronic publication, though,
perceptions and actualities may converge.<a href="#9">9</a> <p>

Certain scholars may suffer disjunction with the rise of electronic
scholarly publishing, since there are implications of impermanence,
fragmentation, dissolution of individual boundaries, and loss of control
over individual endeavors. Alternatively, others may feel that the current
system is too restrictive or not changing quickly enough, since, despite
experimentation and innovation, in general, the established system of
scholarly publication is moving onto the world wide web. Stevan Harnad <a
href="#harnad2">(1995b)</a> suggests that rather than no peer review at
all with resultant problems, it would be better for the existing system,
problems and all, migrate to electronic forums, where it can change and be
modified. (This is, however, one of the points of debate between Fuller
[<a href="#fuller2">1995b</a>] and Harnad [<a href="#harnad">1995a</a>]).
<p>

At present, it seems that the goals of scholarly authors remain the same
as in the past. Authors wish to contribute to the body of scholarly
knowledge in their field and be recognized for it by their peers.
Secondly, authors wish to be recognized for career advancement, especially
in achieving tenure from their institutions (<a href="#arnold2">Arnold
1995</a>; <a href="#harnad">Harnad 1995a;</a> <a href="#tenopir">Tenopir
1995</a>).  These goals remain the same despite the delivery medium.  The
first issue [contributing to the body of knowledge in the field and being
recognized by peers] seems to pose less of a threat to electronic
publications, if the field is moving to e-journals. The problem is one of
perception (<a href="#kling">Kling and Covi 1995</a>), particularly the
mutual perceptions of people on the deciding and receiving sides of the
social system granting rewards. That is, the social structures may be
lagging behind the new modes of communication. <p>

Cyberspace, or the world wide web can embrace all the differing views,
because of the possibilities granted by decentralization and the
imagination. The idea of moving to a piece-based work, being able to
search the discrete fragments of an article, constituted in pieces and
reconstituted through linkages from other pieces seems very post-modern,
though the ACM doesn't use the language of postmodernism in their
publishing plan.<a href="#10">10</a> Moving to a document-based, or even an
object-based conception of a work is a way of moving out of the unit of a
journal issue, volume, or book.  According to the ACM plan,
readers/searchers access the ACM digital library to make a self-created
collection of pieces, bookmarks, or links, that when put together by an
author/reader make a new work. For example, a researcher creates a
bibliography from citations retrieved from an index or indices (either
paper or electronic). Such citations are pointers to other works.
Researchers can embed quotes from a work or cite a standard edition when
giving a reading of a work of literature. These could be considered links
to other texts (that is, intertextuality.) Conceptually this has links to
current and past technology, as well as affinities to conversations about
the changing role of the author (<a href="#arnold2">Arnold 1995</a>; <a
href="#grusin">Grusin 1994</a>; <a href="#lanham">Lanham 1993</a>). <p>

<h2>Summing Up</h2>

While network technology and electronic authorship is changing what we
think of as text, what is a work, and who is an author, print technology
will be with us for a long time to come. As representatives of academic
presses note, the codex format has been around for a long time, thus many
technological features, (such as the ability to make paper and ink that
may be read from many angles and at different light levels) have been
perfected over the last several hundred years, books are portable and
fairly indestructible while computer technology has not yet had the time
to catch up (<a href="#arnold">Arnold 1994</a>; <a href="#mccune">McCune
1995</a>). The infrastructure for creating and distributing scholarly
journals is in place. Yet just as certain types of new programming
developed for television out of older forms of cinema and vaudeville, so
too are new forms unique or better suited to electronic transmissions
being developed.  <p>

For the next few (five to ten?) years, though, depending on one's field, I
would not recommend either giving up print or ignoring the electronic
media. An assistant professor should keep up with or be ahead of his or
her area, which in most fields means being aware of the electronic
writings in the field, whether physics preprint databases, e-journals of
various sorts, (such as <cite>Postmodern Culture</cite>, <cite>The Journal
of Biological Chemistry</cite> or <cite>Psycholoquy</cite>) or U.S.
government documents now being distributed only in electronic form.
However, even if one's department chair is a postmodernist, for getting
tenure, there is no death of the author. It will still be important to
publish more than an adequate amount, in respected journals, or whatever
journals become, whether online or in print, and in certain disciplines,
but not others, to write the book as well. <p>

Looking forward, I believe that since there are more and more scholars,
there will be new outlets for "publication" and communication evolving.
People will fragment themselves intellectually, through interdisciplinary
endeavors as well as through micro-specializations. With more scholars,
professional and avocational, people will cluster topically, through both
formal and informal means of electronic communication, as well as through
more traditional means of professional conferences and publication in
print forums. There is a need to be flexible and open to new
technologies. Just as the creators of content for many new technologies
take their first steps by replicating old formats (films reproduced stage
plays, television variety shows replicated vaudeville,) current electronic
publishing is replicating print to a great degree. I suggest that as we move
away from replicating print we don't lose sight of the strengths of the
print/paper media, but people should go beyond replicating paper-based
products on the world wide web. <p>

Still, to achieve tenure, young scholars need to push the limits of
scholarship in format as well as content along with the tenured scholars
that have the freedom to experiment with little fear of censure. Yet we
must do so in a way that educates those scholars unfamiliar with or
suspicious of cyberspace rather than leaving them behind. From the point
of view of a scholar who may hold the varied roles of author, editor, and
reader, the decisions that are economically best for an academic journal
may not be the most useful delivery and pricing schemes for a scholarly
reader. For an author, though, personal goals will be to produce
scholarly information quickly that will be widely and quickly accessible,
allowing him or her to get on with new research while spending the least
amount of time on production processes. <p>

I would hope that the social system of the peer-review process, especially
in research universities, will adapt to value contributions other than the
scholarly work as currently defined, or that the reward system itself will
change. Since the promotion and tenure process is essentially a
socioeconomic system, I think that there will continue to be importance
placed on establishing value. Yet, one of the strengths of the world wide
web is the ability for users to compile media in new documents and to
relate diverse works through hyperlinks. Thus it would be a pity if the
system did not encourage innovative uses of the electronic technology.
With the ability to modify or change one's work, the sequential roles of
author and reader blend into simultaneous roles of
creator/compiler/commenter. Since these roles appear to have less
"authority" than a single personality offering specific works, the current
system will have to adapt or be replaced. <p>

Even if one views the world wide web and the Internet in general as a
post-modern phenomenon, the counter-tendency will be to either try to
impose order and control, or to make sense through describing, listing, or
organizing and judging. People will continue to create lists of useful links
that we've all seen on many web sites, or to create search engines that
offer judgment through selection criteria or evaluative rankings, or to
carve out a corner of the world wide web for peer review (<a
href="#harnad2">Harnad 1995b</a>).  It's noted that forms of communication
alter discourse (<a href="#arnold2">Arnold 1995</a>). Since hierarchical
modes of print are moving to a more horizontally organized structure of
the world wide web, relationships of ownership and power are changing. It
will be interesting to see how the balance shifts.

<p>
<hr width=200>
<p>
<h2><a name="1">Endnotes</a></h2>
<ol>

<li>I thank Professor Sue-Ellen Case and an anonymous reader for their
comments on earlier versions of this work.

<li><a name="2">Some</a> authors have focused on social/theoretical
aspects of electronic publication. See, for example, <a
href="#amiran">Amiran et al. (1992)</a>; <a href="#bishop">Bishop
(1995)</a>; <a href="#grusin">Grusin (1994)</a>; <a href="#hales">Hales
(1993)</a>;  <a href="#kling2">Kling and Lamb (1994)</a>; and <a
href="#lanham">Lanham (1993)</a>.

<li><a name="3">These</a> characterizations of modernism and postmodernism
are derived from several sources; I have heavily borrowed from the
description by <a href="#lefurgy">Bill LeFurgy</a> (in his "In CinC"
column in Culture in Cyberspace, 1(8) (April 1, 1996)
[{http://www.radix.net/~wlefurgy/cinc08.htm}] describing
an explanation of postmodernism in Life on the Screen by Sherry Turkle <a
href="http://www.mit.edu:8001/people/sturkle/">http://www.mit.edu:8001/people/sturkle/</a>). 

<li><a name="4">Harnad's</a> writings can be found at [{<a href="http://www.ecs.soton.ac.uk/~harnad/intpub.html">http://www.ecs.soton.ac.uk/~harnad/intpub.html</a>} or {ftp://ftp.princeton.edu/pub/harnad/Harnad}]; several of Fuller's are in <cite>Information Society</cite>. 11(4) 1996. 

<li><a name="5">See</a> <a
href="http://xxx.lanl.gov">http://xxx.lanl.gov</a> for the automated
physics e-print archives and <a href="#ginsparg">Ginsparg (1996)</a> for an
explanation.

<li><a name="6">For</a> example: "<cite>EJournal</cite> is a peer-reviewed, academic periodical."  {http://rachel.albany.edu/~ejournal/ejournal.html}) and "<cite>Psycoloquy</cite> is a refereed electronic journal sponsored by the American Psychological Association." ({http://www.princeton.edu/~harnad/psyc.html</a>}) and "<cite>Electronic Publishing-Origination, Dissemination, and Design</cite> (EP-ODD) is a refereed academic journal that sets out to explore the huge potential of electronic publishing." ({http://www.ep.cs.not.ac.uk/wiley/journals/epo}) 

<li><a name="7">An</a> interesting example is in <a
href="#hitchcock">Hitchcock et al. (1996)</a>, in which the authors
justify presenting a non-peer-reviewed work on the basis of the currency
and relevance of their work. They note that they might put an abridged
version of the work in a refereed journal. In addition they solicit
comments on this online version to serve as "a sort of open peer
commentary." <a href="http://journals.ecs.soton.ac.uk/survey/survey.html">
http://journals.ecs.soton.ac.uk/survey/survey.html</a>  

<li><a name="8">Am</a> I eliding the people writing, reading, and
evaluating here by using passive voice and scholarly language conventions?

<li><a name="9">Since</a> I found these articles in print-media, you may
wonder if things have changed in the year since they have been published
and the longer time since they have been written, as a result of the rapid
changes in electronic publication and general acceptance of the world wide
web. Recall, though, that we are dealing with a system that changes
slowly. I recently checked with a few faculty colleagues in the sciences
and humanities, who say that individual contribution (or attributable
contribution in co-authored articles) to respected, peer-reviewed journals
(or writing books in the humanities) is still the major criterion for
advancement.

<li><a name="10">Could</a> we think of this rhyzomatically? both of the work, and by extension, the author?
</ol>
<p>
<hr width=200>
<p>

<h2>References</h2>

<a name="amiran">
<strong>Amiran, Eyal, John Unsworth, and Carole Chaski</strong>.</a> 1992.
"Networked Academic Publishing and the Rhetorics of its Reception."
<cite>Centennial Review</cite> 36 (1): 43-58. <P>

<a name="arnold"> <strong>Arnold, Kenneth.</strong></a> 1994. "The
Electronic Librarian Is a Verb/ The Electronic Library Is Not a Sentence."
<cite>Journal of Electronic Publishing</cite>.  [<a
href="http://www.press.umich.edu/jep/works/arnold.eleclib.html">http://www.press.umich.edu/jep/works/arnold.eleclib.html</a>]
"A lecture delivered at the New York Public Library, The Gilbert A. Cam
Memorial Lecture Series, October 14, 1994." (forthcoming in
<cite>Biblion</cite>) <p>

<a name="arnold2">
<strong>Arnold, Kenneth.</strong></a> 1995. "Virtual Transformations: The
Evolution of Publication Media." <cite>Library Trends</cite> 43(4):
609-626. <p>

<a name="ask"> <strong>"Ask Dr. Internet."</strong></a> [{http://promo.net/drnet/drnet09.htm}]
September 1995. <p>

<a name="barry">
<strong>Barry, C.A.</strong></a> 1995. "Critical Issues in Evaluating the
Impact of IT on Information Activity in Academic Research: Developing a
Qualitative Research Solution." <cite>Library &amp; Information Science
Research</cite> 17(2): 107-134. <p>

<a name="berge">
<strong>Berge, Z.L.; Collins, M.</strong></a> 1994. "Life on the Net."
<cite>EDUCOM Review</cite>, 29(2): 11-14. <p>

<a name="bishop">
<strong>Bishop, Ann Peterson.</strong></a> 1995 <cite>Library Trends</cite>.
43(4): 544-70. <p>

<a name="brent">
<strong>Brent, Doug</strong></a>. 1995. "Stevan Harnad's "Subversive
Proposal": Kick-Starting Electronic Scholarship -- A Summary and
Analysis. <cite>Information Society</cite>. 11(4): 275-283.
<p>

<a name="butler">
<strong>Butler, H. Julene.</strong></a> 1994. "Where Does Scholarly
Electronic Publishing Get You?" <cite>Journal of Scholarly
Publishing</cite>. 26(4):  174-186. <p>

<a name="cisler">
<strong>Cisler, Steve.</strong></a> 1995. "The Post Office and Public
Libraries." <cite>Library Journal</cite> 120(5):29-30. <P>

<a name="cronin">
<strong>Cronin, Blaise and Kara Overfelt.</strong></a> 1995. "E-Journals and
Tenure." <cite>Journal of the American Society for Information
Science</cite>. 46(9): 700-703. <p>

<a name="denning"> <strong>Denning, Peter J. and Bernard Rous</strong>.</a> "The ACM Electronic Publishing Plan." [<a href="http://www.acm.org/pubs/epub_plan.html">http://www.acm.org/pubs/epub_plan.html</a>]. 1995 by ACM, Inc. In the web version of this article the authors state that it is a corrected version of that published in the Communications of the ACM. 38(4): 97-103 (April 1995).. Related documents discuss copyright in more detail: the "ACM Interim Copyright Policies" [{http://www.acm.org/pubs/copyright_policy/}], also in Communications of the ACM. 38:4 (April 1995), 104-107) and the "ACM Author's Guide to the Interim Copyright Policies" [{http://www.acm.org/pubs/copyright_policy/author_guide_cr.html}], also in Communications of the ACM. 38:4 (April 1995), 107-109.) <p>

<a name="fuller">
<strong>Fuller, Steve.</strong></a> 1995a. "Cybermaterialism, or Why There is
no Free Lunch in Cyberspace." <cite>Information Society</cite> 11(4):
325-332. <p>

<a name="fuller2">
<strong>Fuller, Steve.</strong></a> 1995b. "Cyberplatonism: An Inadequate
Constitution for the Republic of Science." <cite>Information
Society</cite>. 11(4): 293-303. <p>

<a name="ginsparg">
<strong>Ginsparg, P.</strong></a> "Winners and Losers in the Global Research Village." [{<a
href="http://arxiv.org/blurb/pg96unesco.html">http://arxiv.org/blurb/pg96unesco.html</a>}] (1996). <p>

<a name="grusin">
<strong>Grusin, Richard.</strong></a> 1994. "What is an Electronic Author?
Theory and the Technological Fallacy." <cite>Configurations</cite> 2(3):
469-483. <p>

<a name="hales">
<strong>Hales, N. Katherine.</strong></a> 1993. "Virtual Bodies and
Flickering Signifiers." <cite>October</cite> 66: 69-91. <p>

<a name="harnad">
<strong>Harnad, Stevan.</strong></a> 1995a. "The PostGutenberg Galaxy:
How to Get There from Here." <cite>Information Society</cite>. 11(4):
285-291. <p>

<a name="harnad2">
<strong>Harnad, Stevan.</strong></a> 1995b. "Sorting the Esoterica from the
Exoterica: There's Plenty of Room in Cyberspace." <cite>Information
Society</cite>. 11(4): 305-324. <P>

<strong>Helstien, B.A.</strong> 1995. "Libraries: Once and Future."
<cite>Electronic Library</cite> 13(3): 203-207. <p>

<a name="hickey">
<strong>Hickey, Thomas.</strong></a> 1995. "Present and Future
Capabilities of the Online Journal." <cite>Library Trends</cite>. 43(4):
528-543. <p>

<a name="hitchcock">
<strong>Hitchcock, Steve.</strong></a> "A Survey of STM Online Journals 1990-
1995:The Calm Before the Storm."
[<a
href="http://journals.ecs.soton.ac.uk/survey/survey.html">http://journals.ecs.soton.ac.uk/survey/survey.html</a>]. 15
January 1996, updated 14 February. <p>

<a name="kling">
<strong>Kling, Rob; Covi, Lisa.</strong></a> 1995. "Electronic Journals and
Legitimate Media in the Systems of Scholarly Communication."
<cite>Information Society</cite>. 11(4): 261-271.
<p>

<a name="kling2"> <strong>Kling, Rob; Lamb, Roberta.</strong></a> 1994.
"Envisioning Electronic Publishing and Digital Libraries: How Genres of
Analysis Shape the Character of Alternative visions."
[{http://asearch.mccmedia.com/public/libraries/genres}]
(1994). To appear in: <cite>Academia and Electronic Publishing:
Confronting the Year 2000.</cite> Ed Robin P. Peek, Gregory Newby, Lois
Lunin. Cambridge, MA: MIT Press. <p>

<a name="lanham">
<strong>Lanham, Richard A.</strong></a> 1993 <cite>The Electronic Word:
Democracy, Technology, and the Arts.</cite> Chicago: University of Chicago
Press. <p>

<a name="lefurgy"> <strong>LeFurgy, Bill.</strong></a> "In CinC"
<cite>Culture in Cyberspace</cite>. 1(8),
[{http://www.radix.net/~wlefurgy/cinc08.htm}] <p>

<a name="mccune">
<strong>McCune, David.</strong></a> Guest lecture and discussion, Comparative
Literature 283, University of California, Riverside. November 28, 1995.
<p>

<a name="odlyzko">
<strong>Odlyzko, Andrew M.</strong></a> 1995. "Tragic Loss or Good Riddance?
The Impending Demise of Traditional Scholarly Journals." <cite>International
Journal of Human-Computer Studies</cite> 42: 71-122. <p>

<a name="ohaver">
<strong>O'Haver, T.C.</strong></a> 1995 "CHEMCONF: An Experiment in
International Online Conferencing. <cite>Journal of the American Society
for Information Science</cite>, 46(8): 611-613. <p>

<a name="press">
<strong>Press, Larry.</strong></a> 1995 "McLuhan Meets the Net."
<cite>Communications of the ACM</cite> 38(6): 15-20. <p>

<a name="rowland">
<strong>Rowland, Fytton.</strong></a> 1995. "Electronic Journals: Neither
Free nor Easy." <cite>Information Society</cite>. 11(4): 273-274. <p>

<a name="ruhleder">
<strong>Ruhleder, K.</strong></a> 1995. "Computerization and Changes to
Infrastructures for Knowledge Work." <cite>Information Society</cite>
11(2): 131-144. <p>

<a name="sarup">
<strong>Sarup, Madan.</strong></a> 1993. <cite>An Introductory Guide to
Post-Structuralism and Postmodernism.</cite> 2nd ed. Athens: University
of Georgia Press. <p>

<a name="schauder">
<strong>Schauder, Don.</strong></a> 1994. "Electronic Publishing of
Professional Articles: Attitudes of Academics and Implications for the
Scholarly Communication Industry." <cite>Journal of the American Society
for Information Science</cite>. 45(2): 73-100. <p>

<a name="schwier">
<strong>Schwier, Richard A.</strong></a> 1994. <cite>Some Reflections on
Scholarly Review and Academic Publication</cite>. 13 p. ERIC microfiche
collection, ED368317 <p>

<a name="selden">
<strong>Selden, Raman; Widdowson, Peter.</strong></a> 1993. <cite>A Reader's
Guide to Contemporary Literary Theory</cite>. 3rd ed. Lexington:
University of Kentucky Press. <p>

<a name="stodolsky">
<strong>Stodolsky, David S.</strong></a> 1995. "Consensus Journals:
Invitational Journals Based on Peer Review." <cite>Information
Society</cite>. 11(4): 247-260. <p>

<a name="taubes">
<strong>Taubes, Gary.</strong></a> 1996a. "Electric Preprints Point the way to
'Author Empowerment'." <cite>Science</cite> 271(9): 767-768. <p>

<a name="taubes2">
<strong>Taubes, Gary.</strong></a> (1996b). "Science Journals Go Wired."
<cite>Science</cite> 271(9): 764-766. <p>

<a name="tenopir">
<strong>Tenopir, Carol.</strong></a> 1995. "Authors and Readers: The Keys to
Success or Failure for Electronic Publication." <cite>Library
Trends</cite> 43(4) 571-591. <p>

<a name="zenhausern"> <strong>Zenhausern, Robert.</strong></a> ["JUPR on
Listserv@sjuvm.stjohns.edu"] found through the HyperJournal contents page:
[{http://www.gold.ac.uk/history/hyperjournal/contents.htm}]
(1995) the JUPR announcement is listed under
[{http://www.gold.ac.uk/history/hyperjournal/jupr.htm}]
(1995) This item is an announcement of the Journal of Universal Peer
Review, available through listserv. Dr. Zenhausern is the owner of the
list. 


<P> <A HREF="http://validator.w3.org/"><IMG SRC="http://www.library.ucsb.edu/gallery/valid_html3.2.gif" ALT="HTML 3.2 Checked!" border=0></A>

</body></html>
