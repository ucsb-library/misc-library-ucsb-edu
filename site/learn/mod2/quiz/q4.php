<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 4;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q4 == "" && $next_question == 5){
    $err_msg = "Please select a choice";
    $next_question = 4;
}

//Print and record question results
if ($next_question == 5){
    $correct = 0;
    $msg = "<h4>Answer to Question Four:</h4>\n";
    if ($q4 == "books") {
        $msg .= "<b>A:</b> No. The records for books, magazines, and newspapers are in Pegasus.<br>";
    }
    if ($q4 == "articles") {
        $msg .= "<b>B:</b> You are correct. Articles are <b>not</b> found in Pegasus, the Library catalog. You'll need to use 
								an article index to identify an article on a topic, then look up the journal 
								it is found in using Pegasus.<br>";
								$correct = 1; 
    } 
    if ($q4 == "magazines") {
    $msg .= "<b>C:</b> 
        No. The records for books, magazines, and newspapers are in Pegasus.<br>";
    }
    if ($q4 == "newspapers") {
        $msg .= "<b>D:</b> 
        No. The records for books, magazines, and newspapers are in Pegasus.<br>";
    }

    $response = $_COOKIE["response"];
    $response .= "[Question_4: ".$q4."], ";
    setcookie ("response", $response);
    setcookie ("next", "5");
    setcookie ("ans[3]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 5</a></div></p>";
}

?>

<h2>Question Four</h2>
<h3>
Which one of these records is NOT found in Pegasus, the Library catalog? (Choose one)
</h3>
<form method="POST" action="q4.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q4 == "books"):
        print "<input type=\"radio\" name=\"q4\" value=\"books\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"books\">";
    endif;
    ?>
    A. Books
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q4 == "articles"):
        print "<input type=\"radio\" name=\"q4\" value=\"articles\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"articles\">";
    endif;
    ?>
    B. Articles
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q4 == "magazines"):
        print "<input type=\"radio\" name=\"q4\" value=\"magazines\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"magazines\">";
    endif;
    ?>
    C. Magazines
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q4 == "newspapers"):
        print "<input type=\"radio\" name=\"q4\" value=\"newspapers\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"newspapers\">";
    endif;
    ?>
    D. Newspapers
  </tr>
   </table>
<p>
<input type="hidden" name ="next_question" value="5">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>