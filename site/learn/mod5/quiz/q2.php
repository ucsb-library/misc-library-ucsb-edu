<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 2; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q2 = $_REQUEST["q2"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q2 == "" && $next_question == 3){
    $err_msg = "Please select a choice";
    $next_question = 2;
}
//Print and record question results
if ($next_question == 3){
    $correct = 0;
    $msg = "<h4>Answer to Question Two:</h4>\n";
    if ($q2 == "true") {
        $msg .= "<b>A:</b> Yes, there are guides <b>'By Subject'</b> to many types of resources for your research.
								They are listed under <b>'Research'</b> on the Libraries home page.<br>";
        $correct = 1;
    } else {
    $msg .= "<b>B:</b> No, sorry. There are many subject guides. They are listed under <b>'Research'</b> on the Libraries homepage.
				You can easily choose a guide to a subject disipline everything from Anthropology to Women's Studies.<br>";
}
    $response = $_COOKIE["response"];
    $response .= "[Question_2: ".$q2."], ";
    setcookie ("response", $response);    
    setcookie ("next", "3");
    setcookie ("ans[1]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 3</a></div></p>";
}    
?>
<h2>Question Two</h2>
<h3>
The UCSB Libraries provide subject guides on the Web. These include guides for Business, <br>Engineering, Philosophy, 
and many other subjects. (Choose one)                                                                                                                                                                           
</h3>
<form method="POST" action="q2.php">
<div align="CENTER">
  <table border="0" width="60%">
  <tr> 
    <td>
    <?php
    if ($q2 == "true"):
        print "<input type=\"radio\" name=\"q2\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q2\" value=\"true\">";
    endif;
    ?>
				A. True
    </td>
  </tr>
  <tr> 
    <td>    
    <?php
    if ($q2 == "false"):
        print "<input type=\"radio\" name=\"q2\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q2\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="3">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
