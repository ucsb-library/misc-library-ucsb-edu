<?php
ob_start();
require_once "../../quiz_header.php";
$question = 2; //used in quiz_header.php
foreach ($_REQUEST as $a=>$b){$$a=$b;}

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
$next_question = 3;
}

//Check that at least one response has been entered
if ($q2 == "" && $next_question == 3){
    $err_msg = "Please select a choice";
    $next_question = 3;
}
//Print and record question results
if ($next_question == 3){
    $correct = 0;
    $msg = "<font color=#820707><h4>Answer to Question Two:</h4></font>\n";
    if ($q2 == "journal") {
        $msg .= "<b>A:</b> No, because not everyone will talk about the same topic using the exact same words, it is best to identify first the keywords of your topic and then come up with synonyms.";
    }
    if ($q2 == "pegasus") {
        $msg .= "<b>B:</b> No, because not everyone will talk about the same topic using the exact same words, it is best to identify first the keywords of your topic and then come up with synonyms.<br>";
    }
    if ($q2 == "web") {
    $msg .= "<b>C:</b>
        No, because not everyone will talk about the same topic using the exact same words, it is best to identify first the keywords of your topic and then come up with synonyms. <br>";
        $correct = 1;
    }
    if ($q2 == "magazine") {
        $msg .= "<b>D:</b>
        Yes, good job!  Because not everyone will talk about the same topic using the exact same words, it is best to identify first the keywords of your topic and then come up with synonyms.<br>";
				}
    $response = $_COOKIE["response"];
    $response .= "[Question_2: ".$q2."], ";
    setcookie ("response", $response);
    setcookie ("next", "3");
    setcookie ("ans[4]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 3</a></div></p>";
}

?>

<h2>Question Two</h2>
<h3>When searching a library database, you should:</h3>
<form method="POST" action="q2.php">
<div align="CENTER">
<table border="0" width="75%" cellspacing="2" cellpadding="2">
 <tr>
    <td>
    <?php
    if ($q2 == "journal"):
        print "<input type=\"radio\" name=\"q2\" value=\"journal\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"journal\">";
    endif;
    ?>
    A. type the research question into the search bar and see what results come up
  </tr>

		<tr>
    <td>
    <?php
    if ($q2 == "pegasus"):
        print "<input type=\"radio\" name=\"q2\" value=\"pegasus\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"pegasus\">";
    endif;
    ?>
    B. choose words from the subject terms and use those to search
  </tr>

		<tr>
    <td>
    <?php
    if ($q2 == "web"):
        print "<input type=\"radio\" name=\"q2\" value=\"web\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"web\">";
    endif;
    ?>
    C. identify the keywords in the research question and type those into the search bar
  </tr>

		<tr>
    <td>
    <?php
    if ($q2 == "magazine"):
        print "<input type=\"radio\" name=\"q2\" value=\"magazine\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"magazine\">";
    endif;
    ?>
    D. find the keywords in the research question and come up with synonyms to type into the search bar
  </tr>

  </table>
<p>
<input type="hidden" name ="next_question" value="3">
<?php
    if ($msg == ""){
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    }
?>
</p>
</div>
</form>

<?php
//Print error message or results
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>