<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 7; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q7 = $_REQUEST["q7"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q7 == "" && $next_question == 8){
    $err_msg = "Please select a choice";
    $next_question = 7;
}
//Print and record question results
if ($next_question == 8){
    $correct = 0;
    $msg = "<h4>Answer to Question Seven:</h4>\n";
    if ($q7 == "true") {
        $msg .= "No, actually they don't. Each search engine uses software to compile its own database of pages found 
								on the Web, and has its own formula or algorithm for selection. 
								So each search engine will offer different results.<br>";
    } else {
    $msg .= "You're right! Each search engine uses software to compile its own database of pages 
				found on the Web, and has its own formula or algorithm for selection. 
				So each search engine will offer different results.<br>";
				$correct = 1;
			}
    $response = $_COOKIE["response"];
    $response .= "Question_7: ".$q7."], ";
    setcookie ("response", $response);    
    setcookie ("next", "8");
    setcookie ("ans[6]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 8</a></div></p>";
}    
?>
<h2>Question Seven</h2>
<h3>All search engines return the same results. (Choose one)</h3>
<form method="POST" action="q7.php">
<div align="CENTER">
  <table border="0" width="25%">
  <tr> 
    <td>
    <?php
    if ($q7 == "true"):
        print "<input type=\"radio\" name=\"q7\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"true\">";
    endif;
    ?>
				A. True
    </td>
  </tr>
  <tr> 
    <td>    
    <?php
    if ($q7 == "false"):
        print "<input type=\"radio\" name=\"q7\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="8">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
