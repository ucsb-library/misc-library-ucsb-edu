<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 5;
//$next_question = $_REQUEST["next_question"];
//$q5 = $_REQUEST["q5"];
//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q5 == "" && $next_question == 6){
    $err_msg = "Please select a choice";
    $next_question = 5;
}
//Print and record question results
if ($next_question == 6){
    $correct = 0;
    $msg = "<h4>Answer to Question Five:</h4>\n";
    if ($q5 == "title") {
        $msg .= "<b>A:</b> The answer is B. Using &quot;medical marijuana AND ethics&quot; will give you the best results for your topic.<br>";
    }
    if ($q5 == "journal") {
        $msg .= "<b>B:</b> Yes, good job!  Using &quot;medical marijuana AND ethics&quot; will give you the best results for your topic<br>";
								$correct = 1;
    }
    if ($q5 == "keyword") {
    $msg .= "<b>C:</b> The answer is B. Using &quot;medical marijuana AND ethics&quot; will give you the best results for your topic.<br>";
    }
    if ($q5 == "subject") {
        $msg .= "<b>D:</b> The answer is B. Using &quot;medical marijuana AND ethics&quot; will give you the best results for your topic.<br>";
    }

    $response = $_COOKIE["response"];
    $response .= "Question_5: ".$q5."], ";
    setcookie ("response", $response);
    setcookie ("next", "6");
    setcookie ("ans[4]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to Searchpath Quiz Results: Module 1
</a></div></p>";
}
?>

<h2>Question Five</h2>
<h3>You�ve just chosen to write a research paper on: &quot;The ethics of medical marijuana.&quot; Identify the best strategy of your search.</h3>
<form method="POST" action="q5.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr>
    <td>
				<?php
    if ($q5 == "title"):
        print "<input type=\"radio\" name=\"q5\" value=\"title\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q5\" value=\"title\">";
    endif;
    ?>
    A. Medical AND marijuana
  </tr>
  <tr>
    <td>
   <?php
    if ($q5 == "journal"):
        print "<input type=\"radio\" name=\"q5\" value=\"journal\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q5\" value=\"journal\">";
    endif;
    ?>
    B. Medical marijuana AND ethics
	  </tr>
  <tr>
    <td>
   <?php
    if ($q5 == "keyword"):
        print "<input type=\"radio\" name=\"q5\" value=\"keyword\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q5\" value=\"keyword\">";
    endif;
    ?>
    C. Alternative medicines AND ethics
  </tr>
  <tr>
    <td>
    <?php
    if ($q5 == "subject"):
        print "<input type=\"radio\" name=\"q5\" value=\"subject\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q5\" value=\"subject\">";
    endif;
    ?>
    D. Marijuana AND ethics
  </tr>

  </table>
<p>
<input type="hidden" name ="next_question" value="6">
<?php
    if ($msg == ""){
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    }
?>
</p>
</div>
</form>
<?php
//Print error message or results
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
