<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q1 == "" && $next_question == 2){
    $err_msg = "Please select a choice";
    $next_question = 1;
}
//Print and record question results
if ($next_question == 2){
    $correct = 0;
    $msg = "<h4>Answer to Question One:</h4>\n";
    if ($q1 == "date") {
        $msg .= "<b>A:</b> You're right that the date of publication is provided, but so is other important information.
								The record in an article index provides the information that is essential for locating 
								the article: its author, its title, the name of the periodical in which it is published, date
								of publication, and sometimes an abstract or summary of the article.<br>";
    }
    if ($q1 == "periodical_name") {
        $msg .= "<b>B:</b> You're right that the name of the periodical is included, but so is other important information.
        The record in an article index provides the information that is essential for locating 
								the article: its author, its title, the name of the periodical in which it is published, date
								of publication, and sometimes an abstract or summary of the article.<br>";
				} 
    if ($q1 == "author_name") {
    $msg .= "<b>C:</b> You're right that the author's name is included, but so is other important information.
        The record in an article index provides the information that is essential for locating 
								the article: its author, its title, the name of the periodical in which it is published, date
								of publication, and sometimes an abstract or summary of the article.<br>";
    }
    if ($q1 == "all") {
        $msg .= "<b>D:</b> Absolutely correct! The record in an article index provides the information that is essential for locating
								the article: its author, its title, the name of the periodical in which it is published, date
								of publication, and sometimes an abstract or summary of the article. <br>";
								$correct = 1; 
    }
	
    $response = $_COOKIE["response"];
    $response .= "[Question_1: ".$q1."], ";
    setcookie ("response", $response);
    setcookie ("next", "2");
    setcookie ("ans[0]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 2</a></div></p>";
}
?>

<h2>Question One</h2>
<h3>The record for an article in an index will provide the following information: (Choose one)</h3>
<form method="POST" action="q1.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q1 == "date"):
        print "<input type=\"radio\" name=\"q1\" value=\"date\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"date\">";
    endif;
    ?>
    A. The date it was published 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q1 == "periodical_name"):
        print "<input type=\"radio\" name=\"q1\" value=\"periodical_name\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"periodical_name\">";
    endif;
    ?>
    B. The name of the periodical in which the article appears 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q1 == "author_name"):
        print "<input type=\"radio\" name=\"q1\" value=\"author_name\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"author_name\">";
    endif;
    ?>
    C. The author's name 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q1 == "all"):
        print "<input type=\"radio\" name=\"q1\" value=\"all\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"all\">";
    endif;
    ?>
    D. All of the above
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="2">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
