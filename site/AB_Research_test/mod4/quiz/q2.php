<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q2 == "" && $next_question == 3){
    $err_msg = "Please select a choice";
    $next_question = 2;
}
//Print and record question results
if ($next_question == 3){
    $correct = 0;
    $msg = "<h4>Answer to Question Two:</h4>\n";
    if ($q2 == "biological") {
        $msg .= "<b>A:</b> No, sorry. Here the most appropriate index is Business Source Complete, because the
								topic is about buying and selling using the Internet. Biology Digest
								provides journal citations in biology; Pegasus does not
								index articles; and Humanities Abstracts indexes journals in such disciplines as art,
								philosophy, and religion. <br>";
    }
    if ($q2 == "Pegasus") {
        $msg .= "<b>B:</b> No, sorry. Here the most appropriate index is Business Source Complete, because the
								topic is about buying and selling using the Internet. Pegasus does not index articles;
								Biology Digest provides journal citations in areas such as agriculture
								and biology; and Humanities Abstracts indexes journals in such disciplines as art,
								philosophy, and religion.<br>";
				} 
    if ($q2 == "business") {
    $msg .= "<b>C:</b> Right. Business Source Complete is correct, because the topic is about buying
				and selling using the Internet, a business topic.<br>";
    $correct = 1; 
				}
    if ($q2 == "humanities") {
        $msg .= "<b>D:</b> No, sorry. Here the most appropriate index is Business Source Complete because the
        topic is about buying and selling using the Internet. Humanities Abstracts indexes
								journals in such disciplines as art, philosophy, and religion; Biology Digest 
provides journal citations in biology;	and Pegasus does not index articles. <br>";
    }
	
    $response = $_COOKIE["response"];
    $response .= "Question_2: ".$q2."], ";
    setcookie ("response", $response);
    setcookie ("next", "3");
    setcookie ("ans[1]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 3</a></div></p>";
}
?>

<h2>Question Two</h2>
<h3>
You need to find articles about the use of digital signatures to buy products on the Internet. 
Which one of the following indexes or catalogs would be the most appropriate source to use? (Choose one)
</h3>
<form method="POST" action="q2.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q2 == "biological"):
        print "<input type=\"radio\" name=\"q2\" value=\"biological\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q2\" value=\"biological\">";
    endif;
    ?>
    A. Biology Digest
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q2 == "Pegasus"):
        print "<input type=\"radio\" name=\"q2\" value=\"Pegasus\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q2\" value=\"Pegasus\">";
    endif;
    ?>
    B. Pegasus
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q2 == "business"):
        print "<input type=\"radio\" name=\"q2\" value=\"business\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q2\" value=\"business\">";
    endif;
    ?>
    C. Business Source Complete
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q2 == "humanities"):
        print "<input type=\"radio\" name=\"q2\" value=\"humanities\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q2\" value=\"humanities\">";
    endif;
    ?>
    D. Humanities Abstracts
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="3">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
