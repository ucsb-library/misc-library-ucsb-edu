<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 1;
}
//Check that at least one response has been entered
if ($q1 == "" && $next_question == 2){
    $err_msg = "Please select a choice";
    $next_question = 1;
}
//Print and record question results
if ($next_question == 2){
    $correct = 0;
    $msg = "<font color=#820707><h4>Answer to Question One:</h4></font>\n";
    if ($q1 == "learning") {
        $msg .= "<b>A:</b>
        The answer is B. The items in Academic Search Complete come from everywhere, not just the UCSB Library.<br>";
    }
    if ($q1 == "savvy") {
        $msg .= "<b>B:</b> Yes, good job!  The items in Academic Search Complete come from everywhere, not just the UCSB Library. <br>";
								$correct = 1;
    }
    if ($q1 == "library") {
    $msg .= "<b>C:</b>
        The answer is B. The items in Academic Search Complete come from everywhere, not just the UCSB Library.<br>";
    }
    if ($q1 == "magazine") {
        $msg .= "<b>D:</b>
        The answer is B. The items in Academic Search Complete come from everywhere, not just the UCSB Library.<br>";
    }

    $response = $_COOKIE["response"];
    $response .= "[Question_1: ".$q1."], ";
    setcookie ("response", $response);
    setcookie ("next", "2");
    setcookie ("ans[2]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 2</a></div></p>";
}

?>

<h2>Question One</h2>
<h3>Which statement about Academic Search Complete is false?</h3>
<form method="POST" action="q1.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr>
    <td>
				<?php
    if ($q1 == "learning"):
        print "<input type=\"radio\" name=\"q1\" value=\"learning\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q1\" value=\"learning\">";
    endif;
    ?>
    A. Academic Search Complete allows you to search for articles by subject.
  </tr>
  <tr>
    <td>
   <?php
    if ($q1 == "savvy"):
        print "<input type=\"radio\" name=\"q1\" value=\"savvy\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q1\" value=\"savvy\">";
    endif;
    ?>
    B. Academic Search Complete only indexes items owned by the UCSB library.
  </tr>
  <tr>
    <td>
   <?php
    if ($q1 == "library"):
        print "<input type=\"radio\" name=\"q1\" value=\"library\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q1\" value=\"library\">";
    endif;
    ?>
    C. Academic Search Complete always contains citations and sometimes also includes abstracts or full text.
  </tr>
  <tr>
    <td>
    <?php
    if ($q1 == "magazine"):
        print "<input type=\"radio\" name=\"q1\" value=\"magazine\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q1\" value=\"magazine\">";
    endif;
    ?>
    D. Academic Search Complete includes articles from scholarly journals, popular magazines, newspapers, and more.
  </tr>

  </table>
<p>
<input type="hidden" name ="next_question" value="2">
<?php
    if ($msg == ""){
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    }
?>
</p>
</div>
</form>
<?php
//Print error message or results
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>