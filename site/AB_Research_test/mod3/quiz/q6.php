<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 6; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q6 = $_REQUEST["q6"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q6 == "" && $next_question == 7){
    $err_msg = "Please select a choice";
    $next_question = 6;
}
//Print and record question results
if ($next_question == 7){
    $correct = 0;
    $msg = "<h4>Answer to Question Six:</h4>\n";
    if ($q6 == "true") {
        $msg .= "<b>A:</b> Yes, because these words are usually not included in the catalog entry for the book if they are the first word of the title.<br>";
        $correct = 1;
    } else {
    $msg .= "<b>B:</b> No. You <b>do</b> need to leave off <i>a</i>, <i>an</i>, and <i>the</i> when
				they begin a title.<br>";
}
    $response = $_COOKIE["response"];
    $response .= "[Question_6: ".$q6."], ";
    setcookie ("response", $response);    
    setcookie ("next", "7");
    setcookie ("ans[5]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 7</a></div></p>";
}    
?>
<h2>Question Six</h2>
<h3> When entering a title of a book you leave off <em>a</em>, <em>an</em>, or 
<em>the</em> if it is the first word. (Choose one)</h3>
<form method="POST" action="q6.php">
<div align="CENTER">
  <table border="0" width="60%">
  <tr> 
    <td>
    <?php
    if ($q6 == "true"):
        print "<input type=\"radio\" name=\"q6\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"true\">";
    endif;
    ?>
				A. True
    </td>
  </tr>
  <tr> 
    <td>    
    <?php
    if ($q6 == "false"):
        print "<input type=\"radio\" name=\"q6\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="7">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
