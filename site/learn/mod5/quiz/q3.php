<?php
ob_start();
$question = 3;
foreach ($_REQUEST as $a=>$b){$$a=$b;}

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q3 == "" && $next_question == 4){
    $err_msg = "Please select a choice";
    $next_question = 3;
}
//Print and record question results
if ($next_question == 4){
    $total = count($q3);
    for($i = 0; $i < $total; $i++){
        $response .= "[Question_3: ".$q3[$i]."], ";
        if ($i < ($total - 1))
            $response .= "&";
    }
    setcookie ("next", "4");
    setcookie ("ans[2]", $total);    
    setcookie ("response", $response);
    $msg .= "<h4>Answers to Question Three:</h4>";
    if (count($q3) == 4) {
        $msg .= "Absolutely correct! The Internet includes all four of these functions. 
								These protocols allow you to communicate with others (Email), move files (FTP), 
								access Web pages (HTTP), as well as to log into other computers (Telnet). <br>";
								
    } else {
        $msg .= "The Internet includes all four of these functions. 
								They allow you to communicate with others (Email), move files (FTP), access Web pages (HTTP), 
								as well as to log into other computers (Telnet).<br>";
    }
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 4</a></div></p>";
}
?>
<h2>Question Three</h2>
<h3>Which of these protocols is included as a function of the Internet? (Choose 
all that apply)</h3>
<form method="POST" action="q3.php">
<div align="CENTER">
  <table border="0" width="60%" align="CENTER">
    <tr> 
    <td>
    <?php
    if ($q3[0] == "e-mail"):
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"e-mail\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"e-mail\">";
    endif;
    ?> 
    Email </td>
  </tr>
    <tr> 
    <td>
    <?php
    if ($q3[0] == "FTP" || $q3[1] == "FTP"):
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"FTP\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"FTP\">";
    endif;
    ?> 
    FTP</td>
  </tr>
   <tr> 
    <td>
    <?php
    if ($q3[0] == "HTTP " || $q3[1] == "HTTP" || $q3[2] == "HTTP"):
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"HTTP\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"HTTP\">";
    endif;
    ?> 
    HTTP
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q3[0] == "telnet" || $q3[1] == "telnet" || $q3[2] == "telnet" || $q3[3] == "telnet"):
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"telnet\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q3[]\" value=\"telnet\">";
    endif;
    ?> 
   Telnet
  </tr>
 </table>
<p> 
 <input type="hidden" name ="next_question" value="4">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
