<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 4; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q4 = $_REQUEST["q4"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q4 == "" && $next_question == 5){
    $err_msg = "Please select a choice";
    $next_question = 4;
}
//Print and record question results
if ($next_question == 5){
    $correct = 0;
    $msg = "<h4>Answer to Question Four:</h4>\n";
    if ($q4 == "true") {
        $msg .= "<b>A:</b> Right! Just use your Library Account.<br>";
        $correct = 1;
    } else {
    $msg .= "<b>B:</b> No. You can easily find out this information using
				your Library Account in Pegasus.<br>";
}
    $response = $_COOKIE["response"];
    $response .= "[Question_4: ".$q4."], ";
    setcookie ("response", $response);    
    setcookie ("next", "5");
    setcookie ("ans[3]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 5</a></div></p>";
}    
?>
<h2>Question Four</h2>
<h3> You can see how many books you have checked out and when they are due using Pegasus. <br>
(Choose one) </h3>
<form method="POST" action="q4.php">
<div align="CENTER">
  <table border="0" width="60%">
  <tr> 
    <td>
    <?php
    if ($q4 == "true"):
        print "<input type=\"radio\" name=\"q4\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"true\">";
    endif;
    ?>
				A. True
    </td>
  </tr>
  <tr> 
    <td>    
    <?php
    if ($q4 == "false"):
        print "<input type=\"radio\" name=\"q4\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="5">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
