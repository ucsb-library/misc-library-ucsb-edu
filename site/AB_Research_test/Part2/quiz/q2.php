<?php
ob_start();
require_once "../../quiz_header.php";
$question = 2; //used in quiz_header.php
foreach ($_REQUEST as $a=>$b){$$a=$b;}

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 3;
}

//Check that at least one response has been entered
if ($q2 == "" && $next_question == 3){
    $err_msg = "Please select a choice";
    $next_question = 3;
}

//Print and record question results
if ($next_question == 3){
    $correct = 0;
    $msg = "<font color=#820707><h4>Answer to Question Two:</h4></font>\n";
    if ($q2 == "email") {
        $msg .= "<b>A:</b> The answer is C, the article is written by Allison Ensor.";
    }
    if ($q2 == "web") {
        $msg .= "<b>B:</b> The answer is C, the article is written by Allison Ensor.<br>";
    }
    if ($q2 == "citations") {
    $msg .= "<b>C:</b> Yes, good job! The article is written by Allison Ensor.<br>";
    $correct = 1;
    }
    if ($q2 == "library") {
        $msg .= "<b>D:</b>
        The answer is C, the article is written by Allison Ensor.<br>";
    }
    $response = $_COOKIE["response"];
    $response .= "[Question_2: ".$q2."], ";
    setcookie ("response", $response);
    setcookie ("next", "3");
    setcookie ("ans[3]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 3</a></div></p>";
}

?>

<h2>Question Two</h2>
<h3>Who wrote this article?:<br>
Ensor, Allison. "Twain's ADVENTURES OF TOM SAWYER and ADVENTURES OF HUCKLEBERRY FINN." Explicator 48.1 (Fall89 1989): 32.
</span></h3>
<form method="POST" action="q2.php">
<div align="CENTER">
  <table border="0" width="75%" cellspacing="2" cellpadding="2">
  <tr>
    <td>
    <?php
    if ($q2 == "email"):
        print "<input type=\"radio\" name=\"q2\" value=\"email\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"email\">";
    endif;
    ?>
    A. Mark Twain
  </tr>
  <tr>
    <td>
    <?php
    if ($q2 == "web"):
        print "<input type=\"radio\" name=\"q2\" value=\"web\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"web\">";
    endif;
    ?>
    B. Samuel Langhorne Clemons
  </tr>
  <tr>
    <td>
    <?php
    if ($q2 == "citations"):
        print "<input type=\"radio\" name=\"q2\" value=\"citations\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"citations\">";
    endif;
    ?>
   C. Allison Ensor
  </tr>
  <tr>
    <td>
    <?php
    if ($q2 == "library"):
        print "<input type=\"radio\" name=\"q2\" value=\"library\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"library\">";
    endif;
    ?>
    D. Tom Sawyer  </td>
  </tr>
  </table>
<p>&nbsp;</p>
<input type="hidden" name ="next_question" value="3">
<?php
    if ($msg == ""){
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    }
?>

</p>
</div>
</form>
<?php
//Print error message or results
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>