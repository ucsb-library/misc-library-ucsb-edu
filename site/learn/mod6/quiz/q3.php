<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 3; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q3 = $_REQUEST["q3"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q3 == "" && $next_question == 4){
    $err_msg = "Please select a choice";
    $next_question = 3;
}
//Print and record question results
if ($next_question == 4){
    $correct = 0;
    $msg = "<h4>Answer to Question Three:</h4>\n";
    if ($q3 == "true") {
        $msg .= "<b>A:</b> Wrong! If you are cutting and pasting text from the Web, you <u>must</u> 
								acknowledge where the material came from by citing it. Otherwise, you are guilty 
								of plagiarism. If you download images, music, or files you need to get permission 
								so that you don't violate copyright.<br>";
    } else {
    $msg .= "<b>B:</b> If you are cutting and pasting text from the Web, you <u>must</u> 
				acknowledge where the material came from by citing it. Otherwise, you are guilty 
				of plagiarism. If you download images, music, or files you need to get permission 
				so that you don't violate copyright.<br>";
				$correct = 1;
    }
    $response = $_COOKIE["response"];
    $response .= "[Question_3:".$q3."], ";
    setcookie ("response", $response);    
    setcookie ("next", "4");
    setcookie ("ans[2]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 4</a></div></p>";
}    
?>
<h2>Question Three</h2>
<h3> Because the Internet is free you can download and use anything on it. (Choose one)</h3>
<form method="POST" action="q3.php">
<div align="CENTER">
  <table border="0" width="60%">
  <tr>
    <td>
    <?php
    if ($q3 == "true"):
        print "<input type=\"radio\" name=\"q3\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q3\" value=\"true\">";
    endif;
    ?>
				A. True
    </td>
  </tr>
  <tr> 
    <td>    
    <?php
    if ($q3 == "false"):
        print "<input type=\"radio\" name=\"q3\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q3\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="4">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
