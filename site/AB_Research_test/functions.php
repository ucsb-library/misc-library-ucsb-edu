<?php
foreach($_REQUEST as $a=>$b) {$$a=$b;}
//  foreach($_GET as $a=>$b){$$a=$b;} // $_GET VARIABLES
//  foreach($_POST as $a=>$b){$$a=$b;} // $_POST VARIABLES

function chk_question ($question)
{
    $msg = "";
    $next_q = $_COOKIE["next"];
    if ($next_q == ""){
        $next_q = 1;   
    }
    if (($next_q  >= $question) && $question != ""){
        $msg .= "<div class='redText' align=\"CENTER\">You have already answered this question.<br>
        Please move on to <a href='quiz.php'>question $next_q</a></div>";
    }
    return $msg;
}
  
function percent($num, $denom) 
{
    if ($denom == 0){ /* undefined */
        return 0;
    } else {  
        return round ($num/$denom * 100)."%";
    }
}

function colorPercent($num, $denom) 
{
  $percent = percent($num, $denom);
  if ($percent > 75)
    $msg = "<span class='greenText'>";
  else if ($percent > 50)
    $msg = "<span class=\"orangeText\">";
  else
    $msg = "<span class=\"redText\">";
  $msg .= $percent."</span>";
  return $msg;
}

function formatQuiz($quiz_ary, $correct_ary) 
{
    $total_questions = 0;
    $total_correct = 0;
    $msg = "
    <table width=60% cols=4 cellpadding=0 cellspacing=1 border=0>
    <tr><th align='left'>Question</td><th align='center'># Correct</th>
    <th align='right'># Incorrect</th><th align='right'>Percent</th></tr>
    <tr><td colspan=4><hr noshade></td></tr>";
    for ($x = 0; $x < count($quiz_ary); $x++){
        $msg .= "<tr><td align='left'>".($x + 1)."</td><td align='center'>".$quiz_ary[$x]."</td>
        <td align='right'>".($correct_ary[$x] - $quiz_ary[$x])."</td>
        <td align='right'>".colorPercent($quiz_ary[$x], $correct_ary[$x])."</td></tr>\n";
        $total_correct += $quiz_ary[$x];
        $total_incorrect += $correct_ary[$x] - $quiz_ary[$x];
        $total += $correct_ary[$x];
    }
    $msg .= "<tr><td colspan=4><hr noshade></td></tr>
    <tr><th align='left'>Total</th><th align='center'>$total_correct</th><th align='right'>$total_incorrect</th>
    <th align='right'>".colorPercent($total_correct, $total)."</th></tr></table>";  
    return $msg;
}

?>
