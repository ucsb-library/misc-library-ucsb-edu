<?php
ob_start();
$question = 9;
$next_question = $_REQUEST["next_question"];
$q9 = $_REQUEST["q9"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
$next_question = 0;
}

//Check that at least one response has been entered
if (count($q9) < 2 && $next_question == 10){
$err_msg = "Please select two choices";
$next_question = 1;
}
if (count($q9) > 2 && $next_question == 10){
$err_msg = "Please select only two choices";
$next_question = 1;
}

//Print and record question results
if ($next_question == 10){
$total = count($q9);
for($i = 0; $i < $total; $i++){
$response .= "Question_9: ".$q9[$i]."], ";
if ($i < ($total - 1))
$response .= "&";
}
$msg .= "<h4>Answers to Question Nine:</h4>";
if ($q9[0] == "combine" && $q9[1] == "terms") {
$msg .= "<b>A:</b> Yes, a Keyword search is the only type of search that lets you combine concepts.<br>
<b>D:</b> Yes, this is one of the advantages of a keyword search.<br>";
$correct = 2;
} 
else if ($q9[0] == "combine" && $q9[1] == "find") {
$msg .= "<b>A:</b> Yes, Keyword search is the only type of search that lets you combine concepts.<br>
<b>B:</b> You could try this, but a Title or Author search would be more focused.  
Also, there are very few textbooks at the libraries.";
$correct = 1;
} 
else if ($q9[0] == "combine" && $q9[1] == "search") {
$msg .= "<b>A:</b> Yes, Keyword search is the only type of search that lets you combine concepts.<br>
<b>C:</b> No, to search the Internet you would use a search engine on the web";
$correct = 1;
} 
else if ($q9[0] == "find" && $q9[1] == "terms") {
$msg .= " <b>B:</b> You could try this, but a Title or Author search would be more focused.  
Also, there are very few current textbooks in the Libraries.<br>
<b>D:</b> Yes, this is one of the advantages of a keyword search.";
$correct = 1;
}
else if ($q9[0] == "search" && $q9[1] == "terms") {
$msg .= " <b>C:</b> No, to search the Internet you would use a search engine on the web.<br>
<b>D:</b> Yes, this is one of the advantages of a keyword search.";
$correct = 1;
}
else {
$msg .= "<b>B:</b> No, You could try this, but a Title or Author search would be more focused.
Also, there are very few textbooks at the libraries. <br>
<b>C:</b> No, to search the Internet you would use a search engine on the Web.<br>";
$correct = 0;
}
$response = $_COOKIE["response"];
$response .= "[Question_9a: ".$q9[0].", Question_9b: ".$q9[1]."]";
setcookie ("response", $response);
setcookie ("next", "10");
setcookie ("ans[8]", $correct);
$msg .= "<p><div align='center'><a href='quiz.php'>Go on to Quiz Results</a></div></p>";
}

?>
<h2>Question Nine</h2>
<h3> Using a Keyword search in Pegasus you can (Choose Two)</h3>
<form method="POST" action="q9.php">
<div align="CENTER"> 
<table border="0" width="60%" cellspacing="2" cellpadding="2">
<tr> 
<td> 
<?php
if ($q9[0] == "combine"):
print "<input type=\"checkbox\" name=\"q9[]\" value=\"combine\" CHECKED>";
else: 
print "<input type=\"checkbox\" name=\"q9[]\" value=\"combine\">";
endif;
?>
A. combine two or more concepts </tr>
<tr> 
<td> 
<?php
if ($q9[0] == "find" || $q9[1] == "find"):
print "<input type=\"checkbox\" name=\"q9[]\" value=\"find\" CHECKED>";
else: 
print "<input type=\"checkbox\" name=\"q9[]\" value=\"find\">";
endif;
?>
B. find textbooks for your class </tr>
<tr> 
<td> 
<?php
if ($q9[0] == "search" || $q9[1] == "search"):
print "<input type=\"checkbox\" name=\"q9[]\" value=\"search\" CHECKED>";
else: 
print "<input type=\"checkbox\" name=\"q9[]\" value=\"search\">";
endif;
?>
C. search the Internet </tr>
<tr> 
<td> 
<?php
if ($q9 [0] == "terms" || $q9 [1] == "terms"):
print "<input type=\"checkbox\" name=\"q9[]\" value=\"terms\" CHECKED>";
else: 
print "<input type=\"checkbox\" name=\"q9[]\" value=\"terms\">";
endif;
?>
D. use your own search terms </tr>
</table>
<p> 
<input type="hidden" name ="next_question" value="10">
<?php 
if ($msg == ""){ 
//is msg != "" then the form has been submitted
print "<input type=\"submit\" value=\"submit\">";
} 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
