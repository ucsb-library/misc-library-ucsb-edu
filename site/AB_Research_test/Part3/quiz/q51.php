<?php
ob_start();
$question = 5;
foreach ($_REQUEST as $a=>$b){$$a=$b;}

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q5 == "" && $next_question == 6){
    $err_msg = "Please select a least one choice";
    $next_question = 5;
}
//Print and record question results
if ($next_question == 6){
    $total = count($q5);
    for($i = 0; $i < $total; $i++){
        $response .= "[Question_5: ".$q5[$i]."]";
        if ($i < ($total - 1))
            $response .= " ";
    }
    setcookie ("next", "6");
    setcookie ("ans[4]", $total);
    setcookie ("response", $response);
    $msg .= "<h4>Answers to Question Five:</h4>";
    if (count($q5) == 8) {
        $msg .= "<b>Correct!</b> All sorts of people publish on the Web.<br>";
    } else {
        $msg .= "<b>Nope.</b> Actually <b>all</b> the groups
        listed can and do publish information on the web. Remember that
        domain names can end in: .edu, .gov, .com, .org and country
        codes. Each of these will give you a hint about what type of group
        is providing the information.<br>";
    }
    $msg .= "<p><div align='center'><a href='save_quiz.php'>Go on to Quiz Results</a></div></p>";
}

?>
<h2>Question Five</h2>
<h3>You�ve just chosen to write a research paper on: "The ethics of medical marijuana." Identify the best strategy of your search.
</h3>
<form method="POST" action="q5.php">
<div align="CENTER">
  <table border="0" width="33%" align="CENTER">
    <tr>
    <td>
    <?php
    if ($q5[0] == "students"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"students\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"students\">";
    endif;
    ?>
    Medical <b>AND</b> marijuana
</td>
  </tr>
    <tr>
    <td>
    <?php
    if ($q5[0] == "faculty" || $q5[1] == "faculty"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"faculty\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"faculty\">";
    endif;
    ?>
    Medical marijuana <b>AND</b> ethics
</td>
  </tr>
   <tr>
    <td>
    <?php
    if ($q5[0] == "foreigners" || $q5[1] == "foreigners" || $q5[2] == "foreigners"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"foreigners\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"foreigners\">";
    endif;
    ?>
    Alternative medicines <b>AND</b> ethics
  </tr>
  <tr>
    <td>
    <?php
    if ($q5[0] == "libraries" || $q5[1] == "libraries" || $q5[2] == "libraries" || $q5[3] == "libraries"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"libraries\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"libraries\">";
    endif;
    ?>
   Marijuana <b>AND</b> ethics
  </tr>
      <tr>
    <td>
    <?php
    if ($q5[0] == "government" || $q5[1] == "government" || $q5[2] == "government" || $q5[3] == "government" || $q5[4] == "government"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"government\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"government\">";
    endif;
    ?>
    Medic* <b>AND</b> marijuana <b>AND</b> ethic*
  </tr>
  <tr>
    <td>
    <?php
    if ($q5[0] == "universities" || $q5[1] == "universities" || $q5[2] == "universities" || $q5[3] == "universities" || $q5[4] == "universities" || $q5[5] == "universities"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"universities\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"universities\">";
    endif;
    ?>
    Universities</td>
  </tr>
		  <tr>
    <td>
    <?php
    if ($q5[0] == "companies" || $q5[1] == "companies" || $q5[2] == "companies" || $q5[3] == "companies" || $q5[4] == "companies" || $q5[5] == "companies" || $q5[6] == "companies"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"companies\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"companies\">";
    endif;
    ?>
    Pharmaceutical marijuana</td>
  </tr>
				  <tr>
    <td>
    <?php
    if ($q5[0] == "non-profit" || $q5[1] == "non-profit" || $q5[2] == "non-profit" || $q5[3] == "non-profit" || $q5[4] == "non-profit" || $q5[5] == "non-profit" || $q5[6] == "non-profit" || $q5[7] == "non-profit"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"non-profit\" CHECKED>";
    else:
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"non-profit\">";
    endif;
    ?>
    Non-profit organizations</td>
  </tr>

</table>
<p>
 <input type="hidden" name ="next_question" value="6">
<?php
    if ($msg == ""){
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    }
?>
</p>
</div>
</form>
<?php
//Print error message or results
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>
