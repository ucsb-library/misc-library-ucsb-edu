<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 1;
}
//Check that at least one response has been entered
if ($q1 == "" && $next_question == 2){
    $err_msg = "Please select a choice";
    $next_question = 1;
}
//Print and record question results
if ($next_question == 2){
    $correct = 0;
    $msg = "<font color=#820707><h4>Answer to Question One:</h4></font>\n";
    if ($q1 == "learning") {
        $msg .= "<b>A:</b> Don't confuse information literacy with reading literacy.
								Information literacy is knowing how to pick the best sources for your needs--that's a savvy user!<br>";
    }
    if ($q1 == "savvy") {
        $msg .= "<b>B:</b> Right! Information literacy is one of life's most important skills.<br>";
								$correct = 1; 
    } 
    if ($q1 == "library") {
    $msg .= "<b>C:</b> 
        No, information literacy is knowing how to pick the best sources for your needs--that's a savvy user!<br>";
    }
    if ($q1 == "magazine") {
        $msg .= "<b>D:</b> 
        No, information literacy is knowing how to pick the best sources for your needs--that's a savvy user!<br>";
    }
	
    $response = $_COOKIE["response"];
    $response .= "[Question_1: ".$q1."], ";
    setcookie ("response", $response);
    setcookie ("next", "2");
    setcookie ("ans[0]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 2</a></div></p>";
}

?>

<h2>Question One</h2>
<h3>The term &quot;information literacy&quot; is (Choose one)</h3>
<form method="POST" action="q1.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q1 == "learning"):
        print "<input type=\"radio\" name=\"q1\" value=\"learning\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"learning\">";
    endif;
    ?>
    A. Learning to read
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q1 == "savvy"):
        print "<input type=\"radio\" name=\"q1\" value=\"savvy\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"savvy\">";
    endif;
    ?>
    B. Becoming a savvy user of information
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q1 == "library"):
        print "<input type=\"radio\" name=\"q1\" value=\"library\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"library\">";
    endif;
    ?>
    C. A Library service
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q1 == "magazine"):
        print "<input type=\"radio\" name=\"q1\" value=\"magazine\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"magazine\">";
    endif;
    ?>
    D. A magazine
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="2">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>