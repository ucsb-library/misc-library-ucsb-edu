<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 5; //used in quiz_header.php
$next_question = $_REQUEST["next_question"];
$q5 = $_REQUEST["q5"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q5 == "" && $next_question == 6){
    $err_msg = "Please select a choice";
    $next_question = 5;
}
//Print and record question results
if ($next_question == 6){
    $correct = 0;
    $msg = "<h4>Answer to Question Five:</h4>\n";
    if ($q5 == "true") {
        $msg .= "<b>A:</b> You're right! Truncation is a way to search for alternate endings of words.<br>";
        $correct = 1;
    } else {
    $msg .= "<b>B:</b> Sorry, wrong. Truncation is a way to search for alternate endings of words.<br>";
}
    $response = $_COOKIE["response"];
    $response .= "[Question_5: ".$q5."], ";
    setcookie ("response", $response);    
    setcookie ("next", "6");
    setcookie ("ans[4]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 6</a></div></p>";
}    
?>

<h2>Question Five</h2>
<h3>Truncation is</h3>

<form method="POST" action="q5.php">
<div align="CENTER">
  <table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td><?php
    if ($q5 == "true"):
        print "<input type=\"radio\" name=\"q5\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"true\">";
    endif;
    ?>
				A. A way to search for alternate endings of words
    </td>
  </tr>

  <tr> 
    <td>   
    <?php
    if ($q5 == "false"):
        print "<input type=\"radio\" name=\"q5\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"false\">";
    endif;
    ?>
				B. Having a lot of luggage 
    </td>
  </tr>
  </table>

<p>

<input type="hidden" name ="next_question" value="6">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 

</p>
</div>
</form>

<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>