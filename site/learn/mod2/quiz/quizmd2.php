<html>
<head>
<title>Module 2: Starting Smart. Searchpath. UCSB Libraries.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFCEB" text="000033">
<table width="640" border="0" cellspacing="3" cellpadding="3" align="center">
  <tr valign="top"> 
    <td> 
      
<p align="left"><img src="../images/hdchoose.gif" width="470" height="71" alt="Choosing a Topic"></p>
      <p align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="+2" color="1F4366">QUIZ</font></p>
      <p><font face="Verdana, Arial, Helvetica, sans-serif">The final section 
        of this module is a 9 question quiz.</font></p>
      <ul>
        <li><font face="Verdana, Arial, Helvetica, sans-serif"> Choose your answer, 
          then click the submit button. Sometimes, you may have to scroll down 
          to find the submit button.</font></li>
      </ul>
      <ul>
        <li><font face="Verdana, Arial, Helvetica, sans-serif">Searchpath will 
          display a response to your answer. Read the response, then go to the 
          next question.</font></li>
      </ul>
      <ul>
        <li><font face="Verdana, Arial, Helvetica, sans-serif">You can take the 
          quiz over again, but you must complete it before retaking any questions.</font></li>
      </ul>
      <ul>
        
<li><font face="Verdana, Arial, Helvetica, sans-serif">The last page of the quiz 
displays a summary of your answers. You will be able to print out or email your 
quiz results to your instructor.</font></li>
      </ul>
<ul>
        <li><font color="000333" face="Verdana, Arial, Helvetica, sans-serif">If you use the Safari browser, your results may not display correctly. Use Firefox or Internet Explorer.</font></li>
      </ul>
      <table width="100%" border="0" cellspacing="3" cellpadding="3" align="center">
        <tr> 
          <td width="25%" valign="middle" align="center"> 
            <p align="CENTER"> 
            <form method="get" action="quiz.php">
              <div align="CENTER"> 
                <input type="submit" name="quiz" value="So quiz me already.">
              </div>
            </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
