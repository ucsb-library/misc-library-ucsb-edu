<?php
ob_start();
require_once "../../quiz_header.php";
$question = 4; //used in quiz_header.php
foreach ($_REQUEST as $a=>$b){$$a=$b;}

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
$next_question = 0;
}

//Check that at least one response has been entered
if ($q4 == "" && $next_question == 5){
    $err_msg = "Please select a choice";
    $next_question = 4;
}
//Print and record question results
if ($next_question == 5){
    $correct = 0;
    $msg = "<font color=#820707><h4>Answer to Question Four:</h4></font>\n";
    if ($q4 == "journal") {
        $msg .= "<b>A:</b> No, journals may be published weekly, monthly, or annually. 
								Journals would not report breaking news in a timely fashion.";
    }
    if ($q4 == "pegasus") {
        $msg .= "<b>B:</b> No, Pegasus contains the records for everything owned by the Library.<br>";
    } 
    if ($q4 == "web") {
    $msg .= "<b>C:</b> 
        Right! Breaking news can be found on the Web. <br>";
        $correct = 1;  
    }
    if ($q4 == "magazine") {
        $msg .= "<b>D:</b> 
        No, usually magazines report the previous week's news.<br>";
				}
    $response = $_COOKIE["response"];
    $response .= "[Question_4: ".$q4."], ";
    setcookie ("response", $response);    
    setcookie ("next", "5");
    setcookie ("ans[3]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 5</a></div></p>";
}    

?>

<h2>Question Four</h2>
<h3>The latest news can be found using this source. (Choose one)</h3>
<form method="POST" action="q4.php">
<div align="CENTER">
<table border="0" width="75%" cellspacing="2" cellpadding="2">
 <tr> 
    <td> 
    <?php
    if ($q4 == "journal"):
        print "<input type=\"radio\" name=\"q4\" value=\"journal\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"journal\">";
    endif;
    ?>
    A. Journals
  </tr>

		<tr> 
    <td> 
    <?php
    if ($q4 == "pegasus"):
        print "<input type=\"radio\" name=\"q4\" value=\"pegasus\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"pegasus\">";
    endif;
    ?>
    B. Pegasus
  </tr>

		<tr> 
    <td> 
    <?php
    if ($q4 == "web"):
        print "<input type=\"radio\" name=\"q4\" value=\"web\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"web\">";
    endif;
    ?>
    C. Web
  </tr>

		<tr> 
    <td> 
    <?php
    if ($q4 == "magazine"):
        print "<input type=\"radio\" name=\"q4\" value=\"magazine\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q4\" value=\"magazine\">";
    endif;
    ?>
    D. Magazines
  </tr>

  </table>
<p>
<input type="hidden" name ="next_question" value="5">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>

<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>