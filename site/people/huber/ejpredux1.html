<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html><head><title>Electronic Journal Publishers Redux</title></head>

<body bgcolor="seashell" text="#000000">



<h1 align=center>Electronic Journal Publishers:<br>A Reference Librarian's Guide<br>2003 Update</h1>

<center>

<strong>Charles F. Huber</strong><br>

Chemical Sciences Librarian<br>

Davidson Library<br>

University of California - Santa Barbara<br>

<a href="mailto:huber@library.ucsb.edu">huber@library.ucsb.edu</a>

</center>

<hr>

<h2>Abstract</h2>

<blockquote>

In the year 2000, I undertook an examination of STM journals publishers' websites, focusing on those features most relevant to the end-user student or researcher. Three years later, some publishers have vanished, new ones have entered the arena, interfaces have changed, and the total number of available titles has skyrocketed. So, an update seemed useful.

</blockquote>

<p>

<h2>Introduction</h2>

This is not intended to be a comprehensive survey of all e-journals.  First, it is limited to primary publications - indexes and abstracts involve a significantly different feature set than the primary e-journals. Second, I have not attempted to cover individual electronic-only journals such as <a href="http://jhep.mse.jhu.edu/">Journal of High Energy Physics</a> (http://jhep.mse.jhu.edu/) or <a href="http://www.ijc.com/">Internet Journal of Chemistry</a> (http://www.ijc.com/).  This is not to say that these are not high quality, important journals.   Third, even within the range of "electronic versions of print journals", I do not claim to be absolutely comprehensive.  Some publishers I may have missed out of simple ignorance, while others were skipped because they appear to still be in the development stage. One source only on the horizon which would definitely make the cut a year from now is <a href="http://www.plos.org/">Public Library of Science</a> (http://www.plos.org/), which will release its first open-access title, PLoS Biology, in October, 2003.

<h2>Organization</h2>

The main information in this survey is collected in the four tables below:

<ul>

<li><a href="ejpredux2.html"><strong>Large Publishers (&gt;200 journals)</strong></a>

<li><a href="ejpredux2.html"><strong>Small and Medium Publishers</strong></a>

<li><a href="ejpredux4.html"><strong>Electronic Journal Aggregators</strong></a>

<li><a href="ejpredux5.html"><strong>Specialty Aggregators (BioOne, JSTOR, Highwire, MUSE)</strong></a>

<li><a href="ejpredux6.html"><strong>Open Access Aggregators (BioMedCentral, J-STAGE, SciELO)</strong></a>

</ul>

In the remainder of the body of the article, I'll comment on the types of data gathered, and add some supplementary observations on the individual publishers and aggregators.

<h3>Data Fields</h3>

<h4>Publisher/Aggregator and Parent Company</h4>

These are self-explanatory.  I included the links to parent company websites since in the current acquisition-happy state of the publishing industry, it's hard to keep track of who the real players are.

<h4>Number of e-journals and/or Number of publishers (for aggregators)</h4>

This datum gives an idea of the size of each supplier for general interest.  Also, for those publishers/aggregators who provide some sort of global search utility, this gives an idea of just how big a pool of articles one may be searching. ranging from giants like Elsevier or OCLC  to more modest publishers like Nature or the Royal Society (whose size belie their importance.) Note that this is sometimes a slippery figure.  Some publishers and aggregators will list journals for which full text access is not yet available; in some cases varying depending on how soon availability is expected. The aforementioned mergers and acquisitions also can cause numbers to change dramatically.

<h4>Full text format</h4>

Both HTML and PDF (Adobe Acrobat's proprietary file format) enjoy widespread popularity, with Catchword's proprietary RealPage format also in use by several publishers and aggregators. The latter two respond to the persistent user desire for a format that looks and prints like the familiar journal page.  PDF is also common where a publisher is developing a backfile of volumes which were not originally in electronic form by scanning printed pages. HTML pages (or, better still, HTML pages created from underlying SGML documents) are important for searchability and for crosslinking of documents, a rapidly growing feature of electronic journals (see below). Other formats, such as PostScript or LaTex, tend to be important only in particular subject areas.

<p>

It's handy for the reference librarian to be aware of the available formats for a document for a least a couple of reasons: (1) To advise users outside the library.  Everyone can deal with HTML if they have any access to the Web at all.  PDF requires the Adobe Acrobat Reader, but it is freely and easily downloadable, and is almost as ubiquitous as the browsers themselves.  However, the RealPage reader or plugin, while freely available, are much less likely to be already installed on a users computer and are unavailable for Macintosh or UNIX operating systems.  (2) Articles available in HTML format may be searched internally for keywords of interest using the browser's Find function.  This is especially useful when an article has been located by a full text search, and it may not be obvious to the user where the hit terms are located in a long review article, for example. Some PDF files are searchable, but not all.

<h4>Chronological Coverage</h4>

Even within a given publisher, this may vary widely from journal to journal, reflecting the arrival of totally new journals as well as journals which got their electronic start later than the main body of a publisher's work.  Note that the dates given reflect the availability of full text; many publishers provide tables of contents and/or abstracts going back significantly further.  Just as in the world of indexing databases, most electronic journals coverage begins at the point where the publisher began producing the journal in electronic form.  Some have, however, undertaken retrospective conversion, and JSTOR has become the specialist in providing electronic archives.

<h4>Advance papers</h4>

An increasing number of publishers are taking advantage of the speed of electronic processing, especially where they have electronic submission of paper, to provide access to papers in advance of print publication.  Some publishers do so in the form of pre-publication listing of forthcoming issues, with tables of contents, abstracts and finally full text added as they become available. Others create a separate section of papers which have been accepted but not yet assigned an issue date.  Obviously, scientists and engineers interested in being on top of current research will be interested to know if their favorite journals provide advance papers. One detail you may wish to point out to users, or at least those with an interest in patenting their work, is the addition of "date of online publication" information in those journals offering advance papers.  In response to the justifiable insistence of the patent searching community, most publishers have added this information in order to properly enable the determination of priority publication for patent examination purposes.

<p>

Note: this field was omitted for aggregators, as the presence or absence of advance papers is determined by the individual publishers.

<h4>Whole file search and journal search</h4>

The search capabilities of electronic journal publishers vary amazingly.  Elsevier provides a fairly powerful search utility in ScienceDirect. On the other hand, Oxford University Press provides rather minimal searching for those journals hosted at its own site. Some provide various levels of searching: basic or "advanced".  Journal publishers generally do not provide any sophistication in terms of Boolean searching, proximity searching, truncation or subject indexing; they are not a real replacement for the high powered STM indexing services.

<p>

However, two characteristics of publisher search engines which I have noted in the tables are significant. First, some publishers allow searching of the <strong>full text</strong> of articles.  Users looking for references to topics such as experimental methods, which may not be the main thrust of the article, and so are ignored by traditional indexing services, may be able to find useful information available only with great difficulty elsewhere.

<p>

Second, a significant number of publishers, and even some aggregators, allow <strong>non-subscribers</strong> to search their journal collections. This can be very useful for smaller institutions which may not be able to afford all of the most powerful indexing databases, or for remote users without proxy access to their institutions' database resources. While the power of these search tools may be lacking compared to Chemical Abstracts, BIOSIS or Science Citation Index, these freely available searches can provide access to thousands of scholarly journals, often with abstracts, and increasingly with "pay per view" options for non-subscribers (see below.)

<h4>Table of Contents Notification and Other Personalized Services</h4>

Researchers, especially those weaned on Current Contents, are often eager to get e-mail notification of the online arrival of new issues of their favorite journals, and many publishers are offering to meet the demand.  Such services can be especially useful when they include the "advance articles" referred to above. Some also offer additional features, such as personalized "home pages", saved searches, update searches, saved lists of favorite articles or favorite journals and the like.

<p>

By necessity, these services generally require some kind of registration of the individual user and creation of a username and password.  In its early stages, Wiley InterScience required such registration of <em>all</em> users, but under strenous protest from a host of librarians, that approach was dropped.

<h4>Individual article purchase for non-subscribers</h4>

As the mechanisms of e-commerce become more and more commonplace, publishers and even aggregators are increasingly offering online purchase of individual articles by non-subscribers, with online payment by credit card.  Usually linked with non-subscriber access to searching or browsing of tables of contents, this service can be a great boon for researchers who need an otherwise unavailable article and are unwilling or unable to wait for InterLibrary Loan services.

<h4>Other information</h4>

I have made note here of some of the cases where individual publishers have acted as aggregators for other publishers (American Institute of Physics has been especially active in this regard.)  Also noted are some of the specialized crosslinking capabilities some publishers have provided.  Not all such links have been noted, however.  A majority of major STM publishers, including some who are making their journals available only through aggregators, have signed up to participate in <a href="http://www.crossref.org/">CrossRef</a> (http://www.crossref.org/).  These publishers have agreed to implement crosslinks between the references in their electronic articles and the corresponding articles (or at least citations and abstracts) in other publishers' electronic titles and vice-versa.  However, at present, implementation of this agreement is at a very early and limited stage.  It does hold promise for easy flow along the chain of references, especially as more publishers take advantages of links to PubMed, Chemical Abstracts Service's ChemPort, and ISI's Web of Science.

<h3>Publisher Notes: <a href="ejpredux2.html">Large Publishers</a></h3>

<h4>Academic Press IDEALibrary</h4>

Academic Press had a major entry in the 2000 edition of this report.  Subsequently purchased by Reed Elsevier, the separate Academic Press website has been eliminated (except for a handful of reference works) and the contents have been absorbed into Elsevier's ScienceDirect.

<h4><a href="ejpredux2.html#blackwell">Blackwell Synergy</a></h4>

Blackwell's site has changed considerably since 2000.  Not only have additional Blackwell Science journals been added to the lists, but more and more journals from the social sciences and humaniities lists of Blackwell Publishing are being made available here.  In addition, site navigation has become much easier.  Users can view the journal lists alphabetically or by subject, and can limit to subscribed titles.

<h4><a href="ejpredux2.html#elsevier">Elsevier ScienceDirect</a></h4>

Elsevier's ScienceDirect site has remained substantially the same since the last report, though the number of available journals has grown greatly, largely due to the integration of the Academic Press list.  Also, Elsevier is adding journal backfiles (available for an additional one-time fee) on a subject area by subject area basis. More article databases are now available through the ScienceDirect interface, and Elsevier has slowly entered the field of online reference works - only two so far.

<h4><a href="ejpredux2.html#kluwer">Kluwer Online</a></h4>

Kluwer's site has been substantially overhauled since 2000.  Kluwer is now offering reference works and e-books through their site.  For the journals themselves, the Russian translation journals of the Consultants Bureau have now been fully integrated into the list, while the Kluwer Law International journals, while still listed, have been moved to a separate server.  A new Advanced Search screen makes for much more powerful journal article searching than was previously available.

<h4><a href="ejpredux2.html#springer">Springer LINK</a></h4>

Springer LINK is substantially the same as in 2000.  The number of journal titles has grown somewhat, and the number of titles and volumes in their monographic book series has grown tremendously.

<h4><a href="ejpredux2.html#taylor">Taylor &amp; Francis Online Journals</a></h4>

This is a new addition since 2000.  Taylor &amp; Francis had made some of their journals available through aggregators for years, but now have their own online site as well. 

<h4><a href="ejpredux2.html#wiley">Wiley InterScience</a></h4>

Wiley Interscience is substantially the same as in 2000.  Wiley has begun adding journal backfiles by subject area, beginning with their Polymer Science collection. The number of titles in their Major Reference Works series has grown significantly.

<h3>Publisher Notes: <a href="ejpredux3.html">Small and Medium Publishers</a></h3>

<h4><a href="ejpredux3.html#acs">ACS Publications</a></h4>

ACS Publications has added full archives for all of their journals back to Volume 1.  Access to the journal archives is by an additional annual fee.

<h4><a href="ejpredux3.html#aip">AIP Online Journal Publishing Service</a></h4>

AIP has revised their site somewhat since 2000.  More outside publishers have been added, and a "Search OJPS" feature allows searching across all the titles on the site.

<h4><a href="ejpredux3.html#ams">American Mathematical Society Journals</a></h4>

AMS's website is virtually the same as in 2000.

<h4><a href="ejpredux3.html#annrev">Annual Reviews</a></h4>

Annual Reviews no longer publishes its e-journals through Highwire Press, but retains a similiar format.  Annual Reviews has recently made their complete backfiles available for a one-time fee.

<h4><a href="ejpredux3.html#acm">ACM Digital Library</a></h4>

ACM Digital Library continues to grow.  Its journal features have not changed greatly, but the organization of the conference proceedings has changed considerably.

<h4><a href="ejpredux3.html#cup">Cambridge Journals Online</a></h4>

Cambridge has considerably improved their interface in the past three years, with tegard to look, feel and searching.

<h4><a href="ejpredux3.html#crc">CRC Press Journals</a></h4>

In 2000, the CRC Press journals were made available through ScienceDirect. Now, CRC uses their CRCnetBASE platform to provide electronic access to their growing collection of journals.

<h4><a href="ejpredux3.html#deg">de Gruyter Online Journals</a></h4>

Once available only through aggregators, Walter de Gruyter now has its own website.  Despite the size of their overall journal group, they only have a small number available online at present, and the features of the site are fairly primitive.

<h4><a href="ejpredux3.html#emerald">Emerald Library</a></h4>

The absence of any searching or ToC browsing for non-subscribers is curious, given their ToC alert service for non-subscribers. On the whole, though the former MCB Univesity Press site has improved, it still lags its peers.

<h4><a href="ejpredux3.html#ieee">IEEE Xplore</a></h4>

IEEE Xplore is adding complete backfiles for the IEEE/IRE journals for no additional charge to IEEE Xplore subscribers.

<h4><a href="ejpredux3.html#iop">Institute of Physics Publishing</a></h4>

IOP has added backfiles for their journals, available for a one-time added fee.

<h4><a href="ejpredux3.html#dekker">Marcel Dekker</a></h4>

Like de Gruyter, Marcel Dekker is a relatively new entrant in the field of publisher e-journal sites. Its sitewide features are somewhat more advanced than de Gruyter, but still lag the more experienced publishers.

<h4><a href="ejpredux3.html#nature">Nature.com</a></h4>

Nature's publsher has changed its name to the Nature Publishing Group. Nature's website has matured considerably, along with its policies on institutional site licensing.  The full range of Nature "daughter" journals is now available, along with a large number of other jouranls under the Nature Publishing Group umbrella. 

<h4><a href="ejpredux3.html#nrc">NRC Research Press Electronic Journals</a></h4>

NRC has a generally attractive and effective site (in both English and French!), but the fact that searching has been unavailable for a week or more raises questions about the maintenance of the site.

<h4><a href="ejpredux3.html#oup">Oxford University Press Journals</a></h4>

Still more OUP journals have migrated to Highwire Press since 2000, and some have also shown up as part of Project MUSE, but there are still some that are available primarily though the OUP home site.

<h4><a href="ejpredux3.html#portland">Portland Press Journals</a></h4>

Portland Press has contracted significantly; it is now essentially only the publisher for the Biochemical Society (UK).  They have added some electronic books -- a selection of Biochemical Society proceedings and essays.

<h4><a href="ejpredux3.html#royalsoc">The Royal Society</a></h4>

The Royal Society site has added a couple of journals, and generally enhanced the site since 2000.

<h4><a href="ejpredux3.html#rsc">Royal Society of Chemistry Journals</a></h4>

RSC has generally done an excellent job of making their e-journals available, and has cleaned up the few flaws which were conspicuous in 2000.

<h4><a href="ejpredux3.html#turpion">Turpion, Ltd.</a></h4>

Turpion has not changed their website since 2000. Given Turpion's small but significant lineup of journals, they would be well advised to consider hooking up with one or more major aggregators, and/or offering individual article purchase to non-subscribers.

<h4><a href="ejpredux3.html#ucp">University of Chicago Press Journals Division</a></h4>

University of Chicago Press continues to add more of their journals in electronic form.  The website is otherwise unchanged.

<h3>Publisher Notes: <a href="ejpredux4.html">Electronic Journal Aggregators</a></h3>

<h4>Blackwell Electronic Journal Navigator</h4>

Blackwell's aggregator service has been dropped, presumably in favor of SwetsWise.

<h4>Catchword</h4>

CatchWord was acquired by Ingenta, and renamed IngentaSelect, for which, see below.

<h4><a href="ejpredux4.html#ebsco">EBSCOhost</a></h4>

EBSCO massively overhauled their site since 2000, improving ease of use, and opening searching to non-subscribers.

<h4>Information Quest</h4>

Like the rest of Rowe.com, this service has gone belly-up, never to return.

<h4><a href="ejpredux4.html#ingenta">ingenta</a></h4>

ingenta is one of the few aggregators with searching open to non-subscribers.  Since it's a public/private partnership connected to the University of Bath, and sells copies of individual articles to non-subscribers, perhaps this is not surprising. Since 2000, Ingenta has acquired the CARL Uncover database and integrated it into Ingenta.

<h4><a href="ejpredux4.html#ingentaselect">IngentaSelect</a></h4>

The CatchWord interface has not changed under its new title of IngentaSelect.  Its journals list, however, has been gretly enriched by the addition of almost all the titles from the ingenta list

<h4><a href="ejpredux4.html#oclc">OCLC FirstSearch Electronic Collections Online</a></h4>

OCLC ECO has not changed significantly since the introduction of "New FirstSearch" in 2000.

<h4><a href="ejpredux4.html#ovid">Ovid Journals@Ovid</a></h4>

Another service growing out of a database vendor, Ovid, too, provides excellent search services, but is limited to subscribers.

<h4><a href="ejpredux4.html#proquest">ProQuest</a></h4>

Some of ProQuest's search features (personal, company and geographic name) reflect its large business and newspaper content.  It lists 975 titles as "Science" journals in its Research Library package, but examining the list reveals their definition of "science" to be rather loose.  <cite>American Music</cite>?  <cite>Art History</cite>? Odd...

<h4><a href="ejpredux4.html#swets">SwetsWise</em></a></h4>

SwetsWise has an improved interface since the old Blackwell site was fully folded in.

<h3>Publisher Notes: <a href="ejpredux5.html">Specialty Aggregators</a></h3>

<h4><a href="ejpredux5.html#bioone">BioOne</a></h4>

BioOne was just being launched at the time of the 2000 article. BioOne acts as an electronic aggregaor for the journals of a number of small American biological societies and is a joint effort of the American Institute of Biological Sciences (AIBS), SPARC (the Scholarly Publishing & Academic Resources Coalition), The University of Kansas, Greater Western Library Alliance (formerly Big 12 Plus Libraries Consortium), and Allen Press, Inc. BioOne has grown slowly, but steadily, and has replaced commercial publishers for a few journals.  Their first online book was released recently.

<h4><a href="ejpredux5.html#highwire">Highwire Press</a></h4>

Highwire Press is an extremely dynamic initiative from the Stanford University Libraries.  In 2003, it continues to grow steadily, providing small society publishers with high quality e-journal services, as well as hosting some of the most important scientific journals (<cite>Science, PNAS, Journal of Biological Chemistry</cite>), and providing services to at least one major academic publisher which also maintains its own electronic publicatins (Oxford University Press).  They have taken the lead in providing non-subscriber services, including persuading some of their publishers to make their archival volumes available free of charge.  Highwire has also gotten into the reference work arena; though it may not be obvious at the OED website, the new Oxford English Dictionary is hosted by Highwire Press.

<h4><a href="ejpredux5.html#jstor">JSTOR</a></h4>

JSTOR, the archival journal publisher, continues to grow with at least one new collection each year, with a Music collection due by the end of 2003.  Several publishers have adjusted the chronological range available in JSTOR so that it will neatly dovetail with the volumes available on their own sites to current subscribers.  Project MUSE and JSTOR are creating reciprocal links between the journals they have in common to facilitate easy usage.

<h4><a href="ejpredux5.html#muse">Project MUSE</a></h4>

Project MUSE doesn't (yet) have a great deal of impact on STM, but it's interesting as a venue for another subset of small journal publishers - university presses - making it to the Web.  Several more university presses have begun adding their titles to MUSE, and it has grown steadily every year.

<h2>Resources</h2>

The reference below are from the bibliography of the 2000 article. I will add cites for more recent articles as I find them.

<p>

<a name="1"><strong>Arnold, Stephen E.</strong></a> 1999. The Scholarly Hothouse: Electronic STM Journals. <cite>Database</cite> 22(1): 27-30+

<p>

<a name="2"><strong>Brown, Elizabeth W. and Duda, Andrea L.</strong></a> 1996. Electronic Publishing Programs in Science and Technology Part 1: The Journals. <cite>Issues in Science and Technology Librarianship</cite> Fall 1996. Available: <a href="http://www.library.ucsb.edu/istl/96-fall/brown-duda.html">http://www.library.ucsb.edu/istl/96-fall/brown-duda.html</a>

<p>

<a name="3"><strong>Buckley, Chad, et al.</strong></a> Electronic Publishing of Scholarly Journals: A Bibliographic Essay of Current Issues. <cite>Issues  in Science and Technology Librarianship</cite> Spring 1999. Available: <a href="http://www.library.ucsb.edu/istl/99-spring/article4.html">http://www.library.ucsb.edu/istl/99-spring/article4.html</a>

<p>

<a name="4"><strong>Chan, Liza</strong></a> 1999. Electronic journals and academic libraries. <cite>Library Hi-Tech</cite> 17(1): 10-16

<p>

<a name="5"><strong>Peek, Robin and Pomerantz, Jeffrey</strong></a> 1998. The Traditional Scholarly Publishers Legitimize the Web. <cite>Journal of the American Society for Information Science</cite> 49(11): 983-989

<p>

<a name="6"><strong>Tenopir, Carol</strong></a> 1999. Should We Cancel Print?. <cite>Library Journal</cite> 124(14): 138+

<p>



Author: <a href="mailto:huber@library.ucsb.edu">Chuck Huber</a><br>



<br>

Updated: <?php echo date( "m/d/y h:i:s", getlastmod() ); ?>
<br><br>



</body>

</html>


