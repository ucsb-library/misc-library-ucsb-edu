<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 5;
//$next_question = $_REQUEST["next_question"];
//$q5 = $_REQUEST["q5"];
//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q5 == "" && $next_question == 6){
    $err_msg = "Please select a choice";
    $next_question = 5;
}
//Print and record question results
if ($next_question == 6){
    $correct = 0;
    $msg = "<h4>Answer to Question Five:</h4>\n";
    if ($q5 == "title") {
        $msg .= "<b>A:</b> Close, but you would get all the books with that title as well.
								         Use a Journal Title search to look up periodicals.<br>";
    }
    if ($q5 == "journal") {
        $msg .= "<b>B:</b> Correct. It's the best search to use to search for periodicals.<br>";
								$correct = 1; 
    } 
    if ($q5 == "keyword") {
    $msg .= "<b>C:</b> No. A Journal Title search is a much better way to search for periodicals.<br>";
    }
    if ($q5 == "subject") {
        $msg .= "<b>D:</b> No. This type of search uses the Library of Congress Headings to look up subjects, not journal titles.
								Use a Journal Title search to look up periodicals.<br>";
    }
	
    $response = $_COOKIE["response"];
    $response .= "Question_5: ".$q5."], ";
    setcookie ("response", $response);
    setcookie ("next", "6");
    setcookie ("ans[4]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 6</a></div></p>";
}
?>

<h2>Question Five</h2>
<h3>To look up a journal, magazine, or newspaper in Pegasus you would use (Choose one)</h3>
<form method="POST" action="q5.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q5 == "title"):
        print "<input type=\"radio\" name=\"q5\" value=\"title\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"title\">";
    endif;
    ?>
    A. Title search 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q5 == "journal"):
        print "<input type=\"radio\" name=\"q5\" value=\"journal\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"journal\">";
    endif;
    ?>
    B. Journal title search 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q5 == "keyword"):
        print "<input type=\"radio\" name=\"q5\" value=\"keyword\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"keyword\">";
    endif;
    ?>
    C. Keyword search 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q5 == "subject"):
        print "<input type=\"radio\" name=\"q5\" value=\"subject\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"subject\">";
    endif;
    ?>
    D. Subject heading search 
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="6">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
