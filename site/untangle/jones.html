<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<html>
<head>
<title>Critical Thinking in an Online World</title>
</head>
<body bgcolor="#ffffff" text="#000000" link="#0000ff" alink="#b03060" vlink="#d02090">
<img src="web041.gif" alt="Untangling the web">

<hr>
<h1 align=center>Critical Thinking in an Online World</h1>
<center><strong><a href="mailto:dejones@cabrillo.cc.ca.us">Debra 
Jones</a></strong><br>
Internet Librarian<br>
Cabrillo College, Aptos, CA</center><p>

<hr>
<center>Copyright 1996, Debra Jones. Used with permission.</center>
<hr>

<h2>Abstract</h2>
<blockquote>

In a rapidly evolving information technology era, librarians find their
foundations of professionalism shaken. Critically evaluating the intrinsic
role of the librarian reveals our responsibility for the education of
independent information seekers. Using the model of the expert and
apprentice, librarians need to focus on the teaching of critical thinking
skills, over and above the more mechanistic skills of evaluation of
resources and mastery of search tools. The design of instruction in a
situated learning environment, utilizing constructivist tenets and a
self-directed inquiry based approach leads to higher order cognitive
skills and applicable, transferable learning. An instructional design
project for teaching critical thinking skills in the evaluation of online
resources is described as an example curriculum? 

</blockquote><hr><p>

<h2>On The Abyss</h2>

The integration of the Internet into our daily lives affects no single
profession as completely as that of the librarian. For centuries,
information has been archived and accessed through a single location, the
library. Instantaneous access to online information, direct dissemination of
information as it is created, and interaction and creation of information
online, all from the home or office- these are revolutionary and
anarchical concepts. Very few among us still deny the pervasiveness of
online information access, yet how do we see ourselves leading, and not
just reacting, to this revolution? <p>

Questions of self-identity have always plagued the library profession. We
are indeed servers, those who take what is given to us, books, media,
records of human history, and preserve it for others who then ask us to
return it to them, unharmed. No one questions the nobility of this
profession. Only peripherally have we assumed the role of teachers,
instructing others in the basic techniques of navigation in our world.
Programs that train our professionals, bearing the curious name of
"Library Science," rarely offer courses in educational theory or the
design and delivery of instruction. Like the short order cook, reference
librarians turn out packets of information on demand, yet think not of
transferring to the seeker any but a modicum of skills in using the OPAC or
periodical indexes. The Internet, with its nearly full menu of online
information, offers delivery at home, without waiting in line, or forming
the query into the suitable jargon of the library world. <p>

Library of Congress classification schedules, Dewey Decimal organization
of knowledge, our bastions and tools with which we carve our living, may
be under siege. Yet still we try to translate our lessons in
"bibliographic instruction," a term that seemingly only relates to the
construction of bibliographies, to computer based navigation on the
Internet, all the while paying lip service to "information literacy." Are
we able to make the necessary and fundamental paradigm shift? <p>

<h2>Our Body of Knowledge</h2>

It is only reasonable, when considering a profession of centuries
standing, to take a rather long perspective on these issues. If we find
ourselves in an information technology era where our client no longer
researches under our tutelage, and if, in fact, the user is now ensconced
in his or her own private library courtesy of the Internet, then we need
to teach them not only how to be minimally functional with the necessary
tools and techniques, but in essence to be their own librarian. Internet
users, expected to learn complex indexing systems and to create their own
archives of information via bookmarks and downloaded files, will need to
operate on the same skill level as professional reference librarians. Our
traditional "point and name" library orientations and our two hour "Meet
the Internet" workshops are just not going to do the job. <p>

Consider the medieval guild model of apprentice training. We are the
experts, and we have apprentices in attendance as they use our libraries
and attend our classes. These apprentices do not need to learn the same
tools and theories we learned in library school. They need to learn
information seeking skills applicable to their fields, ones transferable
to new situations and new careers. The apprentice of medieval times was
taught applicable and concrete tasks in context, given increasingly
difficult problems presented with an expert available, allowing abstract
theories to evolve as competency grew. In most academic disciplines our
educational system uses an expert to deliver an abstract body of knowledge
to the unpracticed novice, who will later be expected to go out and apply
isolated rules learned in school to unprecedented and shifting work
situations. Similarly, we as bibliographic instructors teach theories of
access to library-structured knowledge through the application of
library-housed search tools. How will this training work, years later, for
the engineer, physician, business person, or lab technician who seeks an
answer to his immediate information need? Will he or she stop to consider
which is the correct periodical index to use and what subject headings are
most appropriate? <p>

The necessity for teaching real world, career applicable learning
strategies should be the focus of library instruction. If we are
responsible for teaching information seeking skills, then we should look
to the conclusions of the Secretary's Commission on Achieving Necessary
Skills (<a href="#scans">SCANS, 1991</a>) which described the need to
equip tomorrow's workers with information management, collaborative
problem solving, and critical thinking skills. The chairman of the SCANS
committee William Brock states that "the most effective way to educate our
youth is to teach them in the context of real-life situations and real
problems" (1991, p. 22). <p>

The lesson from the medieval guild model is represented in the cognitive
apprenticeship concept of Collins, Brown, Newman, and others. This theory
presents a learning environment that ignores the arbitrary delineation of
academic or abstract versus vocational or concrete learning, and places
the learner directly into a community of experts, confronted with
self-selected, increasingly more difficult tasks. The content of the
learning consists of "tricks of the trade," the heuristic, problem-solving
strategies experts rely on, cognitive management devices of goal setting,
strategic planning, monitoring, evaluating, and revision (<a
href="#collins">Collins et al, 1989</a>). The teaching methods employed
must give students exposure to experts' strategies by coaching, providing
scaffolding, then fading- gradually handing over the control of the
learning process (<a href="#berryman">Berryman, 1991</a>). <p>

There is no real difference between what librarians teach - information
skills requiring cognitive management processes, and what might
traditionally be thought of as task-oriented production skills. Following
the expert/apprentice model, it has been found that teachers with
experience in the workplace emphasize dispositions, or a set of attributes
that represent a specific enculturated point of view, rather than
stressing complex reasoning skills. These teachers minimize lecturing and
didactic instruction in favor of "micro-apprenticeships," project centered
courses, and collaborative solving of "authentic" problems (<a 
href="#statz">Statz et al., 1990</a>). <p>

Selecting and using the appropriate index or database (tool based
training), and abstract theories of librarian's subject content (knowledge
classification schemas such as Library of Congress or Dewey) do not in
themselves teach solutions to the authentic problems encountered by
today's information seekers. Librarians do the work of translating the
seeker's information queries into the appropriate research method, and
then present the most likely set of tools to employ. Bibliographic
instruction transfers only this understanding of task-oriented tools to
the learner. The librarian would serve the student best if we taught the
process of defining the information query, of designing the entire
research strategy, and then moved on to selection and evaluation of
research tools. An example of this would be an exercise in defining
keyword terms describing the search concepts, from global to discrete,
without the aid of library based tools such as the Library of Congress
Subject Headings, using mind-mapping, outlining, or simple flowcharting.
Another exercise would be for students to evaluate and map out the process
whereby the information they seek has come to be published, whether in
print or online, including an understanding of primary and secondary
research, and popular vs. scholarly publishing. This will give them a
sense of the likely source of the information they seek based on who
produced it and the intended audience. These are the heuristic skills
librarians practice and they are essential to developing information
literacy skills. <p>

The librarian as expert needs to teach the independent, online information
seeker more than any particular set of skills to attain information
literacy. The Internet user faces a constantly changing body of
"information," radically different from the information traditionally
warehoused in libraries, with tools that reflect the thinking of computer
programmers, far different from that of librarian catalogers. The true
disposition of the expert information seeker, librarian and Internet user
alike, must adapt to these shifting values. The constant evaluation
required, comparing the apparent, or the external, with the required, or
the internal, is the essence of critical thinking, and is the stock in
trade of the librarian. <p>

<h2>The Nature of Critical Thinking</h2>

John Dewey defined the nature of reflective thought as "active,
persistent, and careful consideration of any belief or supposed form of
knowledge in the light of the grounds that support it and the further
conclusion to which it tends" (<a href="#dewey">1938, p.9</a>). Critical
thinking is generally agreed to include the evaluation of the worth,
accuracy, or authenticity of various propositions, leading to a
supportable decision or direction for action. <p>

The knowledge domain of the librarian is the acquisition and evaluation of
information resources. The librarian usually does nothing more with this
knowledge base beyond offer it succinctly and freely to those who request
it. This evaluation of resources across subject domains, selecting the
valid and useful, is common practice of librarians. Free from the coaching
presence of the librarian, the online information seeker must exercise
these skills independently. Looking again to the SCANS recommendations for
basic skills, information management includes not only the process of
analyzing, selecting, and evaluating the information needed, but also
determining when new information must be created. Similarly, critical
thinking takes the learner beyond thoughtful reflection to analysis and a
determined course of action. This is the process that we need to teach our
clients. <p>

Information literacy has been defined recently as ranging from tool and
resource literacy to publishing literacy (communicate electronically),
emerging technology literacy (understand and incorporate new
technologies), and critical literacy (the intellectual and social value of
information). The last three literacies require creating, decision making,
and synthesizing other literacies into an understanding of the potentials
and limits of information technologies (<a href="#shapiro">Shapiro &amp; 
Hughes 1996</a>). <p>

Bloom's taxonomy, applied to the teaching of information literacy skills,
helps us evaluate our present and potential learning objectives. The
cognitive skill levels of knowledge, comprehension, and application are
covered in traditional bibliographic courses teaching the variety of
library resources and their particular uses. Analysis is attempted, while
synthesis and evaluation are untouched. Teaching the skills of evaluation,
especially in an evolving online publishing world with nonexistent
standards, is imperative. Teaching the user synthesis - finding the useful
out of the plethora of what is available and then creating new
information, is our challenge. <p>

<h2>Opportunities For Instruction</h2>

Significant and valuable inroads toward evaluation standards of online
information have been made. Designing our teaching curriculum should
certainly incorporate such standards, along with a basic understanding of
the technology that makes online search engines possible. Our
responsibility as information literacy instructors must extend further
however. <p>

Teaching the learner how to think critically means more than critical
analysis of webpages or comparison of search engines. Both are the medium
of the information, not the meaning. Prescribing a checklist of criteria
to look for or steps to take will leave the learner unprepared when
technology changes. Knowledge is transferred when it is embedded in a more
general understanding of its entire structure and contextualized into the
content familiar to the learner. Therefore, teaching evaluation of
information resources is best taught within the learner's knowledge base
and developed into a network of meaning as charted by the learner. 
Inquiry based instruction with real world applications in a collaborative
setting presents the best opportunity for transferable knowledge. Examples
of this approach could be a collaboratively designed and taught research
class with instructors from specific discipline areas proposing content
related problems that the students, instructors, and librarians jointly
solve. <p>

A constructivist framework for instruction shapes learning as an
individual construction within the learner's environment. Instructional
principles derived from constructivism include the following objectives: 
<p>
<ol>
<li>anchor all learning activities to a larger task,
<li>support the learner in developing ownership of the task,
<li>design an authentic task,

<li>design the task to reflect the complexity of the environment the
learner will face,

<li>support and challenge the learner's thinking,

<li>encourage testing ideas against alternative views and alternative
contexts, and

<li>provide opportunity for reflection on the content learned and the learning
process (<a href="#savery">Savery &amp; Duffy, 1995</a>).
</ol>
<p>

The last two principles relate strongly to developing critical thinking
attributes. Instructors of information resources need to bear in mind that
technology supports knowledge construction and does not define it.
Teaching technology based tools, the vehicles of online information,
should be part of the learning process and not an end in itself. <p>

<h2>Teaching Critical Thinking</h2>

The roots of the critical thinking movement in education are in the 1980
California State University Executive Order announcing the requirement of
formal instruction in critical thinking which stated: <p>

<blockquote><em>

Instruction in critical thinking is to be designed to achieve an
understanding of the relationship of language to logic, which should lead
to the ability to analyze, criticize, and advocate ideas, to reason
inductively and deductively and to reach factual or judgemental
conclusions based on sound inferences drawn from unambiguous statements of
knowledge or belief</em> (<a href="#dumke">Dumke, 1980</a>).</blockquote> <p>

Although appearing to be in the realm of philosophy or literature studies,
critical thinking now emphasizes the mental attitudes or "dispositions"
and the application of reasoning to everyday situations. <p>

Critical thinking across the disciplines share common features.
<p>
<ol>

<li>Critical thinking is a learnable skill with teachers and peers serving
as resources. 

<li>Problems, questions, and issues serve as the source of motivation for
the learner. 

<li>Courses are assignment centered rather than text or lecture oriented.

<li>Goals, methods, and evaluation emphasize using content rather than
simply acquiring it. 

<li>Students need to formulate and justify their ideas in writing.
<li>Students collaborate to learn and enhance their thinking (<a 
href="#meyers">Meyers 1986</a>). </ol>
<p>

These straightforward ideas are easily applicable to the online setting.
Instructors must refocus their thinking away from individual mastery of
the resources and the product of competency. The focus should be instead
on teaching the process of information discovery within the learner's own
contextual meaning. Will the sought after information solve the problem,
will it lead to learning, and the self-construction of knowledge? These
should be the leading objectives as we practice and mentor the goals of
"information literacy." <p>

<h2>An Instructional Design Project</h2>

The author, endeavoring to practice what she preaches, collaborated with
two English instructors to develop a self-paced "laboratory" course for
practicing critical thinking skills. The objective of the one-unit
workbook-based class is for the student to explore the information
resources of the Internet, employing the attributes of a critical thinker.
The simply stated criteria for critical thinking, agreed upon by the three
collaborating instructors are: 
<p>
<ol>
<li>Differentiate between fact and opinion.
<li>Examine the assumptions, including your own.

<li>Be flexible and open minded as you look for explanations, causes, and
solutions to problems. 

<li>Be aware of fallacious arguments, ambiguity, and manipulative reasoning.
<li>Stay focused on the whole picture, while examining the specifics.
<li>Look for reputable sources.
</ol>
<p>

Starting with the concept of the world of information from personal
observation to cultural assumptions of what we "know," the student is
given the "tools" to locate information on the Internet and the
explanation of how they function only when as the tools become necessary.
The student initiates his or her inquiry with a generally accepted
knowledge base - the weather, and proceeds to a self-selected field of
expertise or interest. Guidelines for evaluation of resources are
introduced to the student, and the student is asked to select and compare
research tools and the resultant findings, suggest reasons for the
results, and possible modifications of the search process. The student
will then write a research paper incorporating the selected, evaluated,
and synthesized online information. <p>

The instructional design parameters for self-paced instruction included
these factors: 
<p>
<ol>
<li>Instructional objectives stated initially to the learner.
<li>The learner selects his or her own path of inquiry.
<li>Small steps, with the necessary tools introduced only as they are needed.
<li>Frequent student interaction, requiring high level cognitive involvement.

<li>Alternative paths available for variable levels of involvement or
usefulness. 

<li>Evaluation of the process, not just the resources uncovered, with
considerations of the value of the information, success of the endeavor,
and other (non-online) possibilities of solving the inquiry. 

</ol>
<p>

We do not expect our learner to travel down our same path as librarian or
researcher but to become independent knowledge seekers. There is no right
or wrong process of research, although there are many heuristics we can
pass on. Applicable use of information requires that we see knowledge
acquisition as amorphous and changing. As librarians, so we are too. Let
us teach those who come to us our strengths, not our past. <p>

<hr width=200>
<p>

<h2>References</h2>

<a name="berryman">
<strong>Berryman, S. E.</strong></a> 1991. <cite>Designing effective learning
environments: Cognitive apprenticeship models</cite>. (Eric Document
Reproduction Service No. ED 337689). <p>

<strong>Bloom, B. S.</strong> 1960. <cite>Taxonomy of educational
objectives: Cognitive domain/affective domain</cite>. David Mackay
Company, New York. <p>

<a name="collins">
<strong>Collins, A., Brown, J. S., &amp; Newman, S.</strong></a> 1989.
Cognitive apprenticeship: Teaching the craft of reading, writing, and
mathematics. In: <cite>Knowing, learning, and instruction: Essays in honor
of Robert Glaser</cite> (ed. by L. B. Resnick). Erlbaum, Hillsdale, NY.
<p>

<a name="dewey">
<strong>Dewey, J.</strong></a> 1933. <cite>Experience and education</cite>. 
Macmillan, New York. <p>

<a name="dumke">
<strong>Dumke, G.</strong></a> 1980. <cite>Chancellor's Executive Order
338</cite>. California State University, Chancellor's Office, Long Beach.
<p>

<a name="meyers">
<strong>Meyers, C.</strong></a> 1985. <cite>Teaching students to think
critically</cite>. Jossey Bass, San Francisco. <p>

<a name="savery">
<strong>Savery, J. R. &amp; Duffy, T. M.</strong></a> 1995. Problem based
learning: An instructional model and its constructivist framework.
<cite>Educational Technology</cite> 33(1) 31-38. <p>

<a name="shapiro">
<strong>Shapiro, J. J. &amp; Hughes, S. K.</strong></a> 1996. Information
technology as a liberal art: Enlightenment proposals for a new curriculum.
<cite>Educom Review</cite> 31(2) 31-35. <p>

<a name="statz">
<strong>Statz, C. McArthur, D., Lewis, M., &amp; Ramsey, K.</strong></a>
1991.  <cite>Teaching and learning generic skills for the
workplace</cite>. National Center for Research in Vocational Education,
University of California, Berkeley, CA (ERIC Document Reproduction Service
No. ED 329682). <p>

<a name="scans">
<strong>United States. Dept. of Labor. Secretary's Commission on Achieving
Necessary Skills.</strong></a> 1991. <cite>What work requires of schools : a
SCANS report for America 2000</cite>. Secretary's Commission on Achieving
Necessary Skills, U.S. Dept. of Labor, [Washington, D.C.]

<p>

<P> <A HREF="http://validator.w3.org/"><IMG SRC="http://www.library.ucsb.edu/gallery/valid_html3.2.gif" ALT="HTML 3.2 Checked!" border=0></A>


</body></html>
