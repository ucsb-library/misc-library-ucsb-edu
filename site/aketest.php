<HTML>
<head>
<title>Akebono PHP Include Test</title>
</head>
<body>
<center><H1>Akebono PHP Include Test</H1></center>
<?php
$TESTINCLUDEFAIL = true;
$IncludeFiles = array('Date.php' => '/usr/share/php/Date',
                      'authentication.php' => 'Unknown',
					  'josephine.php' => '');
$FileStatus = array();

function findpkg($filename, $path, &$FileStatus) {
  $bf='<font color="blue">';
  $rf='<font color="red">';
  $ef='</font>';
  echo '&nbsp;' . "<br>\n";
  echo '&nbsp;==> ' . "Searching for File $filename In PHP Include Path<br></em>\n";
  $present = stream_resolve_include_path("$filename");
  if($present) { 
   echo '&nbsp;==> ' . "Found File <em>" . $filename . "</em> To Include in PHP Code<br>\n";
   echo '&nbsp;==> ' . "PHP Discovered File Path Location: " . $present . "<br>\n";
   echo '&nbsp;==> ' . $bf . "This File Can Be Included in PHP Code" . $ef . "<br>\n";
   $FileStatus[$filename] = true;
  } else { 
    echo '&nbsp;==> ' . "Did NOT Find File <em>" . $filename . "</em><br>\n";
    echo '&nbsp;==> ' . "This File Can NOT Be Included in PHP Code<br>\n";
	if($path !== '') {
      echo '&nbsp;==> ' . $rf . "File Should Exist At <em>" . $path . "</em>" . $ef . "<br>\n";	  
	} else {
      echo '&nbsp;==> ' . "This Is A Test File That Should NOT Exist<br>\n";
      echo '&nbsp;==> ' . $bf . "This Is The Proper Result In This Case" . $ef . "<br>\n";		  
	}
   $FileStatus[$filename] = false;
  }
}

function findpkgold($filename, $path, &$FileStatus) {
  echo '&nbsp;' . "<br>\n";
  echo '&nbsp;==> ' . "Searching for File $filename In PHP Include Path<br></em>\n";
  $paths = explode(PATH_SEPARATOR, get_include_path());
  $found = false;
  foreach($paths as $p) {
    $fullname = $p.DIRECTORY_SEPARATOR.$filename;
    if(is_file($fullname)) {
      $found = $fullname;
      break;
    }
  }
  if($found) {
   echo '&nbsp;==> ' . "Found File <em>" . $filename . "</em> To Include in PHP Code<br>\n";
   echo '&nbsp;==> ' . "PHP Discovered File Path Location: " . $fullname . "<br>\n";
   echo '&nbsp;==> ' . "This File Can Be Included in PHP Code<br>\n";
   $FileStatus[$filename] = true;
  } else {
    echo '&nbsp;==> ' . "Did NOT Find File <em>" . $filename . "</em><br>\n";
    echo '&nbsp;==> ' . "This File Can NOT Be Included in PHP Code<br>\n";
	if($path !== '') {
      echo '&nbsp;==> ' . "File Should Exist At <em>" . $path . "</em><br>\n";	  
	} else {
      echo '&nbsp;==> ' . "This Is A Test File That Should NOT Exist</em><br>\n";
      echo '&nbsp;==> ' . "This Is The Proper Result In This Case</em><br>\n";		  
	}
    $FileStatus[$filename] = false;
  }
}

$gip = get_include_path();
$igp = ini_get('include_path');
echo '&bull; ' . "This Is The Include Path (via get_include path): " . $gip . "<br>\n";
echo '&bull; ' . "This Is The Include Path (via ini_get):          " . $igp . "<br>\n";


$respath = true;
if (!function_exists('stream_resolve_include_path')) {
  echo '&nbsp;- ' . "Function <em>stream_resolve_include_path</em> Not In This PHP Version<br>\n";
  $respath = false;
} else {
  echo '&bull; ' . "Searching for Included Files Using <em>stream_resolve_include_path<br></em>\n";
  foreach($IncludeFiles as $file => $path) 
    findpkg($file, $path, $FileStatus);  
}

if(!$respath) {
  echo '&bull; ' . "Searching for Included Files Using <em>PATH_SEPARATOR</em> & <em>get_include_path</em><br></em>\n";
  echo '&bull; ' . "Typically This Method Is Used In PHP Versions Prior to 5.3<br>\n";
  foreach($IncludeFiles as $file => $path) 
    findpkgold($file, $path, $FileStatus);  
  }

if($TESTINCLUDEFAIL) {
  $allgood = true;
  echo '&nbsp;<br>&bull; ' . "Script Flag TESTINCLUDEFAIL Is Currently Set True<br></em>\n";
  echo '&bull; ' . "Testing For PHP Performance With (Potentially) Bad Include Files<br></em><br>\n";
  foreach($IncludeFiles as $file => $path) {
	$status = $FileStatus[$file];
	if($status) { $fstat = "PHP Knows How to Include This File. Should Be No Problems Here."; }
	else        { $fstat = "PHP Cannot Find This File. Code Will Bomb Unless Ignored By PHP."; $allgood = false; }
    echo '&nbsp;==> ' . "Including File " . $file . ". " . $fstat . "<br>\n";	
    include $file;	
  }
  if(!$allgood)
	echo "<center><h2>If You See This Page PHP Is Ignoring Bad Includes!</h2></center>\n";
}

?>
</center></H1>
</body>
</html>

