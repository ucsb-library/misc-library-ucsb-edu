<html>
<head>
<title>LAUC Research Grant | Academic Search Complete questions</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFCEB" text="000033">
<table width="640" border="0" cellspacing="3" cellpadding="3" align="center">
  <tr valign="top">
    <td>

            <p><font face="Verdana, Arial, Helvetica, sans-serif"><font color="1F4366" size="+2">Way
              to go!</font></font></p>
            <p><font face="Verdana, Arial, Helvetica, sans-serif">You've completed Part 2 of the tutorial and you have learned:</font></p>
            <ul>
              <li><font face="Verdana, Arial, Helvetica, sans-serif">about Academic Search Complete and a few highlighted features</font></li>
                    </ul>
      <ul>
              <li><font face="Verdana, Arial, Helvetica, sans-serif">how to read a results screen and identify the parts needed for a bibliography</font></li>
                    </ul>
      <ul>
              <li><font face="Verdana, Arial, Helvetica, sans-serif">how to use UC-eLinks to locate full text of an article</font></li>
            </ul>
            <p><font face="Verdana, Arial, Helvetica, sans-serif">Now let's take a quick quiz!</font></p>
            <table width="100%" border="0" cellspacing="3" cellpadding="3" align="center">
        <tr>
          <td width="25%" valign="middle" align="center">
            <p align="CENTER">
            <form method="get" action="quiz.php">
              <div align="CENTER">
                <input type="submit" name="quiz" value="Lets go!">
              </div>
            </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
