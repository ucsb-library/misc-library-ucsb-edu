<html>
<head>
<title>Module 1: Starting Smart. Searchpath. UCSB Libraries.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFCEB">
<table width="640" border="0" cellspacing="3" cellpadding="3" align="center">
  <tr valign="top"> 
    <td> 
      
 <p align="left"><img width="470" height="71" src="../images/hdstart.gif" alt="Starting Smart"></p>
      <p align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><font size="+2" color="1F4366">QUIZ</font></font></p>
      <p><font face="Verdana, Arial, Helvetica, sans-serif" color="000333">The 
        final section of this module is a 5 question quiz.</font></p>
      <ul>
        <li><font color="000333" face="Verdana, Arial, Helvetica, sans-serif">Choose 
          your answer, then click the submit button. Sometimes, you may have to 
          scroll down to find the submit button.</font></li>
      </ul>
      <ul>
        <li><font color="000333" face="Verdana, Arial, Helvetica, sans-serif">Searchpath 
          will display a response to your answer. Read the response, then go to 
          the next question.</font></li>
      </ul>
      <ul>
        <li><font color="000333" face="Verdana, Arial, Helvetica, sans-serif">You 
          can take the quiz over again, but you must complete it before retaking 
          any questions.</font></li>
      </ul>
      <ul>
        <li><font color="000333" face="Verdana, Arial, Helvetica, sans-serif">The 
          last page of the quiz displays a summary of your answers. You will be 
          able to print out or email your quiz results to your instructor.</font></li>
      </ul>
<ul>
        <li><font color="000333" face="Verdana, Arial, Helvetica, sans-serif">If you 
use the Safari browser, your results may not display correctly. Use Firefox or Internet 
Explorer.</font></li>
      </ul>
      <table width="100%" border="0" cellspacing="3" cellpadding="3" align="center">
        <tr> 
          <td width="25%" valign="middle" align="center"> 
            <div align="left"></div>
            <p align="CENTER"> 
             <form method="get" action="quiz.php">
              <div align="CENTER"> 
                <input type="submit" name="quiz" value="So quiz me already.">
              </div>
            </form>
            <br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
