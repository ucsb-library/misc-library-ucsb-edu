From mfanshie@sassy.ucsd.eduWed Apr 23 11:12:50 1997
Date: Tue, 22 Apr 97 19:37:41 PDT
From: Marsha Fanshier <mfanshie@sassy.ucsd.edu>
To: duda@ariz.library.ucsb.edu
Subject: FINAL FINAL paper

<HTML>
<HEAD>
<TITLE>Survey Data on your Desktop</TITLE>
</HEAD>
<BODY bgcolor="#FFFFFF">

<CENTER>
<H1>Survey Data On your Desktop</H1>
<H2> The Future for Library Managers</H2>
<STRONG><a HREF="mailto:rcoates@ucsd.edu">Renata G. Coates</a>, Associate Librarian<br>
<a HREF="mailto:mfanshier@ucsd.edu">Marsha Fanshier</a>, Programmer/Analyst</STRONG>
<br>
Social Science &amp; Humanities Library
<br>
University of California, San Diego
<br>
La Jolla, California
</CENTER>
<P>

<h2>Abstract</h2>
<blockquote>
The UCSD Libraries User Survey was designed to have its results in  
machine-readable form.  Library management, anticipating the need for 
detailed statistics for future decision making, required that 
survey results be provided in a format directly usable by  library 
managers. To accomplish this goal, the survey had to be designed in 
a particular manner  and subsequently, programming skills were needed to 
create an interactive website.  The results allow survey responses to 
be considered against a number of variables including but not 
limited to-- gender, year in school, major, department, and years at the University by faculty.  Branch managers can review answers 
dealing specifically with their services or operations.  More importantly, 
for the first time ever, this type of data is available to the 
individual's desktop allowing for universal access to anyone interested in 
this information.   The 1996 UCSD Libraries User Survey solicited responses 
from three categories of primary users: undergraduate, graduate students/School of 
Medicine students, and faculty/researchers. Future plans call for a repeat 
of this survey.
</blockquote>
<P>

In 1995 as part of a larger undertaking of organizational change, the UCSD Libraries decided to conduct a user survey of its primary clientele-- faculty, students and staff.  Subsequently, a nine member library-wide user survey team and a consultant from the business community were empowered to administer the entire process. </P>
<P>
The process took the better part of a year  and  in the end we had survey results in machine-readable form. It was the intent of library management that this numeric data be used by library department heads and unit managers in making future managerial decisions.</P>

<P>In the last weeks of the survey process, the Libraries User Survey Team involved members of the Data Services Group in the project. The original request to the Data Services Group was for assistance in housing and archiving the data, providing access to it, and most importantly, according sub-setting capabilities by primary library.</P>

<P>The simplest solution to all our requests, would have involved putting the data files on the web in ASCII format. The Data Services group however, chose to go beyond this simple implementation by 
providing the same functionality for the <I>survey data</I> that it  provides in its other web-based data projects.  They welcomed the opportunity to show-case new and enhanced retrieval powers and ease-of-use features. Additionally, they wanted to meet the needs of library managers whose specific branch level questions could not be answered in sufficient detail by the pre-formatted charts and tables already available on the User Survey Website 
(<a HREF="http://orpheus.ucsd.edu/survey">http://orpheus.ucsd.edu/survey</a>). 
To that end, they expanded the project's definition to include the following:  interactive query-based  access from WWW forms, formatted tables, analytical functionality including cross-tabs and graphing, and lastly, data file format export capabilities.</P>


<H2>Interactive Access and Analysis of Data via the WWW</h2>
<P>
Server-side analysis of data is a new feature that the Data Services
group is providing with some web-based data projects. 
Traditionally the library has provided the data expecting the end-users
to do their own analysis (client-side analysis).   
Given that we can not anticipate all the analysis needs users
will have this is still the most important role for Data Services.
Taking advantage of the tools available for the WWW and current
computing technologies this traditional service has been expanded to 
provide limited interactive analysis capabilities (server-side analysis).
Server-side analysis allows users the ability to answer simple data 
questions quickly and easily as well as providing data exploration 
tools to those users who have more complex questions.   In the long run 
we believe that these features will result in greater access 
to our data collection.
<P>
<H3>Design of HTML Forms</H3>
<P>
The Library User Survey data sets included a relatively small number
of variables that were well documented by the consultant.
This allowed us the luxury of turning the codebooks into the 
HTML query forms and freeing us from spending a lot of time developing
the user-interface. The codebooks
included the text of the questions creating simple, user-friendly
interfaces from which to make interactive queries.
<P>
SAS data sets were created for each group surveyed from
SAS export files provided by the consultant. Two HTML forms are
available for each data set. The 
<A HREF="http://ssdc.ucsd.edu/lib_surv/ugrad_anal.html">
<I><B>Analyze/ChartData</B></I></A>
form provides server-side analysis features where charts and tables
are created <I>on-the-fly</I>. The 
<A HREF="http://ssdc.ucsd.edu/lib_surv/ugrad_sub.html">
<I><B>Download/Subset</B></I></A>
menu can be used to subset the data for client-side analysis with
spreadsheet or statistical software.
<P>
<CENTER>
<TABLE cellpadding=2 cellspacing=2 bgcolor="beige" width=75%>
<TR>
<TH bgcolor="cornflowerblue">Server-side Analysis</TH>
<TH bgcolor="cornflowerblue">Client-side Analysis</TH>
</TR>
<TR>
<TD width=50% valign=top>
<UL>
<LI>Charts
<LI>Tables
<LI>Limited choice
</UL>
</TD>
<TD width=50% valign=top>
<UL>
<LI>Subset
<LI>Download
<LI>Export format
<LI>Full-access
</UL>
</TD>
</TR>
</TABLE>
</CENTER>
<P>
The <B><I>Analyze &amp; Chart Data</I></B>
menus require a primary analysis variable preselected from the table
below.
Part II of the menus allows the user to select one variable
from any in the dataset to compare against the primary variable.
<P>
<CENTER>
<TABLE cellpadding=2 cellspacing=2 bgcolor="beige" width=75%>
<TR>
<TH valign=bottom bgcolor="cornflowerblue">
Undergraduate Students  
Menu
</TH>
<TH valign=top bgcolor="cornflowerblue">
Faculty/Graduate-Medical         
<BR>
Student Menu
</TH>
</TR>
<TD width=50% valign=top>
<UL>
<LI>Primary Library
<LI>Sex of Respondent
<LI>Major Field
<LI>Class Level
<LI>College
</UL>
</TD>
<TD width=50% valign=top>
<UL>
<LI>Primary Library
<LI>Major Field
<LI>Title
<LI>Sex of Respondent
</UL>
</TD>
</TR>
</TABLE>
</CENTER>


<P>
Variable names and values are embedded into the forms using the
<B>input</B> tag as
<P>
<CENTER>
<B>&lt;input type=radio name=keep value=hours&gt;Library hours</B>
</CENTER>

<P>
When the <B>submit</B> button is pressed the <B>CGI</B> program
will be called with the browser passing to it the selected variable
names and values. The program will extract the data from the <B>SAS</B>
data set and output a chart and table comparing the two variables.
<H3>Output Files</H3>

<H4>Analysis Menu <I>(Server-side analysis)</I></H4>

<P>
The output from the <I><B>Analysis Menu</B></I> displays a graphic
comparison of the two variables as well as a table representing
the numeric values and percentages of the two variables selected
by the user.
<P>
<CENTER>
<TABLE width=75% bgcolor=beige>
<TD>
<A HREF="http://ssdc.ucsd.edu/mmf/libex/lib_ex.html"><b>Output charts and tables</b></A> 
are quickly and easily available through
the intra/internet to administrators and public service providers
within the library. They can be used to answer simple questions
or to be printed and included in reports.
</TABLE>
</CENTER>
<P>
<H4>Subset menu <I>(Client-side analysis)</I></H4>

<P>
Output files created from the <I><B>subset menu</B></I> reside
on the server's disk for approximately one-half hour. Export files
are available through FTP links that are output to the browser
and have specific extension names. With proper configuration of
the user's browser these files can be launched directly into spreadsheet
and statistical programs. Otherwise they can be downloaded to
disk <I>(without actually being read by the browser)</I> and then
read into the appropriate software program.
<CENTER>
<TABLE>
<TR>
<TH bgcolor=cornflowerblue>
Export Formats
</TD>
</TR>
<TR>
<TD align=center valign=center>
<FORM>
<SELECT name="export_lib">
<OPTION SELECTED value="dbascii:ASCII">ASCII
<OPTION value="dbdbf:dBase">dBase
<OPTION value="dbexcel4:Excel V3 and V4">Excel V3 and V4
<OPTION value="dbexcel5:Excel V5">Excel V5
<OPTION value="dblotus:Lotus 1-2-3 V1 to V2 & Quattro">Lotus 1-2-3 V1 to
V2 & Quattro
<OPTION value="dblotus3:Lotus 1-2-3 V3 to V5">Lotus 1-2-3 V3 to V5
<OPTION value="dbqpro:Quattro Pro">Quattro Pro
<OPTION value="dbqpro5:Quattro Pro5">Quattro Pro5
<OPTION value="dbrats:Rats">Rats
<OPTION value="dbsas:SAS for Windows">SAS for Windows
<OPTION value="dbsaspc:SAS for PC/DOS">SAS for PC/DOS
<OPTION value="dbspsspc:SPSS for PC/DOS">SPSS for PC/DOS
<OPTION value="dbspssx:SPSS for Sun & Windows">SPSS for Sun & Windows
<OPTION value="dbspsspr:SPSS Portable">SPSS Portable
<OPTION value="dbsystat:Systat">Systat
</SELECT> 
</TD>
</TR>
</FORM>
</TABLE>
</CENTER>
<P>
An HTML link to a <I>formatted ASCII table</I> of the numeric
data in a human-readable form is also provided.
<H3>Programming</H3>
<P>
The <B>CGI</B> program called by all of the <I>Survey</I> HTML
forms is written in <B>PERL</B>. It calls the <B>SAS</B> data
engine which subsets and extracts the data from the <B>SAS</B>
data sets. <B>SAS</B> functions are called to cross-tab, format
and chart the results. <I>Export</I> functionality is provided
through the add-on product <B>DBMS Copy.</B>

<P>
<CENTER>
<TABLE cellpadding=2 cellspacing=2 bgcolor="beige" width=75%>
<TR>
<TH bgcolor="cornflowerblue">
CGI</TH>
<TH bgcolor="cornflowerblue">
SAS</TH>
</TR>
<TR>
<TD width=50% valign=top>
<UL>
<LI>Parse variables and
preferences from HTML form
<LI>Build SAS command script
<LI>Run SAS script
<LI>Output HTML links to
files <I>(client-side)</I>
<LI>Output chart and table
<I>(server-side)</I>
</TD>
<TD width=50% valign=top>
<UL>
<LI>Extract data
<LI>Analyze data
<LI>Create output files
<LI>Call DBMS export routines
</UL>
</TD>
</TR>
</TABLE>
<P>
</CENTER>
<P>
The CGI and SAS scripts being used for data access are still 
evolving.   The Library Survey data project provided a simple,
elegant set of data to work with to add to our library of routines.
Many of the routines used had already been developed for earlier projects 
allowing us to develop this system in a short period of time.
The routines developed for the Library Survey have
proved very useful in more recent development projects such as
<A HREF="http://gort.ucsd.edu/gss">The General Social Survey.</A> 
<P>


<H2>Data Applications</H2>
<P>
Once the Data Services group completed its work, all library
managers and staff were encouraged to review the survey results.
Much of the overall general information seemed to confirm comments
that had been received via the suggestion box, ie., the temperature
of the building was too cold.
Specific operational units, however, found the detailed statistics
to be most useful.   Operations like Inter-library loan and Library
Express now had detailed insights to specific patron needs and
concerns.
It is beyond the scope of this presentation to describe specific
UCSD responses to the survey.
The authors have instead chosen to illustrate using survey data
as a process of decision making by a hypothetical branch manager.
<P>
<H3>A management issue: Library Hours</H3>
<P>
All libraries today are struggling  with the issue of library hours.  Except  for  a few academic institutions that are able to keep their libraries open around the clock, most of us provide less than 24 hours per day of access to building collections.  In one student focus group we conducted, we learned that our undergraduates wanted library buildings to be open longer hours, preferably  into the early hours of the
morning.   Since our campus has nine library branches, satisfying this particular user request would not be simple or cheap.</P>

<P>Keeping library buildings open longer involves more than just paying an additional heating or lighting  bill.  Even if services are scaled to a minimum, additional staff is needed to insure the safety and security of patrons in very large buildings.   The vigilance provided by library staff during the day, is not available nights and hence many libraries hire additional security staff. This added staffing need  means more payroll dollars.  Even if our branch managers wanted to keep their buildings open later, they could not make that decision without understanding the budgetary impact of such a move. </P>
<P>In addition to the fiscal issue, there are numerous other questions to be asked.  Do we need to keep all the buildings open longer or is it possible to satisfy this request by extending the hours at only one building?   Was this an issue for graduate students as well?  Maybe more importantly,  did the faculty want longer hours at specific branches?</P>
<P>It is at this stage that the 

work done by the Data Services Group becomes invaluable and directly applicable.  By retrieving the answers to survey questions provided by our three user groups
(<I>undergraduate, graduate /SOM, and faculty</I> )
the library manager can determine which branch libraries are candidates for a change in hours.</P>

<P>One of the best quantified survey results, indicates that 
the majority of our  undergraduates 
use libraries as places to study. </P>

<CENTER>
<IMG SRC="http://ssdc.ucsd.edu/mmf/libex/Image1.gif">
<P>
<IMG SRC="http://ssdc.ucsd.edu/mmf/libex/Image2.gif">
</CENTER>

<P>The graph immediately above indicates library usage on the average of once per week for all branches except IRPS (International Relations/Pacific Studies) and Special Collections.  That is  consistent with the fact that the IRPS library has as 
its 
primary users, graduate students and faculty.  Special Collections on our campus is primarily an archive and a rare book collection rather than a normal study space for students.   The heaviest used study libraries  appear to be  the science libraries: the Science &amp; Engineering, the BioMedical library, and the Scripps Institute of Oceanography Library.</P>

<P>By combining the fuchsia bar (once a wk) with the green bar 
(once every 2-3 wks), one can see that  better than 
eighty percent of the sampled  students study in campus libraries 
regularly . The fact that a very small percentage of 
students never study at UGL (the Undergraduate Library) is interesting 
since all undergraduate reserves are housed at UGL.  It may 
be because many UCSD classes now have students purchasing class 
readers [a compilation of photocopies articles]
rather than placing materials on reserve.</P>

<P>The graph below shows that more than one half of 
the sophomores, juniors and seniors study at a library once 
a week (fuchsia bar). This data supports the information in 
the previous graph that students are making heavy use of UCSD 
libraries as places to study.</P>

<CENTER>
<IMG SRC="http://ssdc.ucsd.edu/mmf/libex/ImageX.gif">
</CENTER>

<P>Now that the question about whether students are really using the library is clearly established, the branch manager needs to document the request for longer hours.</P>

<P>Running a data analysis on the Undergraduate population against the choice of longer hours on the question of  which future service or resource they wanted the UCSD libraries to offer, gives us the following table.</P>


<CENTER>
<IMG SRC="http://ssdc.ucsd.edu/mmf/libex/Image3.gif">
</CENTER>


<P>The most obvious thing about this chart is the overall dominance  of the blue bar indicating that longer hours was not one of the 
top three choices for a large percentage of students.  The green bar 
(indicating a 
ranking of 1, hence of highest priority) 
is the next largest response. </P>

<P>Since the issue of library hours had come up in 
focus group discussions and in early survey design sessions, the 
consultant specifically decided to ask a &quot;which longer library hours&quot; 
question on the survey.  The students were asked to choose 

all that applied;
longer library hours: weekdays, longer library hours: weekends, longer library 
hours: holidays. The next three graphs show their answers.</P>

<CENTER>
<TABLE height=70 width=75% bgcolor="beige">
<TD valign=center align=center>
<a HREF="http://ssdc.ucsd.edu/mmf/libex/week.html"><B>Tables of hour preferences</B></a> by weekday, weekend
and holiday.
<BR>
[http://ssdc.ucsd.edu/mmf/libex/week.html]
</TD>
</TABLE>
</CENTER>


<P>The request for longer weekday hours  applies to all branches with the exception of Special Collections and IRPS (International Relations/Pacific Studies) which is consistent with what we already know. The last graph shows that students want longer weekend hours at all the branches with the exception of IRPS.</P>


<P>And what was the students response regarding longer holiday hours.? Well at least this is clear.  Students, no more than staff, want to come to the library on holidays.</P>


<P>Now that the issue of library hours is well documented in terms of the  Undergraduates, are these opinions shared by graduate students and faculty.  It will  certainly be 
easier for branch managers to make a case for increasing library hours if they can be shown that all three primary user groups provided the same feedback.</P>

<P>The graph below shows the graduate students and faculty responses on longer library hours. This table does not tell us the reason for these responses but clearly the faculty and graduate students are not in agreement with the undergraduates.</P>

<CENTER>
<IMG SRC="http://ssdc.ucsd.edu/mmf/libex/Image7.gif">
</CENTER>
<P>This is certainly a problem for making a case for longer library hours. The larger question however is why the difference between these groups.</P>



<P>Now granted, faculty don't study at the library and with electronic access and document delivery (called Library Express on our campus), perhaps they no longer come to the library.  But surely, graduate students must  be studying in the library.  After some serious review of the survey data, additional clues  about faculty and graduate student use of the library comes to light.</P>

<P>The two graphs below shows the frequency with which the UCSD faculty access libraries from their home or office. The first graph indicates frequency of usage by various titles and the second one is by their department.</P>


<CENTER>
<TABLE height=70 width=75% bgcolor="beige">
<TD valign=center align=center>
<a HREF="http://ssdc.ucsd.edu/mmf/libex/facaccess.html"><B>Tables of Faculty Access of Libraries</B></A>
<BR>
[http://ssdc.ucsd.edu/mmf/libex/facaccess.html]
</TD>
</TABLE>
</CENTER>


<P>The first graph suggests that faculty at the lower ranks 
don't access the library electronically as often as their seniors.  Now this data doesn't tell us why, but we could speculate that it might be because their 
don't have the resources either in the offices or at home.  The fact that better than 50 percent of the Associate Professors log in 2 or more times a week is interesting.  Taking the three professional ranks together, it's quite amazing at how actively our faculty access the  library. This is information library managers did not know prior to the survey being taken.  
If a large number of faculty access the library remotely, then it makes sense that library hours are not a major issue for this group. </P>

<P>The second graph shows the information from a different point of view.  It's quite apparent that graduate students and faculty using the CMRR Library and Special Collection 
don't have an issue with library hours.  CMRR is the Center for Magnetic Recording  a library facility used primarily by faculty and researchers. 
It would 
appear however, that  percent of the polled faculty from the Scripps Institution of Oceanography and those who primarily using the Art and Architecture library, would like longer hours. </P>



<P>Does this suggest that faculty never come into the library 
any more?   Not at all. When you combine the more 
than 2 times per week with the once a week 
percentiles in the graph, it shows that better than 
one half of the faculty still visit in person every 
week.   From our knowledge of library patron behavior, it would 
be easy to assume that a fair number of quick look-ups 
are now done on-line (instead of calling the reference desk) 
and trips to the library are reserved for larger and 
more complex needs. If such library usage by faculty is 
an increase over previous years, we do not know that at this time.
The 1996 
survey will be our benchmark for future data gathering.</P>



<P>So how did graduate students and students in the School of Medicine
use the library and did they want longer hours?  
The tables below show the answers.</P>


<CENTER>
<TABLE height=70 width=75% bgcolor="beige">
<TD valign=center align=center>
<a HREF="http://ssdc.ucsd.edu/mmf/libex/faculty.html"><B>Tables of Faculty/Grad (SOM) usage of library</B></a>
<BR>
[http://ssdc.ucsd.edu/mmf/libex/faculty.html]
</TD>
</TABLE>
</CENTER>

<P>It appears that graduate students in all disciplines use the library frequently.  Adding the fuchsia line with the green line indicates that better than 50 percent use the library weekly.</P>

<H2>Conclusions</H2>
<P>The user survey information available to our hypothetical branch manager as a result of our 1996 efforts, has provided  a sufficient  amount of information to make recommendations to the library administration. For the first time every, he/she has the hard data to substantiate the need to increase library hours at specific branches.  Even better, this information was easily obtainable from their own desktop.</P>
<H2>References</h2>
<P>
<B><P>Applegate, Rachel</B>. 1993. Models of User Satisfaction: Understanding False Positives. <I>RQ</I> 32(4):525-539.</P>
<P>
<B><P>Berger, Kenneth W. &amp; Richard W. Hines.</B> 1994.  What Does the User Really Want?  The Library User Survey Project at Duke University.  <I>Journal of Academic Librarianship </I> 306-309.</P>
<P>
<B><P>Burns, Robert W. Jr.</B>  1973.  A survey of User Attitudes Toward Selected  Surveys Offered by Colorado State University Libraries.  Colorado State University, Ft. Collins, Co.  <I>ERIC Document ED086261</I>.</P>
<P>
<B><P>Crist, M., Daub, P. &amp; MacAdam. </B> 1994.  User Studies: Reality Check and Future Perfect.  <I>Wilson Library Bulletin</I> 68(6): 38-41.</P>
<P>
<B><P>Dillman, Don A</B>.  1978<I>.  Mail and Telephone Surveys: The Total Design</I><U> </U><I>Model.</I>  New York: John Wiley and Sons.</P>
<P>
<B><P>Frey, Thomas K.</B>  1995. Undergraduate Perceptions of Library Service: Use of Focus Groups and Surveys in Strategic Planning.  In: <I>Continuity &amp; Transformation The Promise of Confluence: Proceedings of the Seventh National Conference of the Association of College and Research Libraries. </I>(ed. by  Richard AmRhein) pp. 41-50. ACRL, Chicago, Il.</P>
<P>
<B><P>Kirk, R. </B> 1995.  What Do Our Readers Want? Some Conclusions from a Survey Made at Leicester University: An Education Library Slant.  <I>Education Libraries Journal</I>  38(1): 5-16.</P>
<P>
<B><P>Meldrem, J. A., Johnson C. V. &amp; Ury, C. J.</B>  1995.  Customer Input into Library Decision Making.  In: <I>Continuity &amp; Transformation The Promise of Confluence: Proceedings of the Seventh National Conference of the Association of College and Research Libraries. </I>(ed. by Richard AmRhein) pp.181-185. ACRL, Chicago, Il.</P>
<P>
<B><P>Schlichter, Doris J. &amp; Pemberton, J. Michael.  </B>1992.  The Emperorís New Clothes? Problems of the User Survey as a Planning Tool in Academic Libraries.  <I>College and Research Libraries</I> 53(3): 257-265.</P>
<P>
<B><P>UCSD Libraries User Survey Team</B>. 1995.  <I>UCSD Libraries User Survey</I>.
<a HREF="http://orpheus.ucsd.edu/survey">[http://orpheus.ucsd.edu/survey]</a>.</P>
<P>
<I><P>User Surveys.</I> 1988. ARL SPEC Kit #148. Washington DC: Office of Management Services, Association of Research Libraries.</P>
<P>
<I><P>User Surveys in ARL Libraries. </I>1994.<I> </I> ARL SPEC Kit  #205.  Washington DC: Office of Management Services, Association of Research Libraries.</P>
<P>
</BODY>
</HTML>
