<?php
ob_start();
$question = 1;
$next_question = $_REQUEST["next_question"];
$q1 = $_REQUEST["q1"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered


if (count($q1) < 2 && $next_question == 2){
    $err_msg = "Please select two choices";
    $next_question = 1;
}
if (count($q1) > 2 && $next_question == '2'){
    $err_msg = "Please select only two choices";
    $next_question = 1;
}
//Print and record question results
if ($next_question == 2){
    $total = count($q1);
    for($i = 0; $i < $total; $i++){
        $response .= "[Question_1: ".$q1[$i]."], ";
        if ($i < ($total - 1))
            $response .= "&";
    }
    $msg .= "<h4>Answers to Question One:</h4>";
    if ($q1[0] == "quotes" && $q1[1] == "like") {
        $msg .= "<b>A:</b> Good choice! You remembered that quotes around a term ensure that it will be searched as a phrase, 
								not as separate words.<br> 
								<b>D:</b> Yes, nouns like <b>controversy</b>, <b>debate</b>, or <b>issue</b> will help you find material covering 
								two sides of an issue.<br>";
        $correct = 2;
    }
				else if($q1[0] == "quotes" && $q1[1] == "search") {
        $msg .= "<b>A:</b> Good choice! You remembered that quotes around a term ensure that it will be searched as a phrase, 
								not as separate words.<br> 
								<b>B:</b> No, For a more effective search, use quotes around the term <b>\"gun control\"</b> to ensure
	       that it will be searched as a phrase, not as separate words.";								
								$correct = 1;
    } 
				else if($q1[0] == "quotes" && $q1[1] == "additional") {
        $msg .= "<b>A:</b> Good choice! You remembered that quotes around a term ensure that it will be searched as a phrase, 
								not as separate words.<br> 
								<b>C:</b> No, Although you may find a few relevant sites using <b>against</b> and <b>for</b> you will get better 
								results using nouns like <b>controversy</b>, <b>debate</b>, or <b>issue</b> when you want material covering 
								two sides of an issue.";
								$correct = 1;
    } 
				else if($q1[0] == "search" && $q1[1] == "like") {
        $msg .= "<b>B:</b> No, for a more effective search, use quotes around the term <b>\"gun control\"</b> to ensure
								that it will be searched as a phrase, not as separate words.<br>
								<b>D:</b> Yes, nouns like <b>controversy</b>, <b>debate</b>, or <b>issue</b> will help you find material covering 
								two sides of an issue.<br>";
								$correct = 1;
    }
				else if($q1[0] == "additional" && $q1[1] == "like") {
        $msg .= "<b>C:</b> No, Although you may find a few relevant sites using <b>against</b> and <b>for</b> you will get better 
								results using nouns like <b>controversy</b>, <b>debate</b>, or <b>issue</b> when you want material covering 
								two sides of an issue.<br>
								<b>D:</b> Yes, nouns like <b>controversy</b>, <b>debate</b>, or <b>issue</b> will help you find material covering 
								two sides of an issue.";
								$correct = 1;
    }
				else {
        $msg .= "<b>B:</b> No, for a more effective search, use quotes around the term <b>\"gun control\"</b> to ensure
								that it will be searched as a phrase, not as separate words.<br>
								<b>C:</b> No, although you may find a few relevant sites using <b>against</b> and <b>for</b> you will get better
								results using nouns like <b>controversy</b>, <b>debate</b>, or <b>issue</b> when you want
								material covering two sides of an issue.<br>";
        $correct = 0;
    }
				$response = $_COOKIE["response"];
    $response .= "[Question_1a: ".$q1[0].", Question_1b: ".$q1[1]."], ";
    setcookie ("response", $response);
    setcookie ("next", "2");
    setcookie ("ans[0]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 2</a></div></p>";
}
?>

<h2>Question One</h2>
<h3> You want to find material about the pros and cons of gun control. Using a <br>
search engine, which of these strategies would be the most effective? (Choose 
two) </h3>
<form method="POST" action="q1.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
    <?php
    if ($q1[0] == "quotes"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"quotes\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"quotes\">";
    endif;
    ?>
    A. Enter a search with quotes around <b>"gun control"</b>.
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q1[0] == "search" || $q1[1] == "search"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"search\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"search\">";
    endif;
    ?>
    B. Enter the search <b>gun control</b>.   
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q1[0] == "additional" || $q1[1] == "additional"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"additional\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"additional\">";
    endif;
    ?>
    C. Use additional words like <b>against</b> and <b>for</b> in your search.
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q1 [0] == "like" || $q1 [1] == "like"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"like\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"like\">";
    endif;
    ?>
    D. Use additional words like <b>controversy</b>, <b>debate</b>, or <b>issue</b> in your search. 
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="2">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
