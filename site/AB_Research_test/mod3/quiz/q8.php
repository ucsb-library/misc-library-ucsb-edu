<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 8; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q8 = $_REQUEST["q8"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q8 == "" && $next_question == 9){
    $err_msg = "Please select a choice";
    $next_question = 8;
}
//Print and record question results
if ($next_question == 9){
    $correct = 0;
    $msg = "<h4>Answer to Question Eight:</h4>\n";
    if ($q8 == "true") {
        $msg .= "<b>A:</b> Yes, because they may give you other terms to use in your search.<br>";
        $correct = 1;
    } else {
    $msg .= "<b>B:</b> Wrong. Subject terms <u>can</u> help to focus your search.<br>";
}
    $response = $_COOKIE["response"];
    $response .= "Question_8: ".$q8."], ";
    setcookie ("response", $response);    
    setcookie ("next", "9");
    setcookie ("ans[7]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 9</a></div></p>";
}    
?>
<h2>Question Eight</h2>
<h3> When looking for a book on a topic, a good strategy to use is to first do 
a <br>Keyword search, 
find a book that matches your topic, and then look at <br>subject terms in that catalog entry that you might want to 
use. (Choose one)<br>
</h3>
<form method="POST" action="q8.php">
<div align="CENTER">
  <table border="0" width="60%">
  <tr> 
    <td>A. True
    <?php
    if ($q8 == "true"):
        print "<input type=\"radio\" name=\"q8\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"true\">";
    endif;
    ?>
    </td>
  </tr>
  <tr> 
    <td>B. False    
    <?php
    if ($q8 == "false"):
        print "<input type=\"radio\" name=\"q8\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"false\">";
    endif;
    ?>
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="9">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
