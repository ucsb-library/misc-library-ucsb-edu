<?php
ob_start();
require_once "../../functions.php";
$question = 3;
$next_question = $_REQUEST["next_question"];
$a = $_REQUEST["a"];
$b = $_REQUEST["b"];
$c = $_REQUEST["c"];
//Check that question hasn't been answered
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($a == "" && $next_question == 4){
    $err_msg = "Please select an answer for A";
    $next_question = 3;
} else if ($b == "" && $next_question == 4){
    $err_msg = "Please select an answer for B";
    $next_question = 3;
} else if ($c == "" && $next_question == 4){
    $err_msg = "Please select an answer for C";
    $next_question = 3;
}
//Print and record question results
if ($next_question == 4){
    $correct = 0;
    $msg = "<h4>Answers to Question Three:</h4>";
    $msg .= "<b>A: </b>";
    if ($a == "main"){
        $msg .= "Right.<br>";
								$correct++;
    } else {
        $msg .="No, Check the <b>Location</b> line.<br>";
        
    }
    $msg .= "<b>B: </b>";
    if ($b == "music") {
        $msg .= "Correct.<br>";
        $correct++;
    } else {   
        $msg .= "No, Check the <b>Subject</b> line.<br>";
    } 
    $msg .= "<b>C: </b>";
    if ($c == "ansc") {
        $msg .= "Correct.<br>";
        $correct++;
    } else {
        $msg .= "No, Check the <b>Publisher</b> line.<br>";
    }
    $response = $_COOKIE["response"];
    $response .= "[Question_3a: ".$a.", Question_3b: ".$b.", Question_3c: ".$c."], ";
    setcookie ("response", $response);
    setcookie ("next", "4");
    setcookie ("ans[2]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 4</a></div></p>";
}
require_once "../../quiz_header.php";?>
<h2>Question Three:</h2>

<form method="POST" action="q3.php">
<table border="0" width="738" cellspacing="0" cellpadding="0">
<tr> 
  <td valign="top" colspan="3" rowspan="5"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr> 
 						<td width="356" height="258" valign="top">
							<img src="../images/book-record.gif" width="400" height="271" alt="book record"></td>
      </tr>
    </table>
	  <td width="63" height="87"> 
  <td width="19" valign="TOP" align="LEFT"> 
  <div align="LEFT"> A. </div>
	  <td valign="top" width="229" align="LEFT"> This book is located in the<br>
		<?php
  		if ($a == "education"):
  		print "<input type=\"radio\" name=\"a\" value=\"education\" CHECKED>";
  		else: 
   	print "<input type=\"radio\" name=\"a\" value=\"education\">";
   	endif;
  ?>
  Education Library
		<br>
		<?php
  		if ($a == "main"):
  		print "<input type=\"radio\" name=\"a\" value=\"main\" CHECKED>";
  		else: 
   	print "<input type=\"radio\" name=\"a\" value=\"main\">";
   	endif;
  ?>
  Main Library (Davidson)
		<br>
		<?php
  		if ($a == "music"):
  		print "<input type=\"radio\" name=\"a\" value=\"music\" CHECKED>";
  		else: 
   	print "<input type=\"radio\" name=\"a\" value=\"music\">";
   	endif;
  ?>
  Music Library
	  <td width="18"></td>
  <td width="9"></td>
  </tr>
		
		<tr> 
  <td height="9">
  <td>
  <td>
  <td></td>
  <td></td>
  </tr>
		
		<tr> 
  <td height="88">
  <td valign="TOP"> 
  <div align="LEFT"> B. </div>
		<td valign="top"> This book is about:<br>
		<?php
  		if ($b == "presidents"):
  		print "<input type=\"radio\" name=\"b\" value=\"presidents\" CHECKED>";
  		else: 
   	print "<input type=\"radio\" name=\"b\" value=\"presidents\">";
   	endif;
  ?>
  Presidents
		<br>
		<?php
  		if ($b == "music"):
  		print "<input type=\"radio\" name=\"b\" value=\"music\" CHECKED>";
  		else: 
   	print "<input type=\"radio\" name=\"b\" value=\"music\">";
   	endif;
  ?>
  Music
		<br>
		<?php
  		if ($b == "engg"):
  		print "<input type=\"radio\" name=\"b\" value=\"engg\" CHECKED>";
  		else: 
   	print "<input type=\"radio\" name=\"b\" value=\"engg\">";
   	endif;
  ?>
  Engineering
		<br>
		<td></td>
  <td></td>
  </tr>
  <tr> 
  <td height="15">
  <td>
  <td>
  <td></td>
  <td></td>
  </tr>
  <tr> 
  <td height="72">
  <td rowspan="2" valign="TOP"> 
  <div align="LEFT"> C. </div>
	  <td colspan="2" rowspan="2" valign="top"> The year that this book was published is:<br>
		<?php
  	if ($c == "ansa"):
  	print "<input type=\"radio\" name=\"c\" value=\"ansa\" CHECKED>";
  	else: 
  	print "<input type=\"radio\" name=\"c\" value=\"ansa\">";
  	endif;
  ?>
  1995
		<br>
		<?php
  	if ($c == "ansb"):
  	print "<input type=\"radio\" name=\"c\" value=\"ansb\" CHECKED>";
  	else: 
  	print "<input type=\"radio\" name=\"c\" value=\"ansb\">";
  	endif;
  ?>
  1997
		<br>
<?php
  	if ($c == "ansc"):
  	print "<input type=\"radio\" name=\"c\" value=\"ansc\" CHECKED>";
  	else: 
  	print "<input type=\"radio\" name=\"c\" value=\"ansc\">";
  	endif;
  ?>
  1998<br>
	  <td>
  </tr>
  <tr> 
  <td height="15" width="182"> 
  <td rowspan="2" width="61" valign="top"> 
		<p>
		<input type="hidden" name ="next_question" value="4">
  <?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
  ?>
		</p>
	  <td width="157">
  <td> 
  <td>
  </tr>
  <tr> 
  <td height="18">
  <td>
  <td>
  <td>
  <td>
  <td>
  <td>
  </tr>
  </table>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>
