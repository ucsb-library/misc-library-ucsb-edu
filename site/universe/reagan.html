<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<html><head><title>An Accent on Access</title></head>
<body bgcolor="ffffff" text="000000"
link="0000ff" alink="b03060" vlink="d02090">
<img src="saturn.gif" alt="The Universe at Your Fingertips">

<center> <table width="75%" border=1 cellpadding=3><tr><td> URLs in this  
document have been updated.  Links enclosed in <strong>{curly
brackets}</strong> have been changed.  If a replacement link was located,
the new URL was added and the link is active; if a new site could not be
identified, the broken link was removed.</td></tr></table></center><p>


<hr>
<center>
Copyright 1997, Michael J. Reagan. Used with permission.
</center>
<hr>
<h1 align=center>An Accent on Access: Writing HTML for the Widest
Possible Audience</h1>

<center>
<strong><a href="mailto:mreagan@csun.edu">Michael J. Reagan</a></strong><br>
Circulation Unit Coordinator, Database Coordinator II<br>
California State University, Northridge<p>
</center>
<hr>
<h2>Abstract.</h2>
Web authors are often tempted to use the latest and sexiest means to
present their information.  "Hot" and "Cool" sites use dancing
graphics, frames, tables, specific fonts, background and foreground
colors to entice the reader and delight the eye.  Sound clips often
convey emotional content that cannot be expressed in text.  Digital
video clips are jerky but they do move.<p>

This tends to disenfranchise some users, however.  When we put up web
pages to provide information to our clientele, we need to remember
their limitations, and make our information accessible.  Deaf readers
need text support for sound clips, as well as visual clues to any
audio stimuli, including beeps and bells.  Blind readers need to be
able to access the information content through text presented in a
linear manner, so that it can be rendered as sound by their
specialized equipment.  Readers at the end of a telephone line need
access to the information content even when they turn off display of
inline images, and readers with older computers need pages that work with a
text browser, such as Lynx.<p>

The presentation will demonstrate HTML coding techniques to enhance
accessibility without totally forsaking attractiveness.  The
potential audience will be web authors who can understand HTML tagging
without lengthy explanations.

<h2>Audience.</h2>

Writing HTML for accessibility differs according to the needs of the
audience.  While many of my suggestions are applicable across the board,
there are some considerations that apply particularly to specific groups
of users.<p>

<h3>Blind readers.</h3>

The usual coping mechanism for blind readers of computer screens is to use
a screen reader program to render the text of the screen as speech with a
speech synthesizer or in Braille with a refreshable Braille display. (<a
href="#paciello">Paciello</a>) The speech synthesizer may be standalone
hardware, such as DecTalk, or software used in connection with a standard
sound card, such as TextAssist, which comes bundled with SoundBlaster
cards from Creative Labs. <p>

Screen readers do a pretty amazing job of translating the characters on a
screen, reading from left to right and top to bottom, into phonemes which
are spoken by the hardware.  Most screen readers take total control of the
hardware, however, and override the possibility of playing embedded sound
files through the sound card. A specialized web page reader, {<a
href="http://www.prodworks.com/">pwWebSpeak</a>}, overcomes this
limitation. Pages designed to be taken in at a glance, using colors and
type sizes to draw the eye to particular parts, take on a whole new "look"
when read aloud serially. <p>

<h3>Deaf readers.</h3>

Web pages are mostly accessible to deaf readers. Audio clips are an
obviously inaccessible element. Some of the sound cues provided by web
pages and web browsers should be reinforced with visual cues. Windows 95
has accessibility options, including SoundSentry and ShowSounds to
accommodate deaf users. SoundSentry is available for Windows 3.1 as part
of the Access Pack at
{http://www.windows.com/windows/enable/accessw.htm} by
mail or FTP. <p>

Many hearing readers have non-speaking computers (no sound cards). Both
deaf readers and these impoverished souls (including most users of Library
computers) are helped by web authors who include a text alternative, or at
least a description, for each audio clip. <p>

<h3>Poor readers.</h3>

Not everyone can have the latest Intel and Microsoft wallet-shrinking
toys. Many of our readers make do with slow modems, and turn off automatic
loading of images. Freenets and low-end Internet service providers supply
UNIX shell access to the WWW with Lynx text-only browsers. Most third
world access to the Web is through such narrow pipes. <p>

Webmasters who count access by browser type cite low percentages of Lynx
users as justification for using the more resource intensive HTML
features. This smacks of the self-fulfilling prophecy; who would visit <a
href="http://www.sears.com/">www.sears.com</a> a second time with Lynx or
with images turned off?  In a recent exchange on the web-consultants
discussion list, Gregory J. Rosmaita, under the pseudonym Oedipus wrecked,
suggests that the number of non-graphic browser users is understated: <p>

<blockquote>
even a cursory glance at the lynx-dev mailing list's hypertext archives
would reveal that, currently, there are Japanese, Chinese, Egyptian,
Turkish, Brazilian, American, Canadian, German, Swedish, Ukranian,
Russian, and a host of other programmers working on national charsets,
libraries, and patches for lynx...  if lynx's user base was as small as
is claimed on this list, it would have breathed its last with the release
of 2.4.2    (<a href="#gill">Gill</a>, The Web: Design, ADA and Lynx)
</blockquote>

<h2>Practicalities.</h2>

This section will present specific suggestions for improving HTML with an
eye to accessibility by all the audiences explored above. <P>

<h3>Compatibility.</h3>

What works in Lynx will work in all other browsers; the converse is not
true. Designing for Lynx requires clean HTML 2.0 code; providing
alternatives for blind and deaf users also accommodates readers without
disabilities who have diverse learning styles. For example, a .jpg image
of a medieval manuscript is visually impressive, but inaccessible to a
blind reader.  Provision of a transcribed text not only allows the blind
reader access to the content on one level but also provides the sighted
reader with a key to deciphering the text, which is likely to be full of
medieval abbreviations. <p>

Many Web authors do not know the differences among the HTML standards, and
may not realize how badly the pages they write with WYSIWYG editors may
present themselves in diverse browsers. Gerald Oskoboiny does us all a
great service with his "Kinder, Gentler Validator" at 
{http://ugweb.cs.ualberta.ca/~gerald/validate/}
where he offers HTML grammar checking and Weblint style analysis. If you
submit a page without a &lt;!DOCTYPE&gt; declaration it will be parsed at
HTML 2.0 level and the information that Gerald makes available will teach
you much about the various flavors of HTML. <p>

Oskoboiny also provides a way to see approximately how your pages would
appear in Lynx without launching a Lynx browser. <strong><a
href="http://cgi.w3.org/cgi-bin/html2txt?">http://cgi.w3.org/cgi-bin/html2txt?</a></strong>
is the address; this is actually a script that requires a URL as an
argument, but try the address given to get further instructions from
Gerald directly. <p>

<h3>Images.</h3>

No &lt;img&gt; tag should be published to the world without the alt=" "
parameter. (<a href="#fontaine">Fontaine</a>) For images that contribute
content to the document, provide a concise description, preferably within
square brackets, since Lynx users associate square brackets with images,
usually in the useless form [image] or [inline]. For example &lt;img
src="paul.jpg" alt="[Paul]" width=44 height=60&gt; would alert the user that
there is a picture of Paul available. Inclusion of the width and height
attributes does nothing for the Lynx user, but does save the time of the
graphic browser user, since the browser can reserve a space 44 by 60
pixels and continue to display text without having to stop and retrieve
the image from the server to take its measurements. <p>

Text expressed not in ASCII characters but as part of an image, often in a
banner or letterhead image, must be expressed in alt="the words in the
image" to avoid losing content without the image. <p>

Larger images should be presented in thumbnail versions, with a link to
their larger selves to be loaded on demand. The link text would include
the thumbnail image itself and an indication of its approximate size in
kilobytes, as in this example: &lt;a href="asiamin.gif"&gt;&lt;img
src="asiamic.gif" width=44 height=26 alt="[Map: Asia Minor]"&gt;
47K&lt;/a&gt; The full-size image will be loaded as a separate page if the
link is selected by the user. Since it stands alone, no provision for
including its width and height is needed. <p>

An image editor, such a LviewPro or Photoshop, can easily create smaller
versions of .gif or .jpg images for use as thumbnails. It is also possible
to use the same file for both, with the width and height attributes used
to tell the browser to reduce the image size on the fly, but this means
asking the user to do with each load work that can be done once only. This
is a blatant violation of Ranganathan's fourth law of library science,
"Save the time of the Reader." <p>

Images that are purely decorative, contributing to the look of the graphic
page but useless to the non-graphic browser user, can be hidden from the
latter's sight or hearing by using the alt= parameter to make the
alternative text a null or empty string:  &lt;img src="wavyline.gif"
width=480 height=5 alt=""&gt; Note that there is absolutely nothing
between the quotes. <p>

<h3>Image Maps.</h3>

Graphic designers love them, HTML writers are pleased as punch to make
them work at last, but ISMAP images are absolutely useless to readers
without graphics.  For these users we need to provide either a link to a
text-based alternate page or a set of text links in the page with the map
itself. My preference is for the latter approach, with the image map
carrying a null string as alternative text, lest a user select it as a
link without any coordinates. (<a href="#fontaine">Fontaine</a>) <p>

<h3>Buttons.</h3>

Some page designers seem to resent the conventional look of link text in
bright blue with underlining, preferring to use buttons as link images.
They often add the border=0 parameter to the &lt;img&gt; tag to prevent
the blue border that indicates a link. When they provide alternate text
for the buttons it is likely to be alt="*" which creates a bit of a
mystery for the Lynx user reading the page with a screen reader and a
voice synthesizer. These pages can also be mysterious to sighted users
with graphic browsers, unless they are savvy enough to notice that the
cursor changes shape when poised over a link image. <p>

My preference is to use link text in association with images that suggest
the target, if appropriate. I welcome the blue border and the bright blue
underlined text convention that gives the user belt-and-suspenders
assurance that this is indeed a link. <p>

<h3>"Click here".</h3>

One way to avoid the "click here" syndrome, is to test pages in Lynx,
which does not click at all. Link text should express concisely what the
user will reach by selecting the link. A list of link text items should
sound like an invitation to content, not a litany of "here, here, this,
here, this, here" and "click me." (<a href="#fontaine">Fontaine</a>) <p>

<h3>Audio.</h3>

Links to audio clips should be accompanied by alternative links to text of
an announcement, lyrics of a song, or a description of the sounds that
would be heard if the user could hear them. For example, it would be
delightful if The Capitol Steps (<a
href="http://www.capsteps.com/">http://www.capsteps.com/</a>) would
provide a link to the script for <cite>Lirty Dies: Graberdeen Grooving
Pounds</cite> as well as links to the .au and RealAudio formats of the
monologue. <p>

Linking to a site that automatically starts playing a tune can be quite
disconcerting on a browser that cannot play the tune; an error message is
likely to pop up in the user's face. One such is the satiric
{http://www.highersource.org/} site (not to be confused
with the .com site), which plays the theme from M*A*S*H ("Suicide is
Painless") as background.  <p>

<h3>Other formats.</h3>

Just as it is helpful to provide a text alternative to an audio clip,
providing either a text or an HTML alternative to a .pdf file is a
lifesaver to readers who cannot see or display these Adobe Acrobat pages.
(<a href="#fontaine">Fontaine</a>)<p>

Not all browsers can handle forms. If you are really concerned about
giving the reader a means to provide information to you or your
organization, provide alternatives.  Many sites give users a choice
among: <p>

<ul>

<li>an online form;
<li>a text form that can be downloaded, filled in, and returned by e-mail
or snail mail; or
<li>a telephone number to call.
</ul><p>

While it is beyond the scope of this paper, when possible moving pictures
may be made accessible by adding captions in text or in ASL and by
providing a descriptive soundtrack for blind users. If such options are
available, they should probably be provided in side-by-side alternative
links, rather than included in the only copy of the movie provided.
(<a href="#vanderheiden">Vanderheiden</a>)

<h3>Lists.</h3>


Lists are pretty straightforward HTML elements, but there are a couple of
hints that will improve accessibility of lists: <p>

<ul>

<li>if using graphics as bullets, provide alt="*" or alt="o" parameters in
the &lt;img&gt; tag.
<li>include closing punctuation on list items, even if the items are not
grammatical sentences, for the benefit of users with screen readers
rendering either speech or Braille.<p>
</ul>

Closing punctuation (full stop or period) is nearly invisible in lists,
but looks a little silly on headings, but it makes a big difference in how
the headings sound with a screen reader.  Did you really notice the
periods in the headings in this page? <p>

I also added a &lt;br&gt; tag at the end of almost every URL that was
expressed as such in the text. This really helps clarify to the hearer
that the end of the link has been reached, especially when the speech
synthesizer tries to interpret and pronounce the "words" in the URL; for
example, the synthesizer reads the "ca" in ugweb.cs.ualberta.ca as
"California."

<h3>Tables.</h3>

From an accessibility standpoint, concern for users with screen readers
suggests a "just say no" policy.  Screen readers read the screen line by
line, an approach which creates chaos when it encounters even a
relatively simple table or columnar arrangement of data.  Try reading the
following table in a line-by-line fashion:<p>

<table border=1>
<tr><th>Table type	<th>Results in Lynx	<th>Screen reader results
<tr><td><pre>preformatted text in
columns  and
rows</pre>

<td>spacing of the original text is preserved in <strong>fixed
spacing</strong> typeface
<td>line-by-line reading does violence to the logic of the text arrangement

<tr><td>&lt;table&gt;standard table construction&lt;/table&gt;
<td>all table-related tags are ignored, text is presented in a blob, line
by line ignoring line ends
<td>same as above

<tr><td>standard table construction with &lt;br&gt; tags at end of each
cell
<td>table-related tags are ignored, but &lt;br&gt; tags cause new line
starts; significance of headings is lost
<td>if screen reader is reading Lynx  screen, new line indicates new
topic, but logic of columns may not be obvious
</table>

An appropriate use of preformatted text may be seen in my personal home page at {http://library.csun.edu/reagan/} where I include the .sig block that I use in electronic mail as a concise way to display address and telephone numbers.  It is a two-column
presentation that does not lose too much when read line by line: 
<pre>

Michael Reagan   KK6WO                  Circulation Unit Coordinator
mreagan@csun.edu                        University Library
(818) 677-4391    fax (818)677-4136     California State University
home (818) 449-0996 fax 449-0952        Northridge CA 91330-8327
pager (818) 828-1226                    packet KK6WO@W6VIO.#SOCA.CA.USA.NA
--... ...-- -.. . de gustibus non est disputandum -.- -.- -.... .-- ---
</pre>

The roster of the CSU, Northridge Faculty Executive Committee is an
example of the use of table logic with &lt;br&gt; tags to make it work in
Lynx.  Its URL is {http://www.csun.edu/~fs20469/execom.html}
and it has an extra &lt;br&gt; tag in the right-hand cells to create
vertical white space between entries when displayed in Lynx. <p>

<h3>Frames.</h3>

Like many of the participants in the web4lib discussion list, I have seen
few examples of the use of frames that justify the grief this feature
causes to the disenfranchised user.  Lynx cannot handle frames, screen
readers balk at them, older browsers stop in their tracks.<p>

If you do feel a need to use frames logic, create the page without it as
well, and include a link at the top of the frames page to let your
readers get to the non-frames version.  Do this for every page on which
you use frames.  If you are within the range of normally lazy humanity,
you will stop putting yourself through the agony of writing the frames
version.<p>

If your WYSIWYG HTML tool insists on writing with frames, throw it out.<p>

<h3>Applets.</h3>

The arguments above about frames apply in spades to Java applets.  They
are potentially quite powerful, but address only the "rich, well-born,
and able" browsers running in 32-bit operating systems.  The World-Wide
Web is still predominantly a 16-bit world, with huge installed bases of
Windows 3.1 machines.  Java makes perfect sense if you are selling a
product to Sharper Image customers, or if you are addressing an Intranet
running only Windows 95 or Windows NT or PowerPC Macintosh machines.<p>

Microsoft ActiveX has most of the same disadvantages as Java plus a
reputation for creating security problems that has many users refusing to
allow it.<p>

<h3>Limited video.</h3>

Be kind to your less well-endowed readers when including images in web
pages. Some of us have video cards with extremely limited memory, so the
precise shade of off-mauve that best sets off your page featuring a
true-color photograph of your award-winning black and white Dalmatian is
likely to show up as a deadly drab dithered dot design. Limiting
backgrounds to standard colors, and images to a small palette will produce
more uniform results across the user base. <p>

My graphic editor is the freeware LviewPro. When I open an image file the upper border displays the file name and its three dimensions, width in pixels, height in pixels, and number of colors in the palette used. It is amazing how much difference can be made in the size of the image file by reducing the number of colors used. A scanned image of a postcard printed on a wood-grain background was 62,151 bytes in .jpg format. As seen in an experiment with LviewPro at {http://library.csun.edu/reagan/images.html} the size was reduced to 30,569 bytes by limiting the number of colors, and further reduced to 18,345 bytes by saving it in .gif format. Choosing a reduced file size, if the image quality is still adequate, "Saves the time of the Reader." <p>

Smaller image sizes help keep the total page size within reason.  Pages
larger than 50K to 75K total should be scrutinized carefully.  Many
readers will hit the stop button rather than wait to see large pages.
(<a href="#gill">Gill</a>, Issues)<p>

<h3>&lt;Blink&gt;.</h3>

<strong>Don't.</strong><p>

Many users are annoyed by blinking text and animated .gif images, but a
few could be injured by them.  Some epileptic seizures are triggered by
pulsing light.  Blinking text is pulsing light.  Need I complete the
syllogism?<p>

Screen readers used to translate text into speech or Braille are usually
stymied by blinking text, making it impossible for the user to read the
rest of the screen.<p>

<strong>Just don't.</strong><p>

<h3>Futures.</h3>

A simple change of habit will help avoid a "Y2K" problem on web pages.
What date is 01/02/03 on the World-Wide Web?  Taking the time to write
January 2, 2003, 1 February 2003, or 3 February 2001 will avoid
confusion.  (<a href="#starling">Starling</a>)<p>

Screen readers of the future, optimized for HTML, are likely to do a
better job of expressing &lt;strong&gt;, &lt;em&gt;, &lt;cite&gt;, or
&lt;var&gt; than &lt;b&gt; or &lt;i&gt;.  (<a href="#do-it">DO-IT</a>)<p>

<h2>A Homily.</h2>

As if I haven't been preaching throughout this paper <p>

<h3>Appearances.</h3>

Much effort is successfully employed in creating printed advertising with
the most appropriate type faces in exactly the right sizes, custom artwork
rendered just so, perfectly placed illustrative material, and all the
tools of subliminal seduction marshaled to engage the intended audience.
The obvious potential of the World-Wide Web has tempted many to bring
their advertising toolbox to this new medium. Some Web authors have gone
to great lengths in their attempts to control presentation, even including
tailored quantities of invisible single-pixel .gif images to force the
spacing of text on a web page. <p>

<blockquote> Yet, HTML is a Markup Language. It is designed to separate
content from specific presentation of information. It expects the designer
to indicate sentences, paragraphs, etc. in a simple way to format clear
text. Yet many page designers, in an effort to create a "magazine on the
screen,"  have resorted to clever techniques to extend the range of HTML.
They endeavor to force the browser to "display" that information in a
specific way--a method which may exactly match the format which the
designer might pick if he/she had complete control.  (<a
href="#murphy">Murphy</a>) </blockquote>

This effort is doomed to failure, because the balance of power in the Web
is shifted away from the page producer.  The look and feel of a web page
is determined by three elements:

<ol>
<li>The internal logic of the browser client software has primary
control.  Pages will look different on PCs and on Macintoshes.  Netscape,
Mosaic, and Internet Explorer all have different approaches to display,
both among themselves and between their own versions.  Lynx obviously
does things differently.

<li>The browser options selected by the user are secondary but powerful.
Whether as a matter of taste or as a matter of necessity, users are
likely to choose different base typefaces and font sizes, monitor
resolutions, and background colors, and make such basic decisions as
whether to allow loading of images or to override page preferences with
their own.

<li>The choices coded by the Web author are tertiary.
</ol>

Since control of the output is futile, you can afford to make your pages
as widely accessible as possible and embrace the

<h3>KISS principle (keep it simple, stupid).</h3>

<ul>
<li>Focus on content.
<li>Use tagging that makes the content clear to the widest possible audience.
<li>Embrace the conventional - bright blue underlined text is unmistakably
a link.
<li>Use "sexy" features, bells and whistles only if delivery of the content
requires them and limited accessibility is acceptable.
</ul>

<h3>Allow exceptions.</h3>

Not every page can be usable in Lynx.  The images.html page referenced
above is readable in Lynx, but the essential information simply cannot be
conveyed without graphic capabilities.  A page on leitmotifs in Mozart is
likely to require audio clips and .pdf scores.  The reference desk
schedule at CSU, Northridge is much clearer and easier to maintain in
&lt;table&gt; format, and its limited audience is all adequately equipped to
handle tables.  Life writing web pages with an accent to access need not
be drab.<P>


<h2>References.</h2>

<strong>Burstein, Carl D.</strong> 1997. "Best Viewed With Any
Browser." 
[{http://server.berkeley.edu/~cdaveb/anybrowser.html}]<br>
Argues for the use of standard HTML according to the standard
specifications, to promote compatibility among graphic browsers and with
text browsers. <p>

<strong>Dardailler, Daniel, Paciello, Michael G.</strong> 1997. "W3C
Disabilities Developments." [{<a
href="http://www.w3.org/WAI/References/">http://www.w3.org/WAI/References/</a>}]<br>
Includes links to most of the helpful sites dealing with accessible HTML.<p>

<strong>Design Visualization Laboratory at the University of Illinois at
Chicago.</strong> 1997. "NCSA Mosaic Access Page.".
[{http://bucky.aa.uic.edu/}]<p>

<strong><a name="do-it">DO-IT Disabilities, Opportunities, Internetworking
& Technology.</a></strong> 1997. "DO-IT HTML Guidelines." [{<a
href="http://www.washington.edu/doit/">http://www.washington.edu/doit/</a>}]
<p>

<strong>Flavell, A. J.</strong> 1997. "Imagemaps - Text-friendly? Cache-friendly?" [{http://ppewww.ph.gla.ac.uk/%7Eflavell/alt/imgmap.html}]<p>

<strong><a name="fontaine">Fontaine, Paul.</a></strong> 1997. "Writing Accessible HTML Documents." [{http://www.itpolicy.gsa.gov/cita/wwwcode.htm}] <br> 
Addresses many of the practicalities in this paper with clear rationales, strategies, and examples of both inaccessible code and accessible alternatives. <p>

<strong><a name="gill">Gill, Kathy E.</a></strong> "Issues of Web Design:
Multiple Browser Compatibility." 
[{http://www.enetdigest.com/design/design.html}]<br> An
introduction with selected links. <p>

<strong>Gill, Kathy E.</strong> 1997. "The Web: Design, ADA and Lynx."
[{http://www.enetdigest.com/design/text_ada.html}]<br>
An exchange of messages on the web-consultants distribution list. <p>

<strong>Gunderson, Jon.</strong> 1997. "World Wide Web Accessibility to People with Disabilities A Usability Perspective." [{http://www.staff.uiuc.edu/~jongund/access-overview.html}]<br>
Speaks of features that should be added to browsers, HTML editors, and information formats (e.g. movies) to enhance accessibility.<p>

<strong>MacCandlish, Stanton.</strong> 1997. "Designing HTML Tables to Work with HTML 2.0 Browsers." [{http://www.eff.org/~mech/Scritti/html_table_design.html}]<br> 
Demonstrates "tricks" to make tables look acceptable in a non-tables browser.<p>

<strong><a name="murphy">Murphy, Arthur R.</a></strong> 1997. "Design Considerations: Readers with Visual Impairments." {http://www.lcc.gatech.edu/gallery/dzine/access/}]<br>
Urges use of Lynx as a measure of accessibility.<p>

<strong><a name="paciello">Paciello, Mike.</a></strong> 1997. "Making the
Web Accessible for the Blind and Visually Impaired."
[{http://www.yuri.org/webable/mp-blnax.html}]<br>
Describes access considerations for "print-impaired" users. <p>

<strong>Yuri Rubinsky Insight Foundation.</strong> 1997. "WEBable."
[{http://www.yuri.org/webable/}]<br>
Another good source for web sites on access. <p>

<strong><a name="starling">Starling Access Services.</a></strong> 1997.
"Accessible Web Page Design." 
[{http://www.igs.net/~starling/acc/}]<p>

<strong><a name="vanderheiden">Vanderheiden, Gregg C.</a></strong> 1997.
"Design of HTML pages to increase
their accessibility to users with disabilities: strategies for today and
tomorrow, version 6.0." 
[{http://www.yuri.org/webable/htmlglds.html}]<br>
Mixes practical suggestions for today with wishes and hopes for the
future. <p>

<p>
<hr>

<P> <A HREF="http://validator.w3.org/"><IMG SRC="http://www.library.ucsb.edu/gallery/valid_html3.2.gif" ALT="HTML 3.2 Checked!" border=0></A>
</body>
</html>
