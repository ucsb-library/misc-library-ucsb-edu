<?php
ob_start();
require_once "../../quiz_header.php";
$question = 6;
$next_question = $_REQUEST["next_question"];
$a = $_REQUEST["a"];
$b = $_REQUEST["b"];
$c = $_REQUEST["c"];
//Check that question hasn't been answered

$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($a == "" && $next_question == 7){
    $err_msg = "Please select an answer for A";
    $next_question = 6;
} else if ($b == "" && $next_question == 7){
    $err_msg = "Please select an answer for B";
    $next_question = 6;
} else if ($c == "" && $next_question == 7){
    $err_msg = "Please select an answer for C";
    $next_question = 6;
}
//Print and record question results
if ($next_question == 7){
    $correct = 0;
    $msg = "<h4>Answers to Question Six:</h4>";
    $msg .= "<b>A: </b>";
    if ($a == "popular"){
        $msg .="Well, no. Popular sources are from magazines. Notice this source actually has the
								word journal in it, which is a good clue that it is a scholarly source. Article titles from popular
								sources may use slang or colloquial terms, also, popular articles tend to be rather short.
							 This journal article is 27 pages long.<br>";
    } else {
        $msg .= "You're right. The title of the source, topic of the article and the length of the article
								were good hints that this was a scholarly source.<br>";
								$correct++;
    }
    $msg .= "<b>B: </b>";
    if ($b == "scholarly") {
        $msg .= "This one might have been a little tricky. Did you notice that the title of the article was
								fairly informal or the article was only three pages long? These are good clues
								that this article was not an in-depth research article from a scholarly journal.<br>";
        
    } else {   
        $msg .= "You got it right, but were you just guessing? Or did you notice that the title was of 
        popular interest and the article was fairly short.<br>";
								$correct++;
    } 
    $msg .= "<b>C: </b>";
    if ($c == "scholarly") {
        $msg .= "Yes, correct. The Historian is a scholarly journal.<br>";
        $correct++;
    } else {
        $msg .= "Keep in mind that not all scholarly publications have the word <i>journal</i> in the title. 
        This article is, in fact, a scholarly article for students and professionals in the field of history.<br>";
    }
    $response = $_COOKIE["response"];
    $response .= "[Question_6a: ".$a.", Question_6b: ".$b.", Question_6c: ".$c."], ";
    setcookie ("response", $response);
    setcookie ("next", "7");
    setcookie ("ans[5]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 7</a></div></p>";
}
require_once "../../quiz_header.php";?>
<h2>Question Six</h2>
<h3>
Decide whether each citation is from a popular or scholarly source. 
(Choose one answer for each section)
</h3>
<form method="POST" action="q6.php">
<div align="CENTER">
  <table border="0" width="75%" cellspacing="4" cellpadding="4">
  <tr> 
    <td colspan="2">
				<strong>A. Harrison, Kristen and Joanne Cantor.&quot;The Relationship 
    between Media Consumption and Eating Disorders.&quot; <i> Journal of 
    Communication</i>. 47 (Spring 1997) 40-67</strong>
  </tr>
  <tr> 
    <td width="57%"> 
    <div align="CENTER">
    <?php
    if ($a == "scholarly"):
        print "<input type=\"radio\" name=\"a\" value=\"scholarly\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"a\" value=\"scholarly\">";
    endif;
    ?>
    Scholarly source 
    </div>
    <td width="43%"> 
    <?php
    if ($a == "popular"):
        print "<input type=\"radio\" name=\"a\" value=\"popular\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"a\" value=\"popular\">";
    endif;
    ?>
    Popular source</td>
  </tr>
  <tr> 
    <td colspan="2">
				<strong>B. Jamison, Dirk. &quot;Tao of the Dumpster: My Father's 
    Love Affair with Trash.&quot; <i>Utne Reader</i>, November 1996, 76-79</strong> 
  </tr>
  <tr> 
    <td width="57%"> 
    <div align="CENTER">
    <?php
    if ($b == "scholarly"):
        print "<input type=\"radio\" name=\"b\" value=\"scholarly\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"b\" value=\"scholarly\">";
    endif;
    ?>
    Scholarly source 
    </div>
    <td width="43%"> 
    <?php
    if ($b == "popular"):
        print "<input type=\"radio\" name=\"b\" value=\"popular\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"b\" value=\"popular\">";
    endif;
    ?>
    Popular source</td>
  </tr>
  <tr> 
    <td colspan="2">
			 <strong>C. Marguilies, Herbert F. &quot;The Moderates in the 
    League of Nations Battle: An Overlooked Faction.&quot; <i>The Historian</i>. 
    60 (Winter 1998) 273-288</strong> 
  </tr>
  <tr> 
    <td width="57%"> 
    <div align="CENTER">
    <?php
    if ($c == "scholarly"):
        print "<input type=\"radio\" name=\"c\" value=\"scholarly\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"c\" value=\"scholarly\">";
    endif;
    ?>
    Scholarly source 
    </div>
    <td width="43%"> 
    <?php
    if ($c == "popular"):
        print "<input type=\"radio\" name=\"c\" value=\"popular\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"c\" value=\"popular\">";
    endif;
    ?>
    Popular source</td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="7">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
