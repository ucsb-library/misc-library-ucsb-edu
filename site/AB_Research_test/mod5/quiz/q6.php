<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 6;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q6 == "" && $next_question == 7){
    $err_msg = "Please select a choice";
    $next_question = 6;
}
//Print and record question results
if ($next_question == 7){
    $correct = 0;
    $msg = "<h4>Answer to Question Six:</h4>\n";
    if ($q6 == "articles") {
        $msg .= "<b>A:</b> No, you won't find many scholarly journal articles on the public Web. 
								Because of intellectual copyright and the need for publishers to 
								make a profit on their publications, this type of material is not 
								freely distributed on the Web. Instead, use one of the article indexes that the 
								library buys to identify articles. (Only the UCSB community can view these.)<br>";
    }
    if ($q6 == "information") {
        $msg .= "<b>B:</b> Yes, many colleges and universities use Web pages to provide information about
								their institutions.<br>";
						  $correct = 1; 
    } 
    if ($q6 == "index") {
        $msg .= "<b>C:</b> No, you won't find such databases on the public Web. 
								Because of intellectual copyright and the need for publishers to make a 
								profit on their publications, you will not find indexes freely 
								available on the Web. Instead, use one of the article indexes purchased by the 
								library. (Only the UCSB community can view these.)<br>";
				}
    if ($q6 == "books") {
        $msg .= "<b>D:</b> No, it would be rare to find the full-text of a book on your topic on the public web, 
								unless it was older and no longer copyrighted. Because of intellectual 
								copyright and the need for publishers to make a profit on their publications, you will 
								not find books freely distributed on the Web. Instead, use Pegasus to find a book on  
								your subject.<br>";
    }
	
    $response = $_COOKIE["response"];
    $response .= "[Question_6:".$q6."], ";
    setcookie ("response", $response);
    setcookie ("next", "7");
    setcookie ("ans[5]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 7</a></div></p>";
}
?>

<h2>Question Six</h2>
<h3>
Which of the following is a good use of the Web? (Choose one)</h3>
<form method="POST" action="q6.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q6 == "articles"):
        print "<input type=\"radio\" name=\"q6\" value=\"articles\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"articles\">";
    endif;
    ?>
    A. To find articles in scholarly journals  
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q6 == "information"):
        print "<input type=\"radio\" name=\"q6\" value=\"information\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"information\">";
    endif;
    ?>
    B. To obtain information about other colleges   
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q6 == "index"):
        print "<input type=\"radio\" name=\"q6\" value=\"index\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"index\">";
    endif;
    ?>
    C. To search databases that index articles in many academic disciplines  
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q6 == "books"):
        print "<input type=\"radio\" name=\"q6\" value=\"books\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"books\">";
    endif;
    ?>
    D. To find books on your topic  
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="7">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
