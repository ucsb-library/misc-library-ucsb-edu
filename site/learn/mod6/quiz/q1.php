<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q1 = $_REQUEST["q1"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q1 == "" && $next_question == 2){
    $err_msg = "Please select a choice";
    $next_question = 1;
}
//Print and record question results
if ($next_question == 2){
    $correct = 0;
    $msg = "<h4>Answer to Question One:</h4>\n";
    if ($q1 == "true") {
        $msg .= "<b>A:</b> Wrong! Even if you paraphrase, you must acknowledge the ideas of others or you are committing plagiarism.<br>";
    } else {
    $msg .= "<b>B:</b> Right! This statement <u>is</u> false. Even if you paraphrase, you must acknowledge the ideas of others or you are committing plagiarism.<br>";
				$correct = 1;
    }
    $response = $_COOKIE["response"];
    $response .= "[Question_1: ".$q1."], ";
    setcookie ("response", $response);    
    setcookie ("next", "2");
    setcookie ("ans[0]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 2</a></div></p>";
}    
?>
<h2>Question One</h2>
<h3> You don't need to credit someone's ideas as long as you change some of their words. (Choose one)</h3>
<form method="POST" action="q1.php">
<div align="CENTER">
  <table border="0" width="60%">
  <tr> 
    <td>
    <?php
    if ($q1 == "true"):
        print "<input type=\"radio\" name=\"q1\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"true\">";
    endif;
    ?>
				A. True
    </td>
  </tr>
  <tr> 
    <td>    
    <?php
    if ($q1 == "false"):
        print "<input type=\"radio\" name=\"q1\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q1\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="2">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
