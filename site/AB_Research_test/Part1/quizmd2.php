<html>
<head>
<title>LAUC Research Grant | Developing a Topic questions</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFCEB" text="000033">
<table width="640" border="0" cellspacing="3" cellpadding="3" align="center">
  <tr valign="top">
    <td>

<p><font face="Verdana, Arial, Helvetica, sans-serif"><font color="1F4366" size="+2">Way
              to go!</font></font></p>
            <p><font face="Verdana, Arial, Helvetica, sans-serif">You've completed this part of the tutorial and you have learned:</font></p>
            <ul>
              <li><font face="Verdana, Arial, Helvetica, sans-serif">how to identify words to describe the different facets of your topic</font></li>
              <li><font face="Verdana, Arial, Helvetica, sans-serif">how to use an asterisk (*) to search for different forms of a word</font></li>
              <li><font face="Verdana, Arial, Helvetica, sans-serif">how to use connectors like AND and OR to do precision searching in a database</font></li>
            </ul>
            <p><font face="Verdana, Arial, Helvetica, sans-serif">Now let's take a quick quiz!</font></p>

      <table width="100%" border="0" cellspacing="3" cellpadding="3" align="center">
        <tr>
          <td width="25%" valign="middle" align="center">
            <p align="CENTER">
            <form method="get" action="quiz.php">
              <div align="CENTER">
                <input type="submit" name="quiz" value="Lets go!">
              </div>
            </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
