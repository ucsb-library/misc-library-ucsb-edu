<?php
ob_start();
$question = 5;
$next_question = $_REQUEST["next_question"];
$q5 = $_REQUEST["q5"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered


if (count($q5) < 2 && $next_question == 6){
    $err_msg = "Please select two choices";
    $next_question = 5;
}
if (count($q5) > 2 && $next_question == 6){
    $err_msg = "Please select only two choices";
    $next_question = 5;
}
//Print and record question results
if ($next_question == 6){
    $total = count($q5);
    for($i = 0; $i < $total; $i++){
        $response .= "q5:".$q5[$i];
        if ($i < ($total - 1))
            $response .= "&";
    }
    $msg .= "<h4>Answers to Question Five:</h4>";
    if ($q5[0] == "public" && $q5[1] == "photographs") {
        $msg .= "<b>A:</b> Yes, Magazine articles are written by journalists for the general public. <br> 
								<b>C:</b> Yes, Magazines have more photos than journals.<br>";
        $correct = 2;
    } 
				else if ($q5[0] == "public" && $q5[1] == "bibliography") {
        $msg .= "<b>A:</b> Yes, Magazine articles are written by journalists for the general public. <br> 
        <b>B:</b> No, articles in magazines are often short and rarely have a bibliography.";
								$correct = 1;
    } 
				else if ($q5[0] == "public" && $q5[1] == "advertisements") {
        $msg .= "<b>A:</b> Yes, Magazine articles are written by journalists for the general public.<br> 
								<b>D:</b> No, Magazines have a lot of ads.";
								$correct = 1;
    }
				else if ($q5[0] == "bibliography" && $q5[1] == "photographs") {
        $msg .= "<b>B:</b> No, articles in magazines are often short and rarely have a bibliography.<br>
								<b>C:</b> Yes, Magazines have more photos than journals.";
								$correct = 1;
    }
				else if ($q5[0] == "photographs" && $q5[1] == "advertisements") {
        $msg .= "<b>C:</b> Yes, Magazines have more photos than journals.<br> 
								<b>D:</b> No, Magazines have a lot of ads.";
								$correct = 1;
    }
				else {
        $msg .= "<b>B:</b> No, articles in magazines are often short and rarely have a bibliography.<br>
        <b>D:</b> No, Magazines have a lot of ads.<br>";
        $correct = 0;
    }
				$response = $_COOKIE["response"];
    $response .= "[Question_5a: ".$q5[0].", Question_5b: ".$q5[1]."], ";
    setcookie ("response", $response);
    setcookie ("next", "6");
    setcookie ("ans[4]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to Question 6</a></div></p>";
}
?>

<h2>Question Five</h2>
<h3>
How can you tell you are looking at a popular magazine? (Choose two)</h3>
<form method="POST" action="q5.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
    <?php
    if ($q5[0] == "public"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"public\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"public\">";
    endif;
    ?>
    A. Articles are written for the general public 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q5[0] == "bibliography" || $q5[1] == "bibliography"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"bibliography\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"bibliography\">";
    endif;
    ?>
    B. Articles are in-depth and often have a bibliography  
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q5[0] == "photographs" || $q5[1] == "photographs"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"photographs\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"photographs\">";
    endif;
    ?>
    C. Issues have lots of photographs  
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q5 [0] == "advertisements" || $q5 [1] == "advertisements"):
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"advertisements\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q5[]\" value=\"advertisements\">";
    endif;
    ?>
    D. Issues have few, if any, advertisements  
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="6">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
