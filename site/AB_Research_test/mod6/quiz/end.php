<?php
foreach($_REQUEST as $a=>$b) {$$a=$b;}
//Get answers array
$answers = $_COOKIE["ans"];
//Expire cookies that may be on system
 setcookie ("ans","");
 setcookie ("next", "");
 setcookie ("response", "");

require_once "../../quiz_header.php";

$question = "Summary";
/* Variables */
$total = 12;  // total number of correct answers
$correct = 0;

$q[0] = 1;	
$q[1] = 5;
$q[2] = 1;	// question two has three correct answer, etc.
$q[3] = 4;
$q[4] = 1;
/* e-mail */
  for ($i = 0; $i < 5; $i ++){
   $correct = $correct + $answers[$i];
   $answers[$i] = percent ($ans[$i], $q[$i]);
  }
  $total_percent = round ($correct/$total * 100);
  $subject = "Searchpath Module 6: Citing Sources Quiz Results";
  $message = "\n\n$subject\n\nQuestion 1:\t$answers[0]\nQuestion 2:\t$answers[1]\nQuestion 3:\t$answers[2]\n";
  $message = $message."Question 4:\t$answers[3]\nQuestion 5:\t$answers[4]\n";
  $message = $message."Total:\t$correct out of $total - $total_percent%\n";
print "<h2><img src=\"../../images/aqua.gif\" width=\"30\" height=\"30\" border=\"0\" alt=\"Module 6\">
Searchpath Quiz Results: Module 6 </h2><HR NOSHADE>";
$today = date("F j, Y, g:i a");
print "<span class='blueText'>$today</span><DIV align='center'><br>";
print formatQuiz($ans, $q);
?>
<style type="text/css">
<!--
.style8 {font-size: 12px; }
.style10 {	font-size: 10px;
	color: #FF0000;
}
-->
</style>
<form method="post" action="../../mail_it.php">
  <input name="formname" type="hidden" id="formname" value="Searchpath Module 6: Citing Sources Quiz Results" />
  <table width="350" border="1" align="center" cellpadding="5" cellspacing="0">
    <tr>
      <td><div align="right" class="style8">
          <input name="message"       type="hidden" value="<? echo "$message"; ?>" />
        Send to (email address): </div></td>
      <td><input name="to" type="text" />
      <br /></td>
    </tr>
    <tr>
      <td><div align="right" class="style8">From (your name): </div></td>
      <td><input name="StudentName" type="text" /></td>
    </tr>
    <tr>
      <td><div align="right" class="style8">From (your email address): </div></td>
      <td><input name="e-mail" type="text" /></td>
    </tr>
    <tr>
      <td colspan="2"><div align="center">
          <input type="submit" name="submit" value="Submit Form" />
      </div></td>
    </tr>
  </table>
</form>

<div align="center">
 

<table width="60%" cellpadding=0 cellspacing=0>

<tr>
<td align="center"> 
<form method="post">
<input type="button" value="Print This Page" onClick="javascript:window.print()">
</form>
</td>
<td align="center"> 
<form method="get" action="../../mod6/quiz">
<input type="submit" value="Retake Quiz">
</form>
</td>
<td align="center"> 
<form method="get" action="../../mod6/09-exit.html">
<input type="submit" value="     Exit     ">
</form>
</td>
</tr>
</table>
</div>

<script language="JavaScript">
<!--
function Validator(send)
{
  var error = ""; 
  if (send.email.value == "")
  {
    error += "Please provide your Instructor's email address.\n";
  }  
  if (send.ssn.value == "")
  {
    error += "Please provide your last 4 digits of SSN.\n";
  }
		if (error != "")
  {
    alert(error);
    return (false);
  } else {
    return (true);
  }  

}
// -->
</script>

<?php
require_once "quiz_footer.php";
?>
