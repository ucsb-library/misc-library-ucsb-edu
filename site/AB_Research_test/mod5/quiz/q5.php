<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 5;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q5 == "" && $next_question == 6){
    $err_msg = "Please select a choice";
    $next_question = 5;
}
//Print and record question results
if ($next_question == 6){
    $correct = 0;
    $msg = "<h4>Answer to Question Five:</h4>\n";
    if ($q5 == "Pegasus") {
        $msg .= "<b>A:</b> No, Pegasus is UCSB's catalog, and it contains a record of everything the library owns. 
								It is <u>not</u> the best place to look for current job openings.<br>";
    }
    if ($q5 == "index") {
        $msg .= "<b>B:</b> No, article indexes contain the records of magazine and journal articles.
								Because the process of writing an article, submitting it for publication, 
								and publishing all takes a certain amount of time, this wouldn't be the best 
								place to look for current job openings.<br>";
						
    } 
    if ($q5 == "web") {
        $msg .= "<b>C:</b> Yes, timeliness is one of the strengths of the Web. Yahoo, for example, has a 
								current careers and job listing feature. It also has local Yahoos! with this feature for cities 
								and for foreign countries.<br>";
								$correct = 1; 
    }
    if ($q5 == "encyclopedia") {
        $msg .= "<b>D:</b> No, an encyclopedia is a good source for background information. It is <u>not</u> the 
								best place to look for current job openings.<br>";
    }
	
    $response = $_COOKIE["response"];
    $response .= "[Question_5:".$q5."], ";
    setcookie ("response", $response);
    setcookie ("next", "6");
    setcookie ("ans[4]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 6</a></div></p>";
}
?>

<h2>Question Five</h2>
<h3>
Which is the best tool for finding current job listings in Malaysia? (Choose one)</h3>
<form method="POST" action="q5.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q5 == "Pegasus"):
        print "<input type=\"radio\" name=\"q5\" value=\"Pegasus\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"Pegasus\">";
    endif;
    ?>
    A. Pegasus 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q5 == "index"):
        print "<input type=\"radio\" name=\"q5\" value=\"index\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"index\">";
    endif;
    ?>
    B. Article index  
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q5 == "web"):
        print "<input type=\"radio\" name=\"q5\" value=\"web\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"web\">";
    endif;
    ?>
    C. Web 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q5 == "encyclopedia"):
        print "<input type=\"radio\" name=\"q5\" value=\"encyclopedia\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"encyclopedia\">";
    endif;
    ?>
    D. Encyclopedia 
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="6">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
