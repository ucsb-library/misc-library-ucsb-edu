<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 10;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q10 == "" && $next_question == 11){
    $err_msg = "Please select a choice";
    $next_question = 10;
}
//Print and record question results
if ($next_question == 11){
    $correct = 0;
    $msg = "<h4>Answer to Question Ten:</h4>\n";
    if ($q10 == "search") {
        $msg .= "<b>A:</b> No, remember you are looking for the statement that NOT true. 
								Using indexes is one of the most efficient ways to search many periodicals
								simultaneously for articles on a subject.<br>";
    }
    if ($q10 == "subject") {
        $msg .= "<b>B:</b> No, remember you are looking for the statement that is NOT true.  
								Indexes usually have a specific focus. For example, ABI Inform concentrates on 
								business-related articles.<br>";
				} 
    if ($q10 == "index") {
    $msg .= "<b>C:</b> You are correct. This statement is <u>not</u> true.<br>
        Periodical indexes provide citations to articles published in many periodicals.  
								Your library might <u>not</u> own all of those periodicals.<br>";
								$correct = 1; 
    }
    if ($q10 == "contain") {
        $msg .= "<b>D:</b> No, remember you are looking for the statement that is NOT true.
        Periodical indexes <u>do</u> provide citations to articles.Some also give you a summary or 
								even the entire text of the article. <br>";
								
    }
	
    $response = $_COOKIE["response"];
    $response .= "[Question_10: ".$q10."]";
    setcookie ("response", $response);
    setcookie ("next", "11");
    setcookie ("ans[9]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to Quiz Results</a></div></p>";
}
?>

<h2>Question Ten</h2>

<h3>Which statement about periodical indexes is <u>not</u> true? (Choose one)</h3>
<form method="POST" action="q10.php">
<div align="CENTER">
<table border="0" width="70%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q10 == "search"):
        print "<input type=\"radio\" name=\"q10\" value=\"search\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q10\" value=\"search\">";
    endif;
    ?>
    A. Periodical indexes allow you to search for articles by subject. 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q10 == "subject"):
        print "<input type=\"radio\" name=\"q10\" value=\"subject\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q10\" value=\"subject\">";
    endif;
    ?>
    B. Periodical indexes often specialize in a subject or type of material. 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q10 == "index"):
        print "<input type=\"radio\" name=\"q10\" value=\"index\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q10\" value=\"index\">";
    endif;
    ?>
    C. Periodical indexes only index items owned by your library. 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q10 == "contain"):
        print "<input type=\"radio\" name=\"q10\" value=\"contain\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q10\" value=\"contain\">";
    endif;
    ?>
    D. Periodical indexes always contain citations and sometimes also include abstracts or full text.
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="11">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
