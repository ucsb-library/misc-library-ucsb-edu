<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1;
//$next_question = $_REQUEST["next_question"];
//$q1 = $_REQUEST["q1"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q1 == "" && $next_question == 2){
    $err_msg = "Please select a choice";
    $next_question = 1;
}

//Print and record question results
if ($next_question == 2){
    $total = count($q1);
    for($i = 0; $i < $total; $i++){
        $response .= "[Question_1: ".$q1[$i]."], ";
        if ($i < ($total - 1))
            $response .= " ";
    }
    setcookie ("next", "2");
    setcookie ("ans[0]", $total);    
    setcookie ("response", $response);
    $msg .= "<h4>Answer to Question One:</h4>";
    if (count($q1) == 5) {
        $msg .= "You're right! There really isn't a wrong way to 
								brainstorm for terms and ideas before you begin your search<br>";
    } else {
        $msg .= "Nope. Actually...before you begin a search it's 
								best to approach your topic in many ways. All of these strategies are effective.<br>";
    }
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 2</a></div></p>";
}

?>

<h2>Question One</h2>
<h3>Which of the following are effective strategies for choosing search terms? (Choose all that apply)</h3>

<form method="POST" action="q1.php">
<div align="CENTER">
  <table border="0"  width="60%">
    <tr> 
    <td>
    <?php
    if ($q1[0] == "write"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"write\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"write\">";
    endif;
    ?> 
    A. Write out your topic in a few sentences</td>
  </tr>
    <tr> 
    <td>
    <?php
    if ($q1[0] == "highlight" || $q1[1] == "highlight"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"highlight\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"highlight\">";
    endif;
    ?> 
    B. Highlight the main terms and phrases</td>
  </tr>
   <tr> 
    <td>
    <?php
    if ($q1[0] == "brainstorm" || $q1[1] == "brainstorm" || $q1[2] == "brainstorm"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"brainstorm\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"brainstorm\">";
    endif;
    ?> 
    C. Brainstorm synonyms, broader terms, and narrower terms
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q1[0] == "List" || $q1[1] == "list" || $q1[2] == "list" || $q1[3] == "list"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"list\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"list\">";
    endif;
    ?> 
   D. List abbreviations and alternate spellings of words
  </tr>
      <tr> 
    <td>
    <?php
    if ($q1[0] == "Check" || $q1[1] == "check" || $q1[2] == "check" || $q1[3] == "check" || $q1[4] == "check"):
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"check\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q1[]\" value=\"check\">";
    endif;
    ?> 
    E. Check a subject encyclopedia for ideas and concepts
  </tr>
</table>

<p> 

 <input type="hidden" name ="next_question" value="2">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>