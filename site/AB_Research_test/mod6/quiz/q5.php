<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 5; //used in quiz_header.php

$next_question = $_REQUEST["next_question"];
$q5 = $_REQUEST["q5"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q5 == "" && $next_question == 6){
    $err_msg = "Please select a choice";
    $next_question = 5;
}
//Print and record question results
if ($next_question == 6){
    $correct = 0;
    $msg = "<h4>Answer to Question Five:</h4>\n";
    if ($q5 == "true") {
        $msg .= "Yes, Works published by the U.S. Government Printing Office 
								(the largest publisher in the world) are copyright free! This means the Constitution of the U.S.
								and publications from U.S. government agencies are in the public domain. These works may be
								duplicated and used, but credit still must be given to the author when there is one.<br>";
								$correct = 1;
    } else {
    $msg .= "No. Works published by the U.S. Government Printing Office 
				(the largest publisher in the world) are copyright free! This means the Constitution of the U.S.
				and publications from U.S. government agencies are in the public domain. These works may be 
				duplicated and used, but credit still must be given to the author when there is one.<br>";
			 }
    $response = $_COOKIE["response"];
    $response .= "[Question_5: ".$q5."]";
    setcookie ("response", $response);    
    setcookie ("next", "6");
    setcookie ("ans[4]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to Quiz Results</a></div></p>";
}    
?>
<h2>Question Five</h2>
<h3> If the U.S. government is the publisher, the material is not protected by copyright. (Choose one)</h3>
<form method="POST" action="q5.php">
<div align="CENTER">
  <table border="0" width="60%">
  <tr> 
    <td>
    <?php
    if ($q5 == "true"):
        print "<input type=\"radio\" name=\"q5\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"true\">";
    endif;
    ?>
				A. True
    </td>
  </tr>
  <tr> 
    <td>    
    <?php
    if ($q5 == "false"):
        print "<input type=\"radio\" name=\"q5\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q5\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="6">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
