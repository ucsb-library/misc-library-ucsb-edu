<?php
ob_start();
require_once "../../quiz_header.php";
$question = 3; //used in quiz_header.php
foreach ($_REQUEST as $a=>$b){$$a=$b;}

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q3 == "" && $next_question == 4){
    $err_msg = "Please select a choice";
    $next_question = 3;
}

//Print and record question results
if ($next_question == 4){
    $correct = 0;
    $msg = "<font color=#820707><h4>Answer to Question Three:</h4></font>\n";
    if ($q3 == "email") {
        $msg .= "<b>A:</b> No, it has nothing to do with email.
        Article indexes (also called periodical indexes) help you identify
        articles on a subject in journals, magazines, or newpapers.";
    }
    if ($q3 == "web") {
        $msg .= "<b>B:</b> No, an article index helps you identify articles
        on a subject in periodicals. The articles you find using an
        index will have gone through an editorial review process,
        which is not the case with pages you find using the Web. (To
							 search the Web you would use an Internet search engine, but
							 we'll talk more about that later.)<br>";
    } 
    if ($q3 == "citations") {
    $msg .= "<b>C:</b> Right! You would find citations and often abstracts 
    of articles. Sometimes the full text of articles is available, too.<br>";
    $correct = 1;  
    }
    if ($q3 == "library") {
        $msg .= "<b>D:</b> 
        No, a periodical index allows you to find which articles were published in particular
        issues of a magazine or journal, but <b>not</b> if the
        Library owns that specific title. To see if the Library owns the
        issue you need of a periodical, you would use UC-eLinks or the Library catalog.<br>";
    }
    $response = $_COOKIE["response"];
    $response .= "[Question_3: ".$q3."], ";
    setcookie ("response", $response);    
    setcookie ("next", "4");
    setcookie ("ans[2]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 4</a></div></p>";
}

?>

<h2>Question Three</h2>
<h3>Why would you use a periodical index? (Choose one) </span></h3>
<form method="POST" action="q3.php">
<div align="CENTER">
  <table border="0" width="75%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
    <?php
    if ($q3 == "email"):
        print "<input type=\"radio\" name=\"q3\" value=\"email\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q3\" value=\"email\">";
    endif;
    ?>
    A. To check your email
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q3 == "web"):
        print "<input type=\"radio\" name=\"q3\" value=\"web\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q3\" value=\"web\">";
    endif;
    ?>
    B. To search the Web
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q3 == "citations"):
        print "<input type=\"radio\" name=\"q3\" value=\"citations\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q3\" value=\"citations\">";
    endif;
    ?>
   C. To find citations to articles on a topic
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q3 == "library"):
        print "<input type=\"radio\" name=\"q3\" value=\"library\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q3\" value=\"library\">";
    endif;
    ?>
    D. To search for materials available at a specific Library</td>
  </tr>
  </table>
<p>&nbsp;</p>
<input type="hidden" name ="next_question" value="4">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>

</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>