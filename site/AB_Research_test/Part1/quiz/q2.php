<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q2 == "" && $next_question == 3){
    $err_msg = "Please select a choice";
    $next_question = 2;
}
//Print and record question results
if ($next_question == 3){
    $correct = 0;
    $msg = "<h4>Answer to Question Two:</h4>\n";
    if ($q2 == "biological") {
        $msg .= "<b>A:</b> The answer is D. Using &quot;video games AND heart rate&quot; will give you the best results for your topic. <br>";
    }
    if ($q2 == "Pegasus") {
        $msg .= "<b>B:</b> The answer is D. Using &quot;video games AND heart rate&quot; will give you the best results for your topic.<br>";
				}
    if ($q2 == "business") {
    $msg .= "<b>C:</b> The answer is D. Using &quot;video games AND heart rate&quot; will give you the best results for your topic.<br>";
				}
    if ($q2 == "humanities") {
        $msg .= "<b>D:</b> Yes, good job!  Using &quot;video games AND heart rate&quot; will give you the best results for your topic. <br>";
$correct = 1;
    }

    $response = $_COOKIE["response"];
    $response .= "Question_2: ".$q2."], ";
    setcookie ("response", $response);
    setcookie ("next", "3");
    setcookie ("ans[1]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 3</a></div></p>";
}
?>

<h2>Question Two</h2>
<h3>
You�ve just chosen to write a research paper on: &quot;Heart rate and video games.&quot; Identify the best strategy for your search.
</h3>
<form method="POST" action="q2.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr>
    <td>
				<?php
    if ($q2 == "biological"):
        print "<input type=\"radio\" name=\"q2\" value=\"biological\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"biological\">";
    endif;
    ?>
    A. heart rate OR video games
  </tr>
  <tr>
    <td>
   <?php
    if ($q2 == "Pegasus"):
        print "<input type=\"radio\" name=\"q2\" value=\"Pegasus\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"Pegasus\">";
    endif;
    ?>
    B. video OR games OR heart OR rate
  </tr>
  <tr>
    <td>
   <?php
    if ($q2 == "business"):
        print "<input type=\"radio\" name=\"q2\" value=\"business\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"business\">";
    endif;
    ?>
    C. heart AND rate AND video AND games
  </tr>
  <tr>
    <td>
    <?php
    if ($q2 == "humanities"):
        print "<input type=\"radio\" name=\"q2\" value=\"humanities\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q2\" value=\"humanities\">";
    endif;
    ?>
    D. video games AND heart rate
  </tr>

  </table>
<p>
<input type="hidden" name ="next_question" value="3">
<?php
    if ($msg == ""){
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    }
?>
</p>
</div>
</form>
<?php
//Print error message or results
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
