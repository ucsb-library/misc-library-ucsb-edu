<?php
ob_start();
$question = 4;

foreach ($_REQUEST as $a=>$b){$$a=$b;}
//$next_question = $_REQUEST["next_question"];
//$q4 = $_REQUEST["q4"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q4 == "" && $next_question == 5){
    $err_msg = "Please select a choice";
    $next_question = 4;
}
//Print and record question results
if ($next_question == 5){
    $total = count($q4);
    for($i = 0; $i < $total; $i++){
        $response .= "[Question_4: ".$q4[$i]."], ";
        if ($i < ($total - 1))
            $response .= "&";
    }
    setcookie ("next", "5");
    setcookie ("ans[3]", $total);    
    setcookie ("response", $response);
    $msg .= "<h4>Answer to Question Four:</h4>";
    if (count($q4) == 4) {
        $msg .= "Correct! All of the above are good ways to help keep you on track
							 and your paper free from plagiarism.<br>";
    } else {
        $msg .= "Right as far as it goes. Actually all of the methods listed above are good ways to help keep you on track 
								and your paper free from plagiarism.<br>";
    }
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 5</a></div></p>";
}
?>
<h2>Question Four</h2>
<h3>You can avoid plagiarizing by: (Choose all that apply)</h3>
<form method="POST" action="q4.php">
<div align="CENTER">
  <table border="0" align="CENTER" cellpadding="2">
    <tr> 
    <td>
    <?php
    if ($q4[0] == "quotation"):
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"quotation\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"quotation\">";
    endif;
    ?> 
    Using quotation marks when directly stating another person's words. </td>
  </tr>
    <tr> 
    <td>
    <?php
    if ($q4[0] == "sparingly" || $q4[1] == "sparingly"):
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"sparingly\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"sparingly\">";
    endif;
    ?> 
    Using the ideas of other people sparingly and only to support your own argument. </td>
  </tr>
   <tr> 
    <td>
    <?php
    if ($q4[0] == "sources " || $q4[1] == "sources " || $q4[2] == "sources "):
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"sources \" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"sources \">";
    endif;
    ?> 
    Taking notes about your sources, including citation information for each source--even Web sources. 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q4[0] == "draft" || $q4[1] == "draft" || $q4[2] == "draft" || $q4[3] == "draft"):
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"draft\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q4[]\" value=\"draft\">";
    endif;
    ?> 
   Writing a short draft of your paper in thirty minutes without using your notes. 
  </tr>
     		
</table>
<p> 
 <input type="hidden" name ="next_question" value="5">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
