<?php
ob_start();
require_once "../../quiz_header.php";
$question = 3; //used in quiz_header.php
foreach ($_REQUEST as $a=>$b){$$a=$b;}

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q3 == "" && $next_question == 4){
    $err_msg = "Please select a choice";
    $next_question = 3;
}

//Print and record question results
if ($next_question == 4){
    $correct = 0;
    $msg = "<font color=#820707><h4>Answer to Question Three:</h4></font>\n";
    if ($q3 == "email") {
        $msg .= "<b>A:</b> Yes, good job! When using synonyms in a search, you will want to use &quot;OR.&quot;  ";
      $correct = 1;
    }
    if ($q3 == "web") {
        $msg .= "<b>B:</b> The answer is A.  When using synonyms in a search, you will want to use &quot;OR.&quot;  <br>";
    }
    if ($q3 == "citations") {
    $msg .= "<b>C:</b> The answer is A.  When using synonyms in a search, you will want to use &quot;OR.&quot;<br>";

    }
    if ($q3 == "library") {
        $msg .= "<b>D:</b>
        The answer is A.  When using synonyms in a search, you will want to use &quot;OR.&quot;<br>";
    }
    $response = $_COOKIE["response"];
    $response .= "[Question_3: ".$q3."], ";
    setcookie ("response", $response);
    setcookie ("next", "4");
    setcookie ("ans[2]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 4</a></div></p>";
}

?>

<h2>Question Three</h2>
<h3>If you wanted to search for a topic with a few synonyms (e.g. women, girls, female) what word would you use to join the terms or phrases together?</span></h3>
<form method="POST" action="q3.php">
<div align="CENTER">
  <table border="0" width="75%" cellspacing="2" cellpadding="2">
  <tr>
    <td>
    <?php
    if ($q3 == "email"):
        print "<input type=\"radio\" name=\"q3\" value=\"email\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q3\" value=\"email\">";
    endif;
    ?>
    A. OR
  </tr>
  <tr>
    <td>
    <?php
    if ($q3 == "web"):
        print "<input type=\"radio\" name=\"q3\" value=\"web\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q3\" value=\"web\">";
    endif;
    ?>
    B. AND
  </tr>
  <tr>
    <td>
    <?php
    if ($q3 == "citations"):
        print "<input type=\"radio\" name=\"q3\" value=\"citations\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q3\" value=\"citations\">";
    endif;
    ?>
   C. NOT
  </tr>
  <tr>
    <td>
    <?php
    if ($q3 == "library"):
        print "<input type=\"radio\" name=\"q3\" value=\"library\" CHECKED>";
    else:
        print "<input type=\"radio\" name=\"q3\" value=\"library\">";
    endif;
    ?>
    D. NEAR</td>
  </tr>
  </table>
<p>&nbsp;</p>
<input type="hidden" name ="next_question" value="4">
<?php
    if ($msg == ""){
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    }
?>

</p>
</div>
</form>
<?php
//Print error message or results
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>