<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<html> <head> <title>Casting the Net: The Development of a Resource
Collection for an Internet Database</title> </head>
<body bgcolor="#ffffff" text="#000000" link="#0000ff" alink="#b03060" vlink="#d02090">
<img src="web041.gif" alt="Untangling the web">

<hr>

<h1 align=center>Casting the Net: The Development of a Resource Collection
for an Internet Database </h1>


<center><strong><a href="mailto:gerrymck@iastate.edu">Gerry 
McKiernan</a></strong><br> 
Coordinator, Science and Technology Section<br>
Iowa State University, Ames</center><p>
<hr>
<center>Copyright 1996, Gerry McKiernan. Used with permission.</center>
<hr>

<center>
<p>
<em>"We need to develop services that provide<br>
information conveniently and quickly with a<br>
minimal investment of users' time-and to<br>
reduce the quantity of that information to<br>
manageable portions ... Dougherty (1991)."</em></center>

<h2>Abstract</h2>
<blockquote>

CyberStacks(sm) - a demonstration prototype world wide web (WWW)
information service, was formally established in November 1995 on the home
page server at Iowa State University with the intent of facilitating
identification and use of significant Internet resources in science and
technology. CyberStacks(sm) was created in response to perceived
deficiencies in early efforts to organize access to Net resources and the
inherent inadequacies of original and current Internet directories and
search services. It has adapted the Library of Congress classification
scheme and 'neo-conventional' functionality (<a
href="#mckiernan2">McKiernan 1996a</a>) as mechanisms for managing access
to the growing number of information sources made available over the Net
in recent years. <p>

This paper reviews the general features of CyberStacks(sm), analyzes the
decision processes behind its creation, and describes the use of
conventional and innovative information management tools and techniques
that were employed in developing a preview set of candidate titles for
potential incorporation within its collection, and discusses the
associated impact on the future enhancement of the service. <p>
</blockquote>
<hr>
<p>

<h2>Introduction</h2>

Information Overload is not unique to Internet users. It is a condition
that has plagued the Information Society for more than a generation (<a
href="#klapp">Klapp 1986</a>) and has led, at least according to some
authorities, to feelings of frustration, disconnectedness, boredom and
anxiety (<a href="#wurman">Wurman 1989</a>). To assist users in managing
the ever-increasing volume of information, librarians and others have
developed or applied a variety of selection and organizational tools and
techniques which have become commonplace in libraries throughout the world
over the years. <p>

A recent review article provides a concise characterization of the
Information Overload phenomenon as well as succinct profiles of a number
of the methods that traditionally have been used in countering this
problem (<a href="#hopkins">Hopkins 1995</a>). Among the conventional
tools that librarians and others have created to assist users manage
information are guides, handbooks, review articles, literature reviews,
abridgments and rankings, as well as indexes, digests and abstracts, among
other similar services. <p>

In Fall 1995, CyberStacks(sm), URL=<a
href="http://www.public.iastate.edu/~CYBERSTACKS/">
http://www.public.iastate.edu/~CYBERSTACKS/</a> a demonstration prototype
database of selected Internet resources in science, technology and related
areas, was formally established on the home page server at Iowa State
University as a model for managing access to and use of an increasing
number of Internet resources (<a href="#mckiernan">McKiernan 1995</a>). 
CyberStacks(sm) was created in response to perceived deficiencies in early
efforts to organize access to Net resources and the inherent inadequacies
in original and current Internet directories and search services, and has
adapted the Library of Congress classification scheme and
'neo-conventional' functionality (<a href="#mckiernan2">McKiernan
1996a</a>) as mechanisms for providing enhanced organization and access to
significant resources available over the Net. <p>

<h2>Overview</h2>

CyberStacks(sm) is a centralized, integrated, and unified collection of
significant world wide web (WWW) and other Internet resources categorized
using the Library of Congress classification schedules. CyberStacks(sm)
uses an abridged Library of Congress call number, that allows users to
browse through a virtual library stacks to identify potentially relevant
information resources. Resources are categorized first within a broad
classification, then within narrower subclasses, and then finally listed
under a specific classification range and associated subject description
that best characterize the content and coverage of the resource (<a
href="#mckiernan3">McKiernan 1996b</a>). The majority of resources
incorporated within its collection are monographic or serial works, files,
databases or search services. All of the selected resources in
CyberStacks(sm) are full-text, hypertext, or hypermedia, and of a research
or scholarly nature. <p>

<h2>Background</h2>

During the early phases of the implementation of CyberStacks(sm), it
became obvious that any effort to manage Information Overload could itself
become easily overloaded, if it were not appropriately and adequately
defined. Thus, in order to manage a collection of relevant Internet
resources, it was essential that its nature be clearly defined and that
the criteria used for inclusion of a resource be established (<a
href="#mckiernan3">McKiernan 1996b</a>). <p>

After reviewing a number of existing efforts, we decided that the creation
of a collection of significant resources in science and technology could
serve the information needs of specialists within the Science and
Technology Section of the Reference and Instructional Services Department
at Iowa State University, as well as those of its clientele. Since the
Section provided reference, as well as instructional service, it was
decided to establish CyberStacks(sm) initially as a collection of
significant world wide web (WWW) and other Net resources in selected
fields of science, technology and related areas with reference value
(<a href="#mckiernan3">McKiernan 1996b</a>). <p>

<h2>Selection Guidelines</h2>

Although we recognize that the Net offers a variety of resources of
potential value to many clientele and communities for a variety of uses,
we do not believe that one should suspend critical judgment in evaluating
quality or significance of sources available from this new medium. In
considering the general principles which would guide the selection of
world wide web (WWW) and other Internet resources for CyberStacks(sm), we
decided to adopt the same philosophy and general criteria used by
libraries in the selection of non-Internet Reference resources (<a
href="#ala">American Library Association. Reference Collection Development
and Evaluation Committee 1992</a>). These principles, noted below, offered
an operational framework in which resources would be considered as
candidate titles for the collection: <p>

<ol>
<li>Authority of the source
<li>Accuracy of information
<li>Clarity of presentation
<li>Uniqueness within the total collection
<li>Recency or timeliness
<li>Favorable reviews
<li>Community needs.
</ol>
<p>

<h2>Reference Works</h2>

As we wished to create a true 'virtual' reference collection - an
electronic counterpart to our physical collection - only resources that
were the equivalent, or an analog, to a print or other electronic
reference work (<a href="#reference">Reference and Information Services:
An Introduction, 1995</a>) were initially considered for potential
inclusion within the defined collection . In the latter stages of our
review, we modified our concept of candidate reference works to include
resources that could be considered similar in function to any of those
delineated in our defined list (<a href="#reftypes">List of Reference 
Resource Types 1996</a>). <p>

These included compilations of acronyms, abstracting and indexing
services, bibliographies, biographical sources, databases and data files,
dictionaries, directories, encyclopedias, handbooks and manuals, guides to
the literature, indexes, maps, standards, statistical sources, and other
types of reference works which historically have served to assist users in
accessing primary and secondary information sources. <p>

<h2>First Steps</h2>

While we recognized that the limited breadth of CyberStacks(sm) could
hamper its immediate practical usefulness, we believed that its defined
scope provided a manageable collection of resources suitable for an
experimental prototype. While we had not surveyed selected sites to
determine the potential number of resources on the web that might meet the
criteria for inclusion within the CyberStacks(sm) collection, from general
searching we believed that the number would not exceed several dozen
items. Indeed, in response to a preliminary proposal for creating an
organized collection of science and technology reference resources posted
to various listservs and newsgroups in Spring 1995, one respected science
and technology reference specialist commented that he believed that there
were not a sufficient quantity of resources to organize within our
proposed scheme! <p>

Initially, resources were identified in an ad hoc manner; any which could
be considered a conventional reference work were considered acceptable,
including those which would likely be of limited reference value to our
local clientele or specialists. As we did not physically acquire such
works nor did such works compete for shelf space with publications of
greater relevance to our local programs, all potentially relevant Net
resources which met the general criteria were selected for preliminary
consideration for inclusion in the CyberStacks(sm) collection. By
realizing that we need not only focus on local needs, we recognized the
potential of creating a model for a centralized, universally-available
virtual Reference collection. <p>

As potential candidates were identified, the home page was either directly
printed, or e-mailed and then printed, for further review. As time
permitted, candidate sites were revisited and re-evaluated and assigned
to one of the broad classifications established for the initial
CyberStacks(sm) collection (Q, R, S, T, U, V). As opportunities were
presented, those resources considered of greatest potential value were
assigned a more specific Library of Congress class number based on their
content and format. As those assigned to the Science (Q) class included a
range of subjects for which there were quality resources and as Science
(Q) was the first class in the CyberStacks(sm) sequence, we decided to
build the prototype beginning with resources in this broad subject area
(<a href="#construction">Under Construction 1996</a>). <p>

We quickly realized, however, that there were more resources than
originally estimated and that managing them by conventional means was both
time-consuming, inappropriate and paradoxical. It soon became apparent
that it made little sense to attempt to organize a collection of
electronic resources using a paper-based approach; available hardware and
software needed to be used more effectively, or better hardware and
systems needed to be adopted. After a systematic review, it became evident
that the DOS and Windows-based hardware and systems available within the
university Library were inherently limited for a project of this nature
and scope and that a more versatile and powerful system was required. <p>

Although the initial and current CyberStacks(sm) collection was created
directly on the university's computation center UNIX home page server, and
all editing was performed using a UNIX-based editing system, access and
editing of directory files were performed over the campus network on
Windows-based PCs from within the Library. Not only was editing tedious,
but the hardware environment prevented full multi-tasking operations that
could expedite the identification, editing and subsequent incorporation of
significant resources within the CyberStacks(sm) scheme. Ironically, a
project that had sought to mitigate Information Overload was itself on the
verge of becoming unmanageable due to the limitations of established
information management practices and technologies. <p>

<h2>Enter UNIX</h2>

With research funding made available in December 1995 to support a
graduate student assistant, and scheduled holiday vacation approaching, we
decided to investigate the features and functionality of DEC 3000
Unix-based terminals located in a remote public classroom at the
university's computation center. Through a series of tests and trials, and
with the assistance of computation center support staff, we gradually
developed a working knowledge and understanding of these units and their
operating environment. The UNIX platform with its ability to establish
several separate sessions, to copy and paste text and graphics, to
establish multiple Netscape sessions, and to process data more readily,
would prove to be ideal for the next phase of our project. <p>

<h2>Template</h2>

Although CyberStacks(sm) has been initially established as a collection of
significant Internet resources with reference value in the fields of
science, technology and related areas, other types of resources could also
be appropriately incorporated within its collection to create a more
complete reference collection, or, a more comprehensive virtual library.
In anticipating the need to manage an increasing number of resources, we
recognized the benefit of a workform for expediting the development of the
CyberStacks (sm) collection. <p>

While the university's server did not permit the processing of CGI scripts
for forms, a basic template could be created in HTML to facilitate the
selection and preliminary incorporation of candidate resources. As we seek
not to analyze a resource but to characterize it in a manner that permits
the user to judge its potential usefulness (<a
href="#mckiernan4">McKiernan and Ames 1996</a>), the template format was
simple, consisting of three major divisions - a resource title and URL
field, a Summary section and a To Search section. The Summary section
consists of three duplicate HTML blockquote fields, while the To Search
section consists of two. Although we have not sought to standardize the
format of the data provided for each selected resource within
CyberStacks(sm), an effort has been made to include excerpted information
from the source itself that describes its subject coverage, scope, size
and/or record structure, as well as available special features or
functions (<a href="#format">Record Format 1996</a>). <p>

Underlying our selection of resources for the CyberStacks(sm) collection
is a general collection development philosophy that considers discrete
Internet resources as the units to be identified and described within a
virtual collection. Unlike many efforts that seek to organize web
resources, CyberStacks(sm) intentionally seeks to identify and describe
primarily discrete resources, be they unique or part of a larger site. 
Indeed, as we searched for candidate titles for possible inclusion within
its collection, we realized that access to highly significant yet elusive
resources within sites could become one of the more important benefits
offered CyberStacks(sm) users. <p>

<h2>Copy and Paste</h2>

To expedite the incorporation of resources within the template, multiple
copies of the template form were created in a running numerical sequence
(e.g. new1.htm, new2.htm, new3.htm, etc.). With the template series
established, a systematic review was undertaken of all
potentially-relevant Net sites which included resources in the defined
fields of Science (Q) in the Library of Congress classification scheme (<a
href="#lc">LC Classification Outline 1990</a>). Candidate sites were
identified through available search engines (e.g. Yahoo, Lycos, Alta
Vista, etc.) and from appropriate sites (e.g. WWW Virtual Library). Where
appropriate, sites within sites were visited and although sometimes
previously reviewed, were revisited again within the context of a current
site. <p>

As a relevant resource was identified, its title was copied and pasted
into the title field within a new, numbered template file along with its
associated URL; any and all potentially- relevant text that could be used
to characterize the resource was similarly copied and pasted into
appropriate blockquotes of the Summary section of the template file. If
available, text that described procedures for searching or using the
candidate resource was copied and pasted in the To Search division. After
reviewing the content of the current template, it was subsequently saved
and the next template in the numbered series opened. Procedures that would
require half-an-hour on PC Windows computers to manipulate were completed
in minutes on the large-screen, DEC 3000 workstations. <p>

For much of the remainder of December 1995, and early January, any and all
Internet sites that included resources in the fields of medicine,
agriculture, technology, and military and naval science, were visited,
reviewed, and revisited, and data on each candidate resource incorporated
into a temporary template record. In the course of this intensive and
comprehensive systematic survey, over 500 candidate resources were
identified and saved for future review. <p>

<h2>Title Index</h2>

Although we had previously attempted to fully incorporate resources into
the CyberStacks(sm) scheme after identification, in response to
preliminary user feedback, we decided to incorporate each into a
newly-created Title Index (<a href="#title">Title Index 1996</a>) before
full incorporation. In response to user expectations and desires, we
considered it more important to provide some level of access to identified
and relevant resources than to wait until funding and time permitted the
preparation of a complete resource profile (<a
href="#mckiernan2">McKiernan 1996a</a>). <p>

<h2>Cooperative Collection Development</h2>

From the inception of CyberStacks(sm), we recognized the need and benefit
of providing an opportunity for users to assist in its development and
refinement and formally provided opportunities for users to participate in
the development of its collection through a nominating process (<a
href="#nominations">Nominations 1996</a>). <p>

The creation of a separate Title Index has not only provided enhanced
access to selective Net resources, but has also provided a mechanism by
which users of the CyberStacks(sm) collection can more directly
participate in its further development. At a formal level, we have
established four 'virtual' advisory boards (<a href="#advisory">Virtual
Advisory Boards 1996</a>) to assist in the development of CyberStacks(sm),
including one to coordinate the overall selection of candidate resources
for its collection. A coordinator for medical resources (R) has been
appointed and she and her colleagues will assist in the selection of
titles for priority description and incorporation beginning in April 1996.
Likewise, a specialist with professional responsibility for identifying
Internet resources in the field of military science has agreed to assist
in the evaluation of candidate resources in Military Science (U) for
CyberStacks(sm). <p>

As an alternative to focused, coordinated collection development,
potential users will be invited in Spring 1996, through a series of
listserv and newsgroup postings, to view a subset of the Technology (T)
resources in the Title Index and asked to rate these for priority
incorporation within the CyberStacks(sm) collection. <p>

<h2>Conclusion</h2>

In an effort to facilitate access and use of Internet resources, we have
adapted established practices and procedures for managing Information
Overload and information resources that have served librarians for
generations. While the process of adaptation was at times frustrating and
distressing, each challenge presented an opportunity to rethink the
efficiency and effectiveness of conventional methods and techniques, and
provided an impetus to seek a more effective management approach to the
task at hand. <p>

The alternative approaches that emerged in the process not only offered
more efficient methods for developing a collection of web resources, but
have created an opportunity for users to directly assist in defining the
scope and depth of this collection in latter phases of the project. <p>

<hr width=200>
<p>

<h2>References</h2>

<a name="ala">

<strong>American Library Association. Reference Collection Development
and Evaluation Committee.</strong></a> 1992. <cite>Reference Collection
Development: A Manual</cite>. American Library Association, Reference and
Adult Services, Chicago, Ill. <p>

<a name="dougherty"> 
<strong>Dougherty, R M.</strong></a> 1991. Editorial: Balancing Access and
Overload. <cite>Journal of Academic Librarianship</cite> 16 (6):339. <p>

<a name="hopkins">
<strong>Hopkins, R. L.</strong></a> 1995. Countering Information Overload: 
The Role of the Librarian. <cite>Reference Librarian</cite> 49/50:305-333.
<p>

<a name="klapp">
<strong>Klapp, O. E.</strong></a> 1986. <cite>Overload and Boredom: 
Essays on the Quality of Life in the Information Society</cite>. Greenwood
Press, Westport, Conn. <p>

<a name="lc">
<strong><cite>LC Classification Outline</cite></strong>.</a> 1990. Library of
Congress, Washington. D.C. <p>

<a name="reftypes"> <strong>"<cite>List of Reference Resource
Types</cite>."</strong></a> [<a
href="http://www.public.iastate.edu/~CYBERSTACKS/ref_book.htm">http://www.public.iastate.edu/~CYBERSTACKS/ref_book.htm</a>].
17 March 1996 <p>

<a name="mckiernan"> <strong>McKiernan, G.</strong></a> "CyberStacks(sm):
A 'Library-Organized' Virtual Science and Technology Reference
Collection." In <cite>D-Lib Magazine</cite> [<a
href="http://www.dlib.org/dlib/december95/briefings/12cyber.html">http://www.dlib.org/dlib/december95/briefings/12cyber.html</a>].
December 1995. <p>

<a name="mckiernan2">
<strong>McKiernan, G.</strong></a> 1996a. Build It and They Will Come: A Case
Study of the Creation, Development and Refinement of an Organized Database
of Internet Resources in Science and Technology. Paper prepared for
<cite>The Digital Revolution: Assessing the Impact on Business, Education
and Social Structures</cite>. The 1996 Mid-Year Meeting of the American
Society for Information Science, May 18-22, 1996, San Diego, California.
<p>

<a name="mckiernan3">
<strong>McKiernan, G.</strong></a> 1996b. The New/Old World Wide Web
Order: The Application of 'Neo-Conventional' Functionality to Facilitate
Access and Use of a WWW Database of Science and Technology Resources.
Submitted to <cite>Journal of Internet Cataloging</cite> April 1996. <p>

<a name="mckiernan4"> <strong>McKiernan, G. and A. Ames.</strong></a>
"two-dimensional limitations / 3-D Possibilities - CyberStacks(sm): An
Alternative Model for Selecting | Organizing | Presenting | Accessing WWW
Resources: A Position Paper Prepared for the <cite>OCLC Internet
Cataloging Project Colloquium</cite>, [<a
href="http://www.public.iastate.edu/~CYBERSTACKS/OCLC.htm">http://www.public.iastate.edu/~CYBERSTACKS/OCLC.htm</a>].
2 February 1996 <p>

<a name="nominations">
<strong>"<cite>Nominations</cite>."</strong></a>
[<a 
href="http://www.public.iastate.edu/~CYBERSTACKS/nominate.htm">http://www.public.iastate.edu/~CYBERSTACKS/nominate.htm</a>]. 17 
March 1996 <p>

<a name="format">
<strong>"<cite>Record Format</cite>."</strong></a>
[<a 
href="http://www.public.iastate.edu/~CYBERSTACKS/record.htm">http://www.public.iastate.edu/~CYBERSTACKS/record.htm</a>]. 17 
March 1996 <p>

<a name="reference">
<strong><cite>Reference and Information Services: An
Introduction</cite>.</strong></a> 1995. Edited by Richard E. Bopp and 
Linda C. Smith. Libraries Unlimited, Englewood, Colo. <p>

<a name="title">
<strong>"<cite>Title Index</cite>."</strong></a> 
[<a 
href="http://www.public.iastate.edu/~CYBERSTACKS/title_lst.htm">http://www.public.iastate.edu/~CYBERSTACKS/title_lst.htm</a>]. 17 
March 1996 <p>

<a name="construction">
<strong>"<cite>Under Construction</cite>."</strong></a>
[<a 
href="http://www.public.iastate.edu/~CYBERSTACKS/under.htm">http://www.public.iastate.edu/~CYBERSTACKS/under.htm</a>]. 17 
March 1996 <p>

<a name="advisory">
<strong>"<cite>Virtual Advisory Boards</cite>."</strong></a>
[<a 
href="http://www.public.iastate.edu/~CYBERSTACKS/advisory.htm">http://www.public.iastate.edu/~CYBERSTACKS/advisory.htm</a>]. 17 
March 1996 <p>

<a name="wurman">
<strong>Wurman, R. S.</strong></a> 1989. <cite>Information Anxiety</cite>. 
Doubleday, New York, N.Y. 
<p>

<P> <A HREF="http://validator.w3.org/"><IMG SRC="http://www.library.ucsb.edu/gallery/valid_html3.2.gif" ALT="HTML 3.2 Checked!" border=0></A>

</body>
</html>


