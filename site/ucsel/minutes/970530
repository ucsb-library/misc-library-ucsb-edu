From lmorrise@sun1.lib.uci.eduMon Jul 28 16:38:06 1997
Date: Mon, 28 Jul 1997 14:10:31 -0700 (PDT)
From: "Locke J. Morrisey " <lmorrise@sun1.lib.uci.edu>
To: lib_ucsel@sun1.lib.uci.edu
Subject: Final Notes from the May 30th Southern UC Science & Engineeering Librarians Meeting at UCR 

Attendees: Dick Vierich, Ann Jensen, Dawn Talbot, Christy Hightower,
Locke Morrisey, Margo Young, Steve Mitchell, Michele Potter, Audrey
Jackson, Sharon Shafer, Carlos Rodriguez, Barbara Schader, Ruth
Gustafson, Charlene Baldwin, Andrea Duda, Peter Briscoe, Diana Lane

1. Update on ACTION items from Fall Meeting.

   --SCAP/mini-SCAP projects: There is no more money systemwide for these
     projects. They would have to be done at the local level. 

   --Elaine Adams (UCLA) has worked out options to make it easier for
     campuses to tell if SRLF actually has a technical report in their
     collection before being sent a box of material that may or may not    
     have the report. A very useful finding guide and summary of UCLA tech
     report depositories handout was distributed. 
   
   --Reminder the URL for the UCSEL webpage is
                     
                       http://www.library.ucsb.edu/ucsel/ 

     Andrea Duda (UCSB) maintains the webpage for UCSEL. Contains links to
     UC/Stanford Librarians Science Librarians list, Minutes of Meeting,
     Cancellation lists, UC Consortial projects (currently Locke Morrisey 
     & Christy Hightower have a consortial deal with METADEX), etc. 

     ACTION: It was also suggested that a UC/Stanford Unionlist of
     Standards be added. Andrea is willing to work on this. PLEASE SEND
     OTHER IDEAS TO HER AT: duda@ariz.library.ucsb.edu

   --ACTION: A query will be sent out regarding which campuses are
     thinking of adding web resources that others might want to go in on
     consortially. 

   --Currently 7 UC Science & Engineering librarians are participating
     in adding websites to Infomine (over 3000 recent additions & over
     500,000 hits/month). Infomine will "move" from UC Riverside to UC
     Office of the President (UCOP) shortly.

2. Update on UC Digital Library Project

Locke, a member of the Science, Technology, Industry Collections (STIC)
taskforce  gave an update of information Susan Starr had shared with him
about the project.

He also asked for feedback from the group as to what they would like to
see in (or other concerns about) STIC:

--We don't subscribe to everything a publisher has in paper. Why would we
do so in electronic format?

--A lot of effort has already been put into cataloging and linking
Springer journals (that are part of the test)

--We need to find key items that will make the site "shine" (a patchwork
of resources will not get used)

--We need to make sure we don't disenfranchise "whole animal" people, as
opposed to cell or micro-biologists.

--Environmental science would be a good place to focus.

--More than just the current year or two of e-journals would be useful

--Would like to see Highwire Press materials added.

--Would like to encourage addition of Society publications.

--It's important to have a critical mass (combination of Society and
Commercial publishers), if it's going to get used.

--Would like to see materials that are available in non-pdf formats.

--Would like to see links to MELVYL databases for e-journals that are
subscribed to.


3. Subject specific librarian groups (as was discussed at the Northern
Meeting)

Consensus of the group was that the formation of these subject specific
groups was redundant and the group should remain as it is, with the
ability to form ad hoc subject specific groups as needed.

4. Journal cancellations (campus round-robin)
   --What criteria/strategies are we all using?

UCSD

--Science & Engineering: Studied use data for a 5 month period for
journals 1991-present. Also used citation data. From this a "proposed
list" was created that amounted to about 20-23% cut. Information was
posted on a website where faculty could comment.

--Biomed: Cut 402 of 1300 titles from 2 Biomed/Med libraries
($160,000). Dropped a lot of standing orders. Had biotech corporations who
use their collections "sponsor" a couple of journal titles. Eliminated
duplication of reference materials between libraries. Expressed concern
about how to pay for electronic journals and how much money will really be
save by converting to electronic from paper.

UCSD is hoping for an augmentation to their collections base so the cut
won't be so bad.

UCB

Engineering

--2 years of use data to make decisions. Looking at cutting 13%, 13% and
25% over consecutive years. Using CISTI for some direct document delivery.
Is getting some kickback from EBSCODOC because the service uses their
collections so heavily.

UCR

--Good news. Chancellor has added money ($1.5 million) to the library's
base most of it going to collections as part of a "Library Recovery Plan".
If journal prices do go up more than 30% department liaisons are asked to
review that particular title for potential cancellation.

UCLA

--Science & Engineering: For 1996/1997 FY cancelled $230,000 (15% cut). 
Database was created that included citation and building use data as well
as increases in price from 1993 to the present. For 1997/1998 the focus is
on cancellation Reference works and standing orders. They will be in a
"cancellation mode" every year. 

--Biomed: Had cancelled every year from 1987-1991. Use studies, impact
factor and duplication were looked at. Also any titles that were more than
$1000 or increased by more than 15% in price. List was reviewed by
librarians and titles that were more than $35/use were targeted (amounted
to about 400 titles at $433,000). Trying to subscribe to both paper and
electronic formats for now (most labs still can't support e-journals)

UCSB

--Cut 17 1/2% last year. No cuts this year. 

UCI

--Engineering/Medicine/Science cut $160,000 last year and looking at
cutting $275,000-$300,000 this year. Each discipline used different
method to create lists: e.g. hand-picked by faculty, citation/usage data,
etc.

5. Commercial Document Delivery (CDD)
   --Who's using it on a regular basis? If it's being used, which service 
     are you using? How often are you using it? Are you happy with the
     service you're getting? What is your success rate? When do you choose
     to use CDD vs. ILL? Is any of the cost subsidized?

UCSB: Using "Rapid Patent" for patents.
 
UCSD: Buying UMI Dissertations when requested (library pays 1/2; user pays
1/2). User keeps dissertation.

UCB: Uses CISTI--articles $15-18/each and come via Ariel or uses British
Lending Library for books ($20-$30/request) particularly IEE Colloquia.

UCLA: Uses Linda Hall.

6. Faculty hiring packages and the library (How successful is the library
in getting additional funds for new programs or research areas?)

Understanding is that UCSC has been somewhat successful in getting library
funds for new programs. UCR talks to individual departments and tries to
get faculty to support the idea. A certain portion of grant money overhead
is supposed to come back to the libraries but it's not clear that this
money is ever seen.

7. E-journal update (Campus round-robin as to how licensing agreements
are being handled, how decisions are being made print vs. electronic, how
faculty and students alerted of these new e-journals?)

NOT ALL CAMPUSES RESPONDED.

UCSD: Has a webpage for e-journals and has created a front end called
"Databased Advisor". 

UCLA: Reviews and modifies contracts and then sends it back to the vendor
to see if it's accepted. 

ACTION: An ad hoc committee was formed (Audrey Jackson (UCLA), Barbara
Schader (UCLA) and Margo Young (UCR)) to look into the possibilities and
necessary procedures for SRLF becoming an archival depository for "single
paper copy" of e-journals.

8. Rowecom (Who's using it? What are campus' responses?)

UCLA seems to be the one using them and has had several problems with the
vendor.

9. Next meeting place: Fall at UC Irvine. Exact date TBA.

Notetaker: Locke Morrisey

