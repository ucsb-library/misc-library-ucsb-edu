<?php
//session_start();
include("mail_fns.php");


// get the directory that this script was
// called from the first time it is called
// and pass it along in a session variable (cookie)

// replaced this with non session method just in case
// there are issues with cookies

/*
if(isset($_SESSION["my_dir"])){
   $my_dir=$_SESSION["my_dir"];
}else{
   $my_dir= getPath();
   session_register($my_dir);
   $_SESSION["my_dir"]=$my_dir;
}
*/

// get the directory that this script was
// called from the first time it is called
// and pass it along in a hidden form field

//if(isset($HTTP_POST_VARS['mydir'])){
if(isset($_POST['mydir'])){
	//$my_dir=$HTTP_POST_VARS['mydir'];
	$my_dir=$_POST['mydir'];
	//print"<h1>ISSET: My Directory is $my_dir</h1><br />\n";
}else{
	$my_dir=getPath();
	//print"<h1>NOTSET: My Directory is $my_dir</h1><br />\n";
}


// referrer that is allowed to use this script

$url_allowed_from="library.ucsb.edu";

$allowed= checkUrl();


// if there is a mail_confirm.php page in the directory where this
// script was called from use it. Otherwise use the default file in
// the same directory as the master script (this script)

if(is_file("$my_dir"."mail_confirm.php")){
		$confirm_file="$my_dir"."mail_confirm.php";
	}else{
		$confirm_file="./mail_confirm.php";
}

// Get an array of special fields and lump everything else into
// an array called body

//$parsed=parseForm($HTTP_POST_VARS);
$parsed=parseForm($_POST);
extract($parsed);

// Get the list of recipients for the email from the sendto.php text
// file.
# $to=getTo($my_dir,$HTTP_POST_VARS);
$to = $_POST['to']; 

//print"<br>Submit:$submit<br>\n";

// If the submit form element that we came to this page with is set to
// Review Form generate the html for both the displayed values and the
// hidden form elements used to pass the values on to the next step.
if($submit=="Review Form"){
	  $confirm_vals=getConfirmVals($parsed,$my_dir);
	  $hidden_form_vals=$confirm_vals['hidden'];
	  $display_form_vals=$confirm_vals['display'];
	  
		 if(is_file("$confirm_file")){
		 	include("$confirm_file");
		 }
		 
}elseif($submit=="Submit Form"){  // otherwise process and send email

// Check the existence of the external and internal thanks pages
// Use them if they exist otherwise use the defaults located in
// the same directory as the main script. (this script)

	if(is_file("$my_dir"."mail_thanks_ext.php")){
		$thanks_ext_file="$my_dir"."mail_thanks_ext.php";
	}else{
		$thanks_ext_file="./mail_thanks_ext.php";
	}

	if(is_file("$my_dir"."mail_thanks.php")){
		$thanks_file="$my_dir"."mail_thanks.php";
	}else{
		$thanks_file="./mail_thanks.php";
	}
	
	
// Format the fields in the body array to be printed in the email
// Currently just removes underscores from fieldnames and uses the
// format FIELDNAME = FIELDVALUE<RETURN>
$body_array=$parsed['body'];
$mail_out = '';
$date=date("D M j G:i:s T Y");
$mail_out ="Submitted: $date\n\n";

// This is where this script seems to fall apart

foreach ($body_array as $fieldName => $fieldValue) {
	if(is_array($fieldValue)){
		foreach ($fieldValue as $key=>$value){
			$key=preg_replace("_"," ","$key");
			$mail_out .= "$key = $value\n";
		}
	 }else{
	 	 $fieldName=preg_replace("/_/"," ","$fieldName",-1);
		 $mail_out .= "$fieldName = $fieldValue\n";
	 }
}


	  
// Send the message and capture any feedback if there is an error
$feedback=mailIt($from,$to,$subject,$mail_out);
if ($feedback !="true"){
print"error $feedback<br>"; // print the error if there was one
exit;
}
	
	if(preg_match("/nonucsb/","$HTTP_REFFERER") or $nonucsb=="true"){
		include("$thanks_ext_file");
		// this doesn't work if we want to re-submit the form over and over
		// like for testing.. <grin>
		//unset($_SESSION["my_dir"]);
	}else{
		include("$thanks_file");
		//unset($_SESSION["my_dir"]);
	}
	

}




?>
