<?php
ob_start();
$question = 2;

foreach ($_REQUEST as $a=>$b){$$a=$b;}
//$next_question = $_REQUEST["next_question"];
//$q2 = $_REQUEST["q2"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q2 == "" && $next_question == 3){
    $err_msg = "Please select a choice";
    $next_question = 2;
}

//Print and record question results
if ($next_question == 3){
    $total = count($q2);
    for($i = 0; $i < $total; $i++){
        $response .= "Question_2: ".$q2[$i]."], ";
        if ($i < ($total - 1))
            $response .= "&";
    }
    setcookie ("next", "3");
    setcookie ("ans[1]", $total);    
    setcookie ("response", $response);
    $msg .= "<h4>Answers to Question Two:</h4>";
    if (count($q2) == 5) {
        $msg .= "<b>Correct!</b> All of these are elements used in citing sources, depending on the type 
								of source (Web, a journal article, a book, etc.). This is the type of information 
								you will need when you cite your sources.<br>";
    } else {
        $msg .= "Almost right. Actually, all of these are elements used in citing sources, depending on 
								the type of source (Web, a journal article, a book, etc.). This is the type of information 
								you will need when you cite your sources.<br>";
    }
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 3</a></div></p>";
}
?>
<h2>Question Two</h2>
<h3>
The type of information that you need for citing sources include: (Choose all that apply)</h3>
<form method="POST" action="q2.php">
<div align="CENTER">
  <table border="0" width="60%" align="CENTER">
    <tr> 
    <td>
    <?php
    if ($q2[0] == "date"):
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"date\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"date\">";
    endif;
    ?> 
    Date</td>
  </tr>
    <tr> 
    <td>
    <?php
    if ($q2[0] == "name" || $q2[1] == "name"):
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"name\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"name\">";
    endif;
    ?> 
    Author's Name</td>
  </tr>
   <tr> 
    <td>
    <?php
    if ($q2[0] == "issue " || $q2[1] == "issue " || $q2[2] == "issue "):
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"issue \" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"issue \">";
    endif;
    ?> 
    Issue 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q2[0] == "journal" || $q2[1] == "journal" || $q2[2] == "journal" || $q2[3] == "journal"):
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"journal\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"journal\">";
    endif;
    ?> 
   Title of Journal
  </tr>
      <tr> 
    <td>
    <?php
    if ($q2[0] == "url" || $q2[1] == "url" || $q2[2] == "url" || $q2[3] == "url" || $q2[4] == "url"):
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"url\" CHECKED>";
    else: 
        print "<input type=\"checkbox\" name=\"q2[]\" value=\"url\">";
    endif;
    ?> 
    URL
  </tr>				
</table>
<p> 
 <input type="hidden" name ="next_question" value="3">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
