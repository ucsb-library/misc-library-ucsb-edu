<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"> 

<html><head><title>Web-Writing in One Minute</title></head> 
<body bgcolor="ffffff" text="000000"
link="0000ff" alink="b03060" vlink="d02090">
<img src="saturn.gif" alt="The Universe at Your Fingertips">

<hr>
<h1 align=center>Web-Writing in One Minute -- and Beyond</h1>

<center>
<strong><a href="mailto:kenh@barepower.net">Kenneth Hughes</a></strong><br>
</center>
<hr>
<center>
Copyright 1997, Kenneth Hughes. Used with permission.
</center>
<hr>

Modern libraries and librarians are often among the first to develop their
own World Wide Web pages.  Even among those of us who do not know how,
there's a strong interest in learning, and an awareness of how many patrons
would enjoy having their own presence on the Web... if only someone could
teach them.<p>

More and more these days, people say "the net" and really mean
the Web.  The Web is popular because it's so easy to use, simply browsing
through what it offers.  It's the first Internet feature that does not
require knowing other people on the net and trusting them to write back,
let alone for you to write code yourself-- in effect, it's a library.<p>
But should we, as librarians, be satisfied with people reading but never
getting involved?<p>

Without a knowledge of HTML, Web users can still do instant research on
whatever they wish, while all the time being exposed to the power of new
information models.  Our profession favors this in principle, certainly.
But browsing is only the beginning of the appeal the Web can have; we all
know people who've followed some published URL and wished they could have
their own homepages up, or who we think would embrace the possibilities of
Web publishing if they only knew how.  In the end, who
<strong>doesn't</strong> want to sound off on what's important to them?<p>
And yet these people hold back.  Clicking around cyberspace is easy, but
creating on it is **Learning Computers** at the level of code, one of the
most dreaded things a person can attempt.  There's no way it could be as
easy as using the Web, they're sure.<p>

But as the title of this paper says, it can.<p>

There are no small number of HTML tags in use today, but the truth is that
if you separate the "window dressing" (no pun intended) from the
bare basics it's very simple to set people up on the Web.  After all, it's
generally accepted that the best way to teach includes featuring what
people value, in manageable doses.<p>

In fact, in demonstrations I have shown that the initial sharing of basic
HTML information can be done in sixty seconds, by the clock:<p>
There are three things to learn.  First is the basic template of Web tags,
which can be copied out from a standing file: <p>

&lt;html&gt;<p>
&lt;head&gt;&lt;title&gt;YOUR TITLE HERE&lt;/title&gt;&lt;/head&gt;<p>
&lt;body&gt;<p>
&lt;h1&gt;YOUR TITLE AGAIN (OR ANOTHER VERSION)&lt;/h1&gt;<p>
ALL THE CONTENT HERE<p>
&lt;/body&gt;<p>
&lt;/html&gt;<p>

Notice how the title appears in two places (the second is an unrelated,
more traditional place to show a title), and all the rest of the content
goes in one place after that.<p>

Secondly, certain typed characters cannot be used as is, and must be
replaced:<p>

Paragraph-break &lt;p&gt;<p>
        "          &amp;quot;<p>
        &amp;           &amp;amp;<p>
        &lt;            &amp;lt;<p>
        &gt;            &amp;gt;<p>

Thirdly, even bare-bones HTML needs the tag for a hyperlink.  This requires
simply the other page's address and setting the following tags around the
words you've chosen for the link:<p>

<a name="ADDRESS">WHAT YOU'LL CLICK ON</a><p>
<p>

With proper use of handouts and practice time afterwards, the above can be
conveyed in only a minute if needed.  Introducing people to HTML really can
be that simple, by prioritizing the basics to let the student see that it
works.<p>

Specific lesson plans can be adapted out of this to suit different needs.
For instance, for a colleague you might spend five minutes of each lunch
hour for three days to convey the three key steps, allowing him/her to
contemplate the growing Web page in off-hours.  A one-hour class has ample
time to let students explore these basics at their own pace, with each
creating a page perhaps about each's favorite subject, or their business or
resume or favorite Web sites.  What matters is finding a form that takes
full advantage of the simple content and appealling benefits of HTML to
pull the student past any initial awkwardness.<p>

The same customizing principles can also be of value in continuing the
lessons beyond the bare bones.  I prefer to know how a student hopes to use
the Web, so as to quickly teach the tags that convey that effect... while
accompanying them with warnings about the dangers of cramming too many
exotic Web tricks in just to show off.<p>

Before learning any tags beyond the basics, I believe a person should have
a plan as to what sections a subject should be divided into, and which ones
are separate Web pages and which subsections of a page.  Once those are
carefully divided, with the top of each page and each link giving a clear
sense of what's beyond, the person is ready to learn the proper tags.  (For
more about organization, see my "Organization in Web Structure"
at http://www.barepower.net/~kenh/WebStructure.html.  Also, rather than
fully explain the tags' rules here, I refer novice readers to "A
Beginner's Guide to HTML", at
http://www.ncsa.uiuc.edu/General/Internet/WWW/HTMLPrimer.html)<p>

Some people's priorities will be to clearly organize and emphasize their
page's parts.  For this, the essential tags are &lt;h1&gt;, &lt;strong&gt;,
and &lt;hr&gt;.  They will also want to itemize thoughts, lists of other
links, and tables of contents; these call for &lt;OL&gt; and &lt;LI&gt; or
the ever-popular &lt;UL&gt; and &lt;LI&gt;.  And they should be aware of
&lt;a name=" "&gt; and &lt;a href="# "&gt; &lt;/a&gt;
to link to a place within a page.<p>

(This goal also is best served when people remember to state the date and
comprehensiveness of their information, and always begin a page by
establishing enough of its context for any readers who may have come to it
without using the main homepage first.  Just by mastering these methods,
users can ultimately create a true Vannevar Bush "memex" that
arranges their and others' knowledge for maximum understanding.)<p>
Other people want to put more flash into pages.  For them, the priority is
of course learning to place images with &lt;img alt=" "
src=" "&gt; along with learning how their platform can both get
and mount images through the Web.  There is also &lt;pre&gt; that can be
used to insert multiple spaces or paragraph breaks, and the importance of
giving each of their pages a similar "masthead" for recognition
value.  Other tricks such as tables and backgrounds can be found at
"HTML Goodies" at http://www.htmlgoodies.com/, and any HTML code
can be double-checked by learning the View Source command in the browser to
compare it to existing tags.  Learning these methods lets people put
together some decidedly impressive pages.<p>

Finally, some people may want to not only mount Web pages but communicate
with other users.  These people should be helped with the e-mail link tag,
&lt;a href="mailto: "&gt; &lt;/a&gt;, and perhaps the Forms tags
(found in "HTML Goodies" above).<p>
The last is perhaps the ultimate goal of a Web teacher, encouraging people
not only to complement their Web browsing with their own page but to begin
exchanging specific e-mail with others about their pages and interests.  It
is this level of engagement that can truly show people the value of
information and the newest methods of finding it.<p>
Information, and our skill in using it, is increasingly what lets a person
succeed in the world today... just as misunderstanding it can encourage
censorship or other distortions of that potential, and ignoring it lets a
person fall behind while others draw further ahead.  As librarians, we have
a professional comittment to see that people make better use of the world
of information, not only when they walk through our doors but after they
leave.<p>

So, who here can spare sixty seconds?     

 
<p>
<hr>
 <p><A HREF="http://validator.w3.org/"><IMG SRC="http://www.library.ucsb.edu/gallery/valid_html3.2.gif" ALT="HTML 3.2 Checked!" border=0></A>
</p></body>
</html>
