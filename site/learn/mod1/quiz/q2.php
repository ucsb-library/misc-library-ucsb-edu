<?php
ob_start();
require_once "../../quiz_header.php";
$question = 2;
$next_question = $_REQUEST["next_question"];
$a = $_REQUEST["a"];
$b = $_REQUEST["b"];
$c = $_REQUEST["c"];

//Check that question hasn't been answered

$msg = chk_question($next_question);
if ($msg != "") {
$next_question = 0;
}
//Check that at least one response has been entered
if ($a == "" && $next_question == 3){
$err_msg = "Please select an answer for A";
$next_question = 2;
} 
else if ($b == "" && $next_question == 3){
$err_msg = "Please select an answer for B";
$next_question = 2;
} else if ($c == "" && $next_question == 3){
$err_msg = "Please select an answer for C";
$next_question = 2;
}

//Print and record question results

if ($next_question == 3){
$correct = 0;
$msg = "<font color=#820707><h4>Answers to Question Two:</h4></font>";
$msg .= "<b>A: </b>";
if ($a == "library"){
$msg .= "No! Organizing information is one of the things libraries do best.<br>";
} else {
$msg .="The Web has a lot of of information that's pretty unorganized.<br>";
$correct++;
}
$msg .= "<b>B: </b>";
if ($b == "library") {
$msg .= "Right, Editors, reviewers and librarians all review materials collected by the Library.<br>";
$correct++;
} else {   
$msg .= "No, information on the Web doesn't go through a formal review process before being published.  
         Because there is no quality control, some information on the Web is unreliable.<br>";
} 
$msg .= "<b>C: </b>";
if ($c == "library") {
$msg .= "Yes, Libraries collect information from previous decades and 
centuries. They keep it permanently for people to refer back to.<br>";
$correct++;
} else {
$msg .= "Nope. Information on the Web changes frequently, is easily updated,
         and may be removed without notice.<br>";
}

$response = $_COOKIE["response"];
$response .= "[Question_2a: ".$a.", Question_2b: ".$b.", Question_2c: ".$c."], ";
setcookie ("response", $response);
setcookie ("next", "3");
setcookie ("ans[1]", $correct);
$msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 3</a></div></p>";
}

?>
<h2>Question Two</h2>
<h3>Does each statement best describe information in the Library<br>
or information on the Web? (Choose one answer for each section)</h3>

<form method="POST" action="q2.php">
<div align="CENTER"> 
<table border="0" width="75%" cellspacing="4" cellpadding="4">
<tr> 
<td colspan="2"><b>A. Information is not organized.</b> </tr>
<tr> 
<td width="57%"> 
<div align="CENTER"> 
<?php

    if ($a == "library"):

        print "<input type=\"radio\" name=\"a\" value=\"library\" CHECKED>";

    else: 

        print "<input type=\"radio\" name=\"a\" value=\"library\">";

    endif;

    ?>Library 
				
</div>
<td width="43%"> 
<?php

    if ($a == "web"):

        print "<input type=\"radio\" name=\"a\" value=\"web\" CHECKED>";

    else: 

        print "<input type=\"radio\" name=\"a\" value=\"web\">";

    endif;

    ?>
Web</td>
</tr>
<tr> 
<td colspan="2"><b>B. Information is selected through a review process.</b> </tr>
<tr> 
<td width="57%"> 
<div align="CENTER"> 
<?php

    if ($b == "library"):

        print "<input type=\"radio\" name=\"b\" value=\"library\" CHECKED>";

    else: 

        print "<input type=\"radio\" name=\"b\" value=\"library\">";

    endif;

    ?>
Library </div>
<td width="43%"> 
<?php

    if ($b == "web"):

        print "<input type=\"radio\" name=\"b\" value=\"web\" CHECKED>";

    else: 

        print "<input type=\"radio\" name=\"b\" value=\"web\">";

    endif;

    ?>
Web</td>
</tr>
<tr> 
<td colspan="2"><b>C. Information is in a permanent collection.</b> </tr>
<tr> 
<td width="57%"> 
<div align="CENTER"> 
<?php

    if ($c == "library"):

        print "<input type=\"radio\" name=\"c\" value=\"library\" CHECKED>";

    else: 

        print "<input type=\"radio\" name=\"c\" value=\"library\">";

    endif;

    ?>
<font color="#000333">Library</font> </div>
<td width="43%"> 
<?php

    if ($c == "web"):

        print "<input type=\"radio\" name=\"c\" value=\"web\" CHECKED>";

    else: 

        print "<input type=\"radio\" name=\"c\" value=\"web\">";

    endif;

    ?>
Web</td>
</tr>
</table>
<p> 
<input type="hidden" name ="next_question" value="3">
<?php 

    if ($msg == ""){ 

        //is msg != "" then the form has been submitted

        print "<input type=\"submit\" value=\"submit\">";

    } 

?>
</p>
</div>
</form>
<?php

//Print error message or results 

if ($err_msg != ""){

    print "

    <script language=\"JavaScript\">

    <!--

    onLoad=window.alert(\"$err_msg\")

    -->

    </script>";

}

print "$msg";

require_once "../../quiz_footer.php";

?>
