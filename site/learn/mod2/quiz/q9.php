<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 9; //used in quiz_header.php
$next_question = $_REQUEST["next_question"];
$q9 = $_REQUEST["q9"];

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}

//Check that at least one response has been entered
if ($q9 == "" && $next_question == 10){
    $err_msg = "Please select a choice";
    $next_question = 9;
}
//Print and record question results
if ($next_question == 10){
    $correct = 0;
    $msg = "<h4>Answer to Question Nine:</h4>\n";
    if ($q9 == "false") {
        $msg .= "<b>B:</b> Right on! The AND connector narrows your search because <b>both</b> terms must be present.<br>";
        $correct = 1;
    } else {
    $msg .= "<b>A:</b> Well...no. The AND connector narrows your search because <b>both</b> terms must be present.<br>";
}
    $response = $_COOKIE["response"];
    $response .= "[Question_9: ".$q9."]";
    setcookie ("response", $response);    
    setcookie ("next", "10");
    setcookie ("ans[8]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to Quiz Results</a></div></p>";
}    
?>

<h2>Question Nine</h2>
<h3>Using the connector AND broadens your search, and you will get more records back. </h3>
<form method="POST" action="q9.php">
<div align="CENTER">
  <table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td><?php
    if ($q9 == "true"):
        print "<input type=\"radio\" name=\"q9\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q9\" value=\"true\">";
    endif;
    ?>
				A. True  
    </td>
  </tr>

  <tr> 
    <td>   
    <?php
    if ($q9 == "false"):
        print "<input type=\"radio\" name=\"q9\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q9\" value=\"false\">";
    endif;
    ?>
				B. False
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="10">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>

<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>