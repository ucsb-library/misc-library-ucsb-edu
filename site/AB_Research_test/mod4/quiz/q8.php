<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 8;

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q8 == "" && $next_question == 9){
    $err_msg = "Please select a choice";
    $next_question = 8;
}
//Print and record question results
if ($next_question == 9){
    $correct = 0;
    $msg = "<h4>Answer to Question Eight:</h4>\n";
    if ($q8 == "source") {
        $msg .= "<b>B:</b> Right. You are correct to search the title of the periodical in the library catalog.<br>";
								$correct = 1;
				} 
				else if ($q8 == "title"){
						$msg .="<b>A:</b> You have the citation, so already know the title of the article, the author, and subject. 
						You need to search the online catalog to see if the library subscribes to <i>U.S. News & World Report.</i><br>";
				}
				else if ($q8 == "author"){
						$msg .="<b>C:</b> You have the citation, so already know the title of the article, the author, and subject. 
						You need to search the online catalog to see if the library subscribes to <i>U.S. News & World Report.</i><br>";
				}
				else if ($q8 == "subject"){
						$msg .="<b>D:</b> You have the citation, so already know the title of the article, the author, and subject. 
						You need to search the online catalog to see if the library subscribes to <i>U.S. News & World Report.</i><br>";
				}
				
    $response = $_COOKIE["response"];
    $response .= "[Question_8: ".$q8."], ";
    setcookie ("response", $response);
    setcookie ("next", "9");
    setcookie ("ans[7]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 9</a></div></p>";
}
?>

<h2>Question Eight</h2>
<h3 align="left"><font color="#1F4366"> You have this citation for an article:</font></h3>
  <table width="79%" border="0" cellspacing="3" cellpadding="3">
    <tr>
      <td><left><b>Thomas, Susan Gregory. &quot;Capitalists on Campus: Students 
        with Computer Skills Cash In.&quot; <i><br>
        U.S. News &amp; World Report,</i> 7 Sept 1998:82</b></left></td>
    </tr>
  </table> 
  <h3 align="left"><font color="#1F4366"> How would you search Pegasus 
    to see if the library owned this article</font><font color="#1F4366">? (Choose 
    one)</font></h3>
				
<form method="POST" action="q8.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q8 == "title"):
        print "<input type=\"radio\" name=\"q8\" value=\"title\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"title\">";
    endif;
    ?>
    A. Search the article title "Capitalists on Campus" 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q8 == "source"):
        print "<input type=\"radio\" name=\"q8\" value=\"source\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"source\">";
    endif;
    ?>
    B. Search the source title "U.S. News and World Report"
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q8 == "author"):
        print "<input type=\"radio\" name=\"q8\" value=\"author\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"author\">";
    endif;
    ?>
    C. Search the author "Susan Gregory Thomas" 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q8 == "subject"):
        print "<input type=\"radio\" name=\"q8\" value=\"subject\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q8\" value=\"subject\">";
    endif;
    ?>
    D. Search the subject "college students and work"
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="9">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
