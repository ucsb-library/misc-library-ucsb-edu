<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 7;
//$next_question = $_REQUEST["next_question"];
//$q7 = $_REQUEST["q7"];
//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q7 == "" && $next_question == 8){
    $err_msg = "Please select a choice";
    $next_question = 7;
}
//Print and record question results
if ($next_question == 8){
    $correct = 0;
    $msg = "<h4>Answer to Question Seven:</h4>\n";
    if ($q7 == "NewYorkTimes") {
        $msg .= "<b>A:</b> No, remember you were asked which would NOT be a good choice. The New York Times, 
								Access World News, or Lexis-Nexis would all provide articles on the latest events. <br>";
    }
    if ($q7 == "BIOSIS") {
        $msg .= "<b>B:</b> You're right. The BIOSIS index would NOT help you locate recent news. 
								For that you'd go to The New York Times, Access World News, or Lexis-Nexis to find articles on the latest events.
								BIOSIS is a good index to use to find articles in the biological sciences.<br>";
								$correct = 1;
				} 
    if ($q7 == "NewsBank") {
    $msg .= "<b>C:</b> No, remember you were asked which would NOT be a good choice.The New York Times,
				 Access World News, or Lexis-Nexis would all provide articles on the latest events.<br>";
     
				}
    if ($q7 == "LexisNexis") {
        $msg .= "<b>D:</b> No, remember you were asked which would NOT be a good choice.The New York Times, 
								Access World News, or Lexis-Nexis would all provide articles on the latest events. <br>";
    }
	
    $response = $_COOKIE["response"];
    $response .= "[Question_7:".$q7."], ";
    setcookie ("response", $response);
    setcookie ("next", "8");
    setcookie ("ans[6]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 8</a></div></p>";
}
?>

<h2>Question Seven</h2>
<h3>
Most of the indexes below would help you find information on recent events in Afghanistan. 
Which of these article indexes would <u>not</u> be a good choice to find this type of information? (Choose one)
</h3>
<form method="POST" action="q7.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q7 == "NewYorkTimes"):
        print "<input type=\"radio\" name=\"q7\" value=\"NewYorkTimes\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"NewYorkTimes\">";
    endif;
    ?>
    A. New York Times (articles from the last 365 days of the New York Times)  
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q7 == "BIOSIS"):
        print "<input type=\"radio\" name=\"q7\" value=\"BIOSIS\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"BIOSIS\">";
    endif;
    ?>
    B. BIOSIS Previews (articles in the life sciences literature.About 545,000 records/year) 
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q7 == "NewsBank"):
        print "<input type=\"radio\" name=\"q7\" value=\"NewsBank\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"NewsBank\">";
    endif;
    ?>
    C. Access World News (contains current articles from over 500 US and Canadian newspapers) 
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q7 == "LexisNexis"):
        print "<input type=\"radio\" name=\"q7\" value=\"LexisNexis\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"LexisNexis\">";
    endif;
    ?>
    D. LexisNexis (A full text database service strong in law, business, and news)
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="8">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
