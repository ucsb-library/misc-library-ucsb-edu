<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<html>
<head>
<title>The Librarian as Publisher:  A Case Study of a World Wide Web Publishing Project</title>
</head>
<body bgcolor="#ffffff" text="#000000" link="#0000ff" alink="#b03060" vlink="#d02090">
<img src="web041.gif" alt="Untangling the Web">

<center> <table width="75%" border=1 cellpadding=3><tr><td> URLs in this  
document have been updated.  Links enclosed in <strong>{curly
brackets}</strong> have been changed.  If a replacement link was located,
the new URL was added and the link is active; if a new site could not be
identified, the broken link was removed.</td></tr></table></center><p>


<h1 align=center> The Librarian as Publisher:  A Case Study
of a World Wide Web Publishing Project</h1>
<center><strong><a href="mailto:mstover@mail.sdsu.edu">Mark
Stover</a></strong><br>
Director of Library Services<br>
Phillips Graduate Institute, North Hollywood, California</center><p>

<hr>
An adaptation of this paper appears in the October 1996
<cite>Computer in Libraries</cite> magazine and is presented here with
permission of the publisher, Information Today, Inc, Medford, NJ,
609/654-6266
<hr>

<h2>Abstract</h2>
<blockquote>

The world wide web has great potential as a publishing medium, both for
original electronic publications as well as for mirrored (already in
print) publications.  Librarians, because of our experience as information
providers, should not hesitate to become involved in web publishing
projects.  This presentation will describe the evolutionary experience of
a web-based electronic journal, and will include a discussion of various
issues confronting librarians who wish to publish on the web.
</blockquote>
<hr><p>

<h2>The Value of Librarians as Publishers on the Web</h2>

It's important for librarians to be involved in publishing on the web for
a number of reasons. <p>

First, we need to support the scholarly communication process, and web
publishing is a powerful symbol of our enfranchised role.  Some of us are
members of the faculty, and others are not, but we all need to be involved
somehow in this process.  Disseminating information is a function that
traditionally has been associated with librarians (though not
exclusively), and web publishing would appear to fit well into this
traditional paradigm. <p>

Second, we as librarians have a certain measure of expertise in the arena
of organizing and providing access to information.  What better way to
share our skills with the academic community than by turning web documents
into one-stop-shopping malls that offer the enduser subject access to a
document along with the full-text of the document itself? <p>

Third, web publishing gives us a higher profile on campus, which is an
important asset these days because of the changing nature of higher
education and the changing role of librarians. <p>

Fourth, the web provides unique access to scholarly information because of
its serendipitous nature, its immediacy, its hypertext structure, and its
universal appeal.  Why wouldn't librarians want to be involved with this
exciting new technology at the publishing level? <p>

Fifth, web publishing gives our parent institutions a positive reputation
in the national and international academic communities. This is more
important than ever given the current climate of declining enrollments
that so many colleges face today. <p>

Sixth, web publishing allows us to bypass the profit-based system of print
publishing that has tormented librarians for years by gouging out huge
chunks of money from our budgets.  Many of us bemoan the fact that we must
buy back (from traditional publishers) scholarship and research that
originates on our own campuses (<a href="#boyce">Boyce, 1996</a>; <a
href="#okerson">Okerson &amp; O'Donnell, 1995</a>; <a
href="#lancaster">Lancaster, 1995</a>).  Web publishing empowers us to
break this multigenerational transmission process.  This is
disintermediation at a very high level, since it eliminates one of the
main intermediaries (in this case, the print publisher) between author and
reader. <p>

However, as various authors (<a href="#crawford">Crawford &amp; Gorman
1995</a>; <a href="#tenopir">Tenopir &amp; King 1996</a>) have pointed
out, the economics of electronic publishing are not as simple as one might
think at first glance.  There are a variety of factors that raise the
costs of traditional print journal publishing, such as copy editing,
reviewing, and refereeing.  Publishers that maintain (and pay for) high
standards over many years build a reputation that in turn brings a certain
measure of authenticity and authority to its articles.  Will scholars want
to publish in electronic journals that may not have the same level of
perceived quality and authenticity?  Will readers, researchers, and tenure
committees grant the same authority and credence to electronic journals
that they currently give to traditional journals? <p>

<h2>A Case Study of a Web Publishing Project</h2>

<cite>Progress: Family Systems Research and Therapy</cite> is a journal
made up primarily of masters level student research.  The faculty of
Phillips Graduate Institute, an independent graduate school offering
degrees in marriage and family therapy, chooses the best masters theses
each year, which are then edited (by an editorial board made up of
students and a faculty advisor) into an annual publication.  The first
issue was published in Spring 1992, and volume five will come out in May
1996. <p>

A subscription to <cite>Progress</cite> sells for $10 per year.  Despite
this modest
cost, the journal has few paid subscriptions.  Web publishing seemed to us
to be an effective way of spreading our research to the rest of the world
without spending a fortune in marketing costs.  I'm the "webweaver" for
our college, and it was essentially my idea to mount
<cite>Progress</cite> on the web.
There was little debate from the faculty or editorial board on the merits
of this idea. <p>

The web version of <cite>Progress</cite>
({http://www.pgi.edu/progress.htm}) is what I like to
call a "mirrored" publication.  That is, the web site "mirrors" the print
text in that it provides access to the same information in an entirely
different format and with added value.  There is room on the web for
mirrored journals, but I think we will see more and more original
publications appearing on the web in the coming months and years.  The web
bypasses many of the printing and distribution costs of print format,
although it does not as yet have the prestige or authority that print
journals carry. <p>

The issue of refereed versus non-refereed journals is a sensitive one for
a publication like <cite>Progress</cite>.  Our scholarly tradition says that
for a journal to be accepted as legitimate and authoritative, it must be
refereed (preferably in a double-blind manner). Anything less, the
tradition states, is open to accusations of vanity publishing.  However,
there are some disciplines (most notably, the law) where non-refereed
journals carry equal weight with refereed journals.  The best example of
this is the Law Review, which is edited by students and generally does not
submit its articles to the double-blind referee process.  Whether or not
this model will be followed (or even accepted) in the social science world
remains to be seen. <p>

There is likely to be some debate over the issue of free access versus
fee-based access to scholarly web information.  Obviously, for-profit
publishers must protect their financial interest in research that they
sponsor, and they are already devising ways to charge for web-based
journals.  (Indeed, the OCLC-based electronic journals are a good example
of a fee-based system of scholarly web information).  But non-profit
academic institutions, who sponsor a great deal of today's research, may
decide (if they choose to get into the web publishing arena) that the goal
of disseminating information may take precedence over the goal of making
money.  In the case of <cite>Progress</cite>, lack of revenue from print
subscriptions made the problem of whether or not free access to the web
version would cut into the subscription base a moot point.  But most web
publishers will have to deal with this potential problem, especially in
the case of mirrored journals. <p>

<h2>Copyright Ramifications of Web Publishing</h2>

The inside cover of the latest issue of <cite>Progress</cite> states:  "No
part of this issue may be reproduced in any form--except for brief
quotations (not to exceed 500 words) in any review of professional
work--without the permission in writing from the publishers."  But, since
Phillips Graduate Institute holds the copyright, it wasn't a problem for
us to reproduce it on the web. <p>

How can we prevent others from ignoring the copyright restrictions and
illegally reproducing the information in a different forum?  At the
present time we can't stop such behavior, but perhaps future technologies
will allow us to prevent these unethical activities. However, while there
may be intellectual reasons (such as the concept of "fair play",
protection of the author's creation, etc.) for <cite>Progress</cite> to be
concerned about copyright violations, there are certainly no compelling
economic reasons for us to get involved in preventing copyright misuse.
But we would have great concern if we were a for-profit publisher or if
our authors had a financial stake in the distribution of their work. <p>

Copyright law in its present form seems somewhat inappropriate when
applied to the Internet.  Some believe that the web's system of copying
files back and forth across the Internet from server to client is actually
a technical violation of the copyright law. Others, such as Bert Boyce (<a
href="#boyce">1996</a>), feel that, even apart from Internet issues, the
system of copyright as it applies to scholarly journal publishing is
outmoded and unfair to authors.  We hope that solutions to these two
problems with the copyright law will emerge, converge, and enable us to
restructure the system of rewarding and protecting authors of scholarly
information in the coming century. <p>

<h2>Marketing the Web Publication</h2>

One approach to use in marketing a web publication is the "you scratch my
back, I'll scratch yours" method.  It is mutually beneficial when two or
more organizations agree to provide links to each others web journals.  I
am not aware of any formal "reciprocal agreements" that have been signed
for this purpose, but I would not be surprised if these become more
commonplace in the future.  This approach moves away from the old
"competitive" model that used to dominate academic publishing and moves
more in the direction of a "collaborative" model that builds on the
strengths of all parties involved. <p> The plethora of search engines and
web crawlers that are roaming the web today make the task of marketing a
web publication somewhat easier.  Other presentations in this conference
will demonstrate the specific strategies to use when submitting
information to a web search engine or when using HTML tags in the creation
of a web publication.  But the bottom line is that your web publication
will receive few visitors if you do not market it through various means.
<p> In other words, If you build it, they WILL come, but only if you
properly market the merits of your web journal throughout the web
community. <p>

<h2>Design Issues</h2>

There are several theories that exist today in regard to the design and
organization of a web journal.  One theory that should be dismissed
immediately is that a web journal should only give away very basic
information, such as tables of contents, abstracts, book reviews, or
sample articles.  These types of web publications are often listed under
the rubric of "Web Journals," but they can only be construed as a journal
in the broadest sense of the word.  More accurately, they are promotional
tools designed to attract more paid subscribers to the print version of
the journal.  To be more charitable, perhaps these pseudo-journals are
simply transitional phases of a publisher's journey toward web publishing,
but it is still a misnomer to label this information as part of the
journal genre. <p>

Another (more reputable) theory is that the web journal should replicate
the print environment.  That is, the web journal should retain the "look
and feel" of the print version as much as possible, and should not add (or
subtract) any material from the print journal.  This theory wishes to
honor the integrity of the journal and of the authors by maintaining a
consistent standard across the different media.  This approach views the
web journal as simply one more platform upon which the content and format
of the journal can stand.  For example, print, microfiche, microfilm,
online, and the web are all viewed as separate but equal media through
which the journal can be published.  Unfortunately, this viewpoint
underestimates the power and flexibility of the web as a publishing
conduit.  It does not take into consideration the hypertext and multimedia
aspects of the web that would allow the publisher or editor to add
tremendous value to the web journal through hyperlinks and multimedia. <p>

A third theory is more far-sighted and less conservative than the first
two.  This theory sees opportunities and possibilities in the new web
medium instead of constraints and limitations.  It seeks to add value to
the web journal by indexing each article and by adding other links (both
internal and external) to the document.  The web environment can be
utilized for maximum effect by employing such mechanisms as Java-based
interactivity, forms-based letters to the editor, and even virtual
reality.  It is this approach that holds tremendous promise for web
publishing as a radical departure from the old order. <p>

I see <cite>Progress</cite> as a third order web journal.  That is, I want
to add value to the text by indexing and hyperlinking it.  While each
article has been fully indexed, time constraints have limited the number
of hyperlinks added to each article.  Also, some of the concerns expressed
below (related to hyperlinking) have contributed to the lack of
value-added markup. <p>

<h2>Indexing the Web Publication</h2>

One solution for converting documents into a searchable database is to
laboriously mark up each article into HTML.  This is obviously a
time-consuming and tedious task, and is probably not the most
cost-effective nor the most efficient method to use. <p>

Another solution is to create a database of documents with an underlying
search engine familiar to the community (for example, the university's
online catalog search software might be modified to allow full-text
documents to be retrieved).  Then, a telnet gateway or CGI scripting can
be utilized to allow full access through the web.  This method can be
expensive for the publisher, but it also can be confusing to users who
often do not expect to see a different (i.e., non-web-based) search engine
while surfing the web. <p>

An inexpensive and relatively elegant solution is to use one of the newer
"web builder" software applications that use MARC, dBase, or ASCII records
to create a searchable web database.  We used BestWeb (<a
href="http://www.bestseller.com">http://www.bestseller.com</a>) in our
project primarily because of the price tag, but the results were more than
satisfactory. BestWeb allowed us to build a database of bibliographic
records that was searchable through three different access points (we
chose author, title, and subject).  In turn, we linked these records to
the full-text documents (which had been converted to HTML), thus creating
(in effect) a one-step lookup for users. <p>

<h2>Hyperlinking the Web Publication</h2>

Hyperlinks between and among the different articles in our journal would
maximize the web's hypertext capabilities.  For example, if a
<cite>Progress</cite> author writing in 1995 quotes a
<cite>Progress</cite> author writing in 1993, the ideal would be to create
links that would connect these articles in a two-way relationship.
Readers viewing the 1995 article could automatically see the article that
was being quoted.  Readers viewing the earlier article could anticipate
what would be written about this article 2 years later. <p>

Another kind of linkage (that is actually relatively common on the web) is
to connect each reference cited in the text of the article with the full
bibliographic citation at the end of the article.  Readers can then toggle
back and forth between the body of the article and the bibliography. <p>

One of the major problems in adding hotlinks to any kind of web document
is the time-consuming nature of the task.  There is intellectual effort
(which words or phrases should be marked up? how many?  how often?  where
should the hotlinks point to?) as well as physical effort (the actual
marking up of the text). Publishers of web documents must ask themselves
if the added value (which is sometimes debatable) of hyperlinking the text
is worth the extra work. <p>

Should hyperlinks be internal (self-referential) or external?
Self-referential links are easier to control, but external links are more
interesting.  For example, a self-referential link would connect one piece
of an author's article to another piece of the same article.  Perhaps all
references to a particular citation within the article could be linked
together. <p>

External links are more risky, but they are also certainly more rewarding.
An external link might connect a mention of the American Psychological
Association to the APA home page.  It might anchor a reference to a
particular author listed in the bibliography to that author's home page or
e-mail address.  The risk comes when these links become orphaned by changed
or defunct URLs. <p>

<h2>Limitations of Hyperlinking</h2>

At a certain point one could argue that the webweaver becomes a co-author
or editor working alongside the original author.  But can we change the
nature of the article without permission? This would seem to be both
arrogant as well as unethical. Another question to be asked is, Do
hotlinks detract from the clarity and original purpose of the article?
The answer to this probably depends on the nature of both the article as
well as the links, but there is undoubtedly a point of diminishing returns
where too many links create noise and confusion for the reader. <p>

<a href="#crawford">Crawford and Gorman (1995)</a>, in their sane but
perhaps overly cautious book, criticize attempts to convert essentially
linear text to hypertext documents.  This criticism is especially
trenchant when "overhyperlinking" takes place.  Overhyperlinking an
article from a web journal, especially when done from the editorial
(unilateral) perspective, is unfair to both author and reader.  This has
already become a problem for more ephemeral web documents, such as home
pages, and could end up detracting from the value of the original article
in more scholarly web publishing. <p>

<h2>The Librarian's Role in Web Publishing</h2>

Librarians can help to bring scholarly information to the web in a number
of different ways.  First, we can (and should) be more assertive in
promoting web publishing.  This can take the form of something as simple
as encouraging others to publish their material on the web, or it can be
as complex as pushing the issue of web publishing to the forefront of the
campus administrative agenda. <p>

Second, we should be actually publishing full-text articles (research
and/or opinion pieces) on the web.  These articles can be placed on our
own home pages, or (better yet) they can be given a place of prominence on
the campus or library web server.  The research can be written by and for
librarians, or it can be more broadly based and venture outside our field
into other disciplines.  As the media has informed us on a different
matter, "Just do it!" <p>

Third, librarians should be indexing these web publications. While
full-text indexing has often proven to be less than helpful in terms of
precision and recall issues, these published articles deserve at least the
basic intellectual access points of author, title, and subject.  Others
more qualified than me will debate the merits of automated indexing versus
human-generated indexing, but my limited knowledge of this area leads me
to believe that there is still a need for human subject catalogers and
indexers. <p> Fourth, we should be adding value to web publications
through internal and external hyperlinks, being careful to stay within the
legal, ethical, and aesthetic boundaries of copyright, authorial
integrity, and web page design.  These links make the web more than just
another platform on which we can mount written research.  The value-added
aspects of the web, especially its almost universal accessibility, its
immediacy, and its hypertext nature, make a powerful and compelling
argument for the concept of scholarly web publishing. <p> <hr width=200>
<p>

<h2>References</h2>

<a name="boyce">
<strong>Boyce, B.R.</strong></a>  February 1996.  Copyright could be wrong.
<cite>American Libraries</cite> 27: 27-28.
<p>

<a name="crawford"> <strong>Crawford, W. &amp; Gorman, M.</strong></a>
1995.  <cite>Future Libraries:  Dreams, Madness and Reality.</cite>
American Library Association, Chicago. <p>


<strong>Guedon, J.</strong>  1995.  Research libraries and electronic
scholarly journals: challenges or opportunities?  <cite>Serials
Librarian</cite> 26 (3- 4): 1-20.
<p>

<strong>Hunter, K.</strong>  1988.  Academic librarians and publishers:
customers versus producers or partners in the planning of electronic
publishing?  <cite>Journal of Library Administration</cite> 9 (4): 35-48.
<p>

<a name="lancaster">
<strong>Lancaster, F.W.</strong></a>  Spring 1995.  Networked Scholarly
Publishing. Theme issue of <cite>Library Trends</cite> 43 (4).
<p>

<a name="okerson">
<strong>Okerson, A. S., &amp; O'Donnell, J. J.</strong></a> 1995.
<cite>Scholarly Journals at the Crossroads: A Subversive Proposal for
Electronic Publishing.</cite> Association of Research Libraries. <p>

<a name="tenopir">
<strong>Tenopir, C., &amp; King, D.</strong></a>  1996.  Setting the record
straight on journal publishing: myth vs. reality.  <cite>Library
Journal</cite> 121 (5): 32-35.<p>


<P> <A HREF="http://validator.w3.org/"><IMG SRC="http://www.library.ucsb.edu/gallery/valid_html3.2.gif" ALT="HTML 3.2 Checked!" border=0></A>



</body></html>
