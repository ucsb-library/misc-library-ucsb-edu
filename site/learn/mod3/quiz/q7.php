<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 7;
//$next_question = $_REQUEST["next_question"];
//$q7 = $_REQUEST["q7"];
//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q7 == "" && $next_question == 8){
    $err_msg = "Please select a choice";
    $next_question = 7;
}
//Print and record question results
if ($next_question == 8){
    $correct = 0;
    $msg = "<h4>Answer to Question Seven:</h4>\n";
    if ($q7 == "keyword") {
        $msg .= "<b>A:</b> No, A keyword search will look for 'bill gates' anywhere in a record.  It might find books that only
								briefly mention Bill Gates (e.g., <i>The Wealthy 100</i>), as well as books <b>by</b> him.  For a more focused
								search for books <b>about</b> Bill Gates, use a subject search. <br>";
    }
    if ($q7 == "subject") {
        $msg .= "<b>B:</b> Good choice! A subject search will focus your search on books that are <i>about</i> Bill Gates.<br>";
								$correct = 1; 
				} 
    if ($q7 == "author") {
    $msg .= "<b>C:</b> No. This would give you books written <b>by</b> him but not necessarily <b>about</b> him.<br>";
    }
    $response = $_COOKIE["response"];
    $response .= "Question_7: ".$q7."], ";
    setcookie ("response", $response);
    setcookie ("next", "8");
    setcookie ("ans[6]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 8</a></div></p>";
}
?>

<h2>Question Seven</h2>
<h3> If you want to find a book about Bill Gates, you would use a (Choose one)</h3>
<p></p>
<form method="POST" action="q7.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    
<td> 
<?php
    if ($q7 == "keyword"):
        print "<input type=\"radio\" name=\"q7\" value=\"keyword\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"keyword\">";
    endif;
    ?>
A. Keyword search </tr>
  <tr> 
    
<td> 
<?php
    if ($q7 == "subject"):
        print "<input type=\"radio\" name=\"q7\" value=\"subject\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"subject\">";
    endif;
    ?>
B. Subject search </tr>
  <tr> 
    
<td> 
<?php
    if ($q7 == "author"):
        print "<input type=\"radio\" name=\"q7\" value=\"author\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q7\" value=\"author\">";
    endif;
    ?>
C. Author search </tr>
  
 
  </table>
<p>
<input type="hidden" name ="next_question" value="8">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>
