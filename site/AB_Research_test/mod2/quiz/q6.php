<?php
ob_start();
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 6; //used in quiz_header.php

//Check that question hasn't been answered
require_once "../../quiz_header.php";
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q6 == "" && $next_question == 7){
    $err_msg = "Please select a choice";
    $next_question = 6;
}
//Print and record question results
if ($next_question == 7){
    $correct = 0;
    $msg = "<h4>Answer to Question Six:</h4>\n";
    if ($q6 == "true") {
        $msg .= "<b>A:</b> You're right! The connector OR broadens your search because either or both of the terms can be present.<br>";
        $correct = 1;
    } else {
    $msg .= "<b>B:</b> Sorry, wrong. The connector OR broadens your search and the connector AND narrows the search.<br>";
}
    $response = $_COOKIE["response"];
    $response .= "[Question_6: ".$q6."], ";
    setcookie ("response", $response);    
    setcookie ("next", "7");
    setcookie ("ans[5]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 7</a></div></p>";
}    
?>

<h2>Question Six</h2>
<h3>Using the connector OR in a search</h3>
<form method="POST" action="q6.php">
<div align="CENTER">

  <table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td><?php
    if ($q6 == "true"):
        print "<input type=\"radio\" name=\"q6\" value=\"true\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"true\">";
    endif;
    ?>
				A. broadens your search 
    </td>
  </tr>

  <tr> 
    <td>   
    <?php
    if ($q6 == "false"):
        print "<input type=\"radio\" name=\"q6\" value=\"false\" CHECKED>";
    else: 
        print "<input type=\"radio\" name=\"q6\" value=\"false\">";
    endif;
    ?>
				B. narrows your search 
    </td>
  </tr>
  </table>
<p>
<input type="hidden" name ="next_question" value="7">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?> 
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
				onLoad=window.alert(\"$err_msg\")
				-->
    </script>";
}
print "$msg";
require_once "quiz_footer.php";
?>