<?php
ob_start();
require_once "../../functions.php";
foreach ($_REQUEST as $a=>$b){$$a=$b;}
$question = 1;
//$next_question = $_REQUEST["next_question"];
//$q1 = $_REQUEST["q1"];
//Check that question hasn't been answered
$msg = chk_question($next_question);
if ($msg != "") {
 $next_question = 0;
}
//Check that at least one response has been entered
if ($q1 == "" && $next_question == 2){
    $err_msg = "Please select a choice";
    $next_question = 1;
}
//Print and record question results
if ($next_question == 2){
    $correct = 0;
    $msg = "<h4>Answer to Question One:</h4>\n";
    if ($q1 == "title") {
        $msg .= "<b>A:</b> The records for all journal titles that the Libraries subscribe to <b>are</b> in Pegasus.<br>";
    }
    if ($q1 == "article") {
        $msg .= "<b>B:</b>Yes, To identify a journal article, you will need to use an article index. Sometimes the article will be available full text in the index 								but more often, you will need to look up the journal title in Pegasus to see 								if we own it.<br>";
								$correct = 1; 
    } 
    if ($q1 == "musical") {
    $msg .= "<b>C:</b> The records for musical scores owned by the Libraries <b>are</b> in Pegasus.<br>";
    }
    if ($q1 == "video") {
        $msg .= "<b>D:</b> The records for videos owned by the Libraries <b>are</b> in Pegasus.<br>";
    }
	
    $response = $_COOKIE["response"];
    $response .= "[Question_1: ".$q1."], ";
    setcookie ("response", $response);
    setcookie ("next", "2");
    setcookie ("ans[0]", $correct);
    $msg .= "<p><div align='center'><a href='quiz.php'>Go on to question 2</a></div></p>";
}
require_once "../../quiz_header.php";
?>

<h2>Question One</h2>
<h3>Which item CANNOT be found in Pegasus, the Libraries catalog? (Choose one)</h3>
<form method="POST" action="q1.php">
<div align="CENTER">
<table border="0" width="60%" cellspacing="2" cellpadding="2">
  <tr> 
    <td> 
				<?php
    if ($q1 == "title"){
        print "<input type=\"radio\" name=\"q1\" value=\"title\" CHECKED>"; }
    else { 
        print "<input type=\"radio\" name=\"q1\" value=\"title\">";
    }
    ?>
    A. Journal Title
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q1 == "article"){
        print "<input type=\"radio\" name=\"q1\" value=\"article\" CHECKED>";}
    else{ 
        print "<input type=\"radio\" name=\"q1\" value=\"article\">";
    }
    ?>
    B. Journal Article
  </tr>
  <tr> 
    <td> 
   <?php
    if ($q1 == "musical"){
        print "<input type=\"radio\" name=\"q1\" value=\"musical\" CHECKED>";}
    else{
        print "<input type=\"radio\" name=\"q1\" value=\"musical\">";
    }
    ?>
    C. Musical Score
  </tr>
  <tr> 
    <td> 
    <?php
    if ($q1 == "video"){
        print "<input type=\"radio\" name=\"q1\" value=\"video\" CHECKED>";}
    else{ 
        print "<input type=\"radio\" name=\"q1\" value=\"video\">";
    }
    ?>
    D. Video Recording
  </tr>
 
  </table>
<p>
<input type="hidden" name ="next_question" value="2">
<?php 
    if ($msg == ""){ 
        //is msg != "" then the form has been submitted
        print "<input type=\"submit\" value=\"submit\">";
    } 
?>
</p>
</div>
</form>
<?php
//Print error message or results 
if ($err_msg != ""){
    print "
    <script language=\"JavaScript\">
    <!--
    onLoad=window.alert(\"$err_msg\")
    -->
    </script>";
}
print "$msg";
require_once "../../quiz_footer.php";
?>
